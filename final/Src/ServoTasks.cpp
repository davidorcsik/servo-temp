#include <ServoTasks.h>

#include <task.h>
#include "cmsis_os.h"
#include <time.h>

#include <limits>
extern "C" void sensorTask(void const * argument)
{
	float temperature = std::numeric_limits<float>::min();
	std::optional<devices::Accelerometer::acceleration> acceleration_reading;
	std::optional<devices::GPS::GPSData> gps_reading;
	for (;;)
	{
		// temperature measurement
		std::optional<float> temperature_reading = DeviceCollection::temperature_sensor.get_temp();
		if (temperature_reading.has_value()) {
			temperature = temperature_reading.value();
		}
		devices::DebugConsole::log("Temperature:");
		devices::DebugConsole::log(temperature);
		
		// accelerometer
		devices::Accelerometer::acceleration acceleration;
		acceleration_reading = DeviceCollection::accelerometer.read_acceleration();
		if (acceleration_reading.has_value()) {
			acceleration = DeviceCollection::accelerometer.read_acceleration().value();
		}
		devices::DebugConsole::log("Acceleration (X,Y,Z):");
		devices::DebugConsole::log(acceleration.x);
		devices::DebugConsole::log(acceleration.y);
		devices::DebugConsole::log(acceleration.z);
		
		// gps
		devices::GPS::GPSData gps_data;
		gps_reading = DeviceCollection::gps.read_and_parse();
		if (gps_reading.has_value()) {
			gps_data = gps_reading.value();
		}
		devices::DebugConsole::log("GPS fix:");
		devices::DebugConsole::log(gps_data.fix);
		
		// lora
		char lora_buffer[32];
		devices::LoRa::STATUS lora_status = DeviceCollection::loraHandle.get_hweui(lora_buffer, 32);
		devices::DebugConsole::log("LoRa hweui:");
		devices::DebugConsole::log(lora_buffer);
		
		
		devices::DebugConsole::log("--------------------");
		
		
		
		osDelay(50);
	}
}