/*
 * peripheral_test.c
 *
 *  Created on: Feb 4, 2019
 *      Author: mate_pc
 */

#include "bnrg_peripheral.h"

static uint16_t own_connection_handler = 0;									//connection handler of the device
static uint32_t own_passkey = 0;											//passkey of the device

uint8_t advertising_data[MAX_ADV_DATA_LENGTH];

static ble_conn_cb_t ble_conn_cb;											//function pointers for callback
static ble_disconn_cb_t ble_disconn_cb;
static ble_pairing_cmplt_cb_t ble_pairing_cmplt_cb;
static ble_attr_mod_cb_t ble_attr_mod_cb;
static ble_write_req_cb_t ble_write_req_cb;
static ble_read_req_cb_t ble_read_req_cb;

static uint8_t central_connected = 0;										//connection counter
static ble_peripheral_state_t device_state = p_disconnected;				//device state
ble_peripheral_procedure_state_t peripheral_procedure_state;				//procedure state

ble_central_procedure_state_t ble_get_peripheral_proc_state()
{
	return peripheral_procedure_state;
}

static uint8_t ble_fill_128bit_uuid(uint8_t * uuid_result,uint8_t uuid_result_size,uuid_t *uuid,uint8_t uuid_size)	//used for advertising
{
	int i;
	uint8_t index = 1;
	uuid_result[0] = AD_TYPE_128_BIT_SERV_UUID;
	uint8_t cntr = 0;

	for(i=0;i<uuid_size;i++)				//check how many arrays are included in uuid
	{
		if(uuid[i].size == UUID_TYPE_128)
			cntr++;
	}

	if(!(cntr*16+1>uuid_result_size))		//if uuid_result is enough big
	{
		for(i=0;i<uuid_size;i++)
		{
			if(uuid[i].size == UUID_TYPE_128)
			{
				memcpy(uuid_result+index,uuid[i].array,16);
				index = index + 16;
			}
		}
		return index;
	}
	else
	{
		uuid_result[0] = 0;
		return 0;
	}
}

static uint8_t ble_fill_16bit_uuid(uint8_t *uuid_result,uint8_t uuid_result_size,uuid_t *uuid,uint8_t uuid_size)	//used for advertising
{
	int i;
	uint8_t index = 1;
	uuid_result[0] = AD_TYPE_16_BIT_SERV_UUID;
	uint8_t cntr = 0;

	for(i=0;i<uuid_size;i++)				//check how many arrays are included in uuid
	{
		if(uuid[i].size == UUID_TYPE_16)
			cntr++;
	}

	if(!(cntr*2+1>uuid_result_size))		//if uuid_result is enough big
	{
		for(i=0;i<uuid_size;i++)
		{
			if(uuid[i].size == UUID_TYPE_16)
			{
				memcpy(uuid_result+index,uuid[i].array,2);
				index = index + 2;
			}
		}
		return index;
	}
	else
	{
		uuid_result[0] = 0;
		return 0;
	}
}

tBleStatus ble_start_advertising_extended(const char*local_name, uint8_t local_name_size, uint8_t*manuf_data,
		uint8_t manuf_data_size, uuid_t* uuid, uint8_t uuid_size)
{
	device_state = p_advertising;
	peripheral_procedure_state = p_pending_proc;
	volatile tBleStatus ret;
	int i;
	uint8_t offset;
	uint8_t uuid_array[MAX_ADV_UUID_NUM * 16 + 2] = { 0 };																//worst case is when each service is costume service and AD_TYPE should be used for the first and the last byte
	uint8_t uuid_array_size = 0;
	uint8_t uuid_type_indicator = 0x00;

	char name[MAX_ADV_NAME_SIZE + 1] = { AD_TYPE_COMPLETE_LOCAL_NAME, 0 };
	uint8_t name_size = 0;

	if (local_name_size > MAX_ADV_NAME_SIZE)
		return BLE_STATUS_INVALID_NAME_SIZE;

	name_size = local_name_size + 1;
	memcpy(name + 1, local_name, local_name_size);																		//copy local_name array to name array from first index

	uint8_t manufact_data[MAX_ADV_MANUF_DATA_SIZE + 2] = {manuf_data_size+1,AD_TYPE_MANUFACTURER_SPECIFIC_DATA, 0 };	//{data size with ad type too,ad type,data}
	uint8_t manufact_data_size = 0;

	if (manuf_data_size > MAX_ADV_MANUF_DATA_SIZE)
		return BLE_STATUS_INVALID_MANUF_DATA_SIZE;

	manufact_data_size = manuf_data_size + 2;
	memcpy(manufact_data + 2, manuf_data, manuf_data_size);

	hci_le_set_scan_resp_data(0, NULL);																					//scan response disabled (not used)

	for(i=0;i<uuid_size;i++)
	{
		if(uuid[i].size == UUID_TYPE_128)
			uuid_type_indicator |= 0x01;
		if(uuid[i].size == UUID_TYPE_16)
			uuid_type_indicator |= 0x02;
	}

	switch(uuid_type_indicator)
	{
		case 0x01:
			uuid_array_size = ble_fill_128bit_uuid(uuid_array,sizeof(uuid_array),uuid,uuid_size);
		break;

		case 0x02:
			uuid_array_size = ble_fill_16bit_uuid(uuid_array,sizeof(uuid_array),uuid,uuid_size);
		break;

		case 0x03:
			offset = ble_fill_128bit_uuid(uuid_array,sizeof(uuid_array),uuid,uuid_size);
			uuid_array_size = ble_fill_16bit_uuid(uuid_array+offset,sizeof(uuid_array),uuid,uuid_size);
		break;

		default:
			uuid_array_size = 0;
		break;
	}

	//TODO: returns invalid parameters in case of costume id (stack did not support costume UUID advertising)
	ret = aci_gap_set_discoverable(ADV_IND, 0x20, 0x100, PUBLIC_ADDR, NO_WHITE_LIST_USE, name_size, name, uuid_array_size, uuid_array, 0x006,0x008);
	//advertising interval is the period between advertising events (when advertising packet sent in each advertising channel at the same time)
	if (ret != BLE_STATUS_SUCCESS)
	{
		return ret;
	}

	if (manuf_data_size > 1)
	{
		ret = aci_gap_update_adv_data(manufact_data_size, manufact_data);
	}

	device_state = p_advertising;
	return ret;
}

tBleStatus ble_start_advertising()
{
	volatile tBleStatus ret;
	ret = aci_gap_set_discoverable(ADV_IND, 0x20, 0x100, PUBLIC_ADDR, NO_WHITE_LIST_USE, 0, NULL, 0, NULL, 0x006,0x008);
	//advertising interval is the period between advertising events (when advertising packet sent in each advertising channel at the same time)
	return ret;
}

tBleStatus ble_stop_advertising()
{
	volatile tBleStatus ret = aci_gap_set_non_discoverable();
	return ret;
}

tBleStatus ble_set_adv_data(uint8_t *data,uint8_t length)
{
	volatile tBleStatus ret = aci_gap_update_adv_data(length,data);
	return ret;
}

void ble_set_scan_resp_data(uint8_t * data,uint8_t length)
{
	hci_le_set_scan_resp_data(length,data);
}

tBleStatus ble_remove_AD_type(uint8_t ad_type)
{
		volatile tBleStatus ret = aci_gap_delete_ad_type(ad_type);
		return ret;
}

tBleStatus ble_update_local_char_value(uint16_t service_handler, uint16_t characteristic_handler, uint8_t data_len, uint8_t* data)
{
	volatile tBleStatus ret = aci_gatt_update_char_value(service_handler, characteristic_handler, 0, data_len, data);
	return ret;
}

static void ble_evt_vendor_handler(hci_event_pckt* event_pckt)
{
	evt_blue_aci* blue_evt = (void*) event_pckt->data;
	switch (blue_evt->ecode)
	{
		//central requested to read specific characteristic (read char value/read long char value)
		case EVT_BLUE_GATT_READ_PERMIT_REQ:
				if(ble_read_req_cb)
				{
					evt_gatt_read_permit_req* ptr = (void*) blue_evt->data;
					(*ble_read_req_cb)(ptr->conn_handle,ptr->attr_handle,ptr->data_length,ptr->offset);
				}
			break;
		//this event is generated in case of "passkey" key generation method (depends on IO capability)
		case EVT_BLUE_GAP_PASS_KEY_REQUEST:

				aci_gap_pass_key_response(own_connection_handler, own_passkey);

			break;
		//pairing completed event
		case EVT_BLUE_GAP_PAIRING_CMPLT:
			if (ble_pairing_cmplt_cb)
			{
				evt_gap_pairing_cmplt* ptr = (void*) blue_evt->data;
				(*ble_pairing_cmplt_cb)(ptr-> conn_handle,ptr->status);
				if(ptr->status == 0x00){
					device_state = p_paired;
					peripheral_procedure_state = p_no_proc;
					central_connected = 1;
				}
			}

			break;
		//this event is  generated if client overwrites an attribute (for example: client characteristic descriptor in case of notification)
		case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:
			if (ble_attr_mod_cb)
			{
				evt_gatt_attr_modified_IDB05A1* ptr = (void*) blue_evt->data;
				(*ble_attr_mod_cb)(ptr->attr_handle, ptr->att_data, ptr->data_length);
			}

			break;
		//this event is generated when a client requested to overwrite an attribute- server must check whether the value can be allowed to be written or not
		case EVT_BLUE_GATT_WRITE_PERMIT_REQ:
			if (ble_write_req_cb)
			{
				evt_gatt_write_permit_req* ptr = (void*) blue_evt->data;
				(*ble_write_req_cb)(ptr->conn_handle, ptr->attr_handle, ptr->data, ptr->data_length);
			}

			break;

		default:
			break;
	}
}

static void ble_evt_le_meta_handler(hci_event_pckt* event_pckt)
{
	evt_le_meta_event* evt = (void*) event_pckt->data;
	//connection completed event
	switch (evt->subevent)
	{
		case EVT_LE_CONN_COMPLETE:
			if (ble_conn_cb)
			{
				if(ble_get_auth_req())										//authentication was set up during initialization
				{
					evt_le_connection_complete* ptr = (void*) evt->data;
					(*ble_conn_cb)(ptr->peer_bdaddr, ptr->handle);
					own_connection_handler = ptr->handle;
					device_state = p_connected;
				}
				else
				{
					evt_le_connection_complete* ptr = (void*) evt->data;	//authentication was not set up
					(*ble_conn_cb)(ptr->peer_bdaddr, ptr->handle);
					own_connection_handler = ptr->handle;
					device_state = p_connected;								//next state is connected (procedure ended)
					peripheral_procedure_state = p_no_proc;
					central_connected = 1;
				}
			}
			break;

		default:
			break;
	}
}

static void ble_evt_hci_handler(hci_event_pckt* event_pckt){
	switch(event_pckt->evt)
	{
		case EVT_DISCONN_COMPLETE:

			central_connected = 0;
			if (ble_disconn_cb)
			{
				disconnect_cp* ptr = (void*) event_pckt->data;
				(*ble_disconn_cb)(ptr->handle, ptr->reason);
				device_state = p_disconnected;
				peripheral_procedure_state = p_no_proc;
			}
			break;

		default:
			break;
	}
}

void ble_hci_event_cb_peripheral(void * pData)
{

	hci_uart_pckt *hci_pckt = pData;
	hci_event_pckt *event_pckt = (hci_event_pckt*) hci_pckt->data;

	if (hci_pckt->type != HCI_EVENT_PKT)
	{
		return;
	}

	switch (event_pckt->evt)
	{

		case EVT_LE_META_EVENT:										//traditional hci meta events
			ble_evt_le_meta_handler(event_pckt);
			break;

		case EVT_VENDOR:
			ble_evt_vendor_handler(event_pckt);			//vendor specific events
			break;

		default:
			ble_evt_hci_handler(event_pckt);			//traditional hci events
			break;
	}
}

 void ble_set_peripheral_passkey(uint32_t key)
{
	own_passkey = key;
}

uint8_t ble_has_central_connected()
{
	return central_connected;
}

void ble_set_peripheral_connection_handler(uint16_t conn_handler)		//stack only holds one handler at a time, application should set up handler before read/write etc. to determine which channel is to be read/written
{
	own_connection_handler = conn_handler;
}

ble_peripheral_state_t ble_get_peripheral_state()
{
	return device_state;
}

void ble_peripheral_set_connection_complete_cb(ble_conn_cb_t cb)
{
	 ble_conn_cb = cb;
}

void ble_peripheral_set_disconnection_complete_cb(ble_disconn_cb_t cb)
{
	ble_disconn_cb = cb;
}

void ble_peripheral_set_pairing_complete_cb(ble_pairing_cmplt_cb_t cb)
{
	ble_pairing_cmplt_cb = cb;
}

void ble_peripheral_set_attribute_modified_cb(ble_attr_mod_cb_t cb)
{
	ble_attr_mod_cb = cb;
}

void ble_peripheral_set_write_request_cb(ble_write_req_cb_t cb)
{
	ble_write_req_cb = cb;
}

void ble_peripheral_set_read_request_cb(ble_read_req_cb_t cb)
{
	ble_read_req_cb = cb;
}

void ble_allow_read()
{
	aci_gatt_allow_read(own_connection_handler);
}

void ble_allow_write(uint16_t attr_handle,uint8_t *attr_val,uint8_t attr_len,uint8_t allow)
{
	if(allow)
	{
		aci_gatt_write_response(own_connection_handler,attr_handle,0x00,0x00,attr_len,attr_val);
	}
}
