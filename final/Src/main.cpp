/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "app_x-cube-ble1.h"
#include <stm32l4xx_hal_flash.h>

#include <cstring>

#include <UART.h>
#include <I2CBus.h>
#include <TemperatureSensor.h>
#include <Accelerometer.h>
#include <LoRa.h>
#include <GPS.h>
#include <CANBus.h>
#include <ServoTasks.h>
#include <DeviceCollection.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

CAN_HandleTypeDef hcan1;

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

osThreadId defaultTaskHandle;
uint32_t defaultTaskBuffer[128];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId sensorTaskHandle;
uint32_t sensorTaskBuffer[512];
osStaticThreadDef_t sensorTaskControlBlock;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_CAN1_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */
  

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_ADC1_Init();
	MX_CAN1_Init();
	MX_I2C1_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_USART3_UART_Init();
	
	DeviceCollection::init();
	
	const uint32_t lora_transmit_period = 60000;
	const uint32_t accelerometer_alarm_cutoff_period = 600000;
	uint32_t lora_transmit_timer = 0;
	uint32_t accelerometer_alarm_cutoff_timer = 0;
	bool accelerometer_alarm = false;
	auto set_alarm_result = DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, 10, 17);
	DeviceCollection::loraHandle.send_data(devices::LoRa::TRANSMIT_TYPE::CONFIRMED, 2, "START", 5);
	uint32_t reference_time = HAL_GetTick();
	HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
	while (true) {
		uint32_t current_time = HAL_GetTick();
		lora_transmit_timer += current_time - reference_time;
		accelerometer_alarm_cutoff_timer += current_time - reference_time;
		reference_time = current_time;
		
		devices::GPS::GPSData gps_data = DeviceCollection::gps.read_and_parse();
		
		if (lora_transmit_timer >= lora_transmit_period) {
			lora_transmit_timer = 0;
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			
			char gps_adenuis_data[128] = { 0 };
			if (gps_data.fix > 2) {
				DeviceCollection::gps.get_last_update_as_adeunis_alternative(0xd2, DeviceCollection::temperature_sensor.get_temp().value_or(16), 0xf11, gps_adenuis_data, 128);
			} else {
				std::sprintf(gps_adenuis_data, "00");
			}
			DeviceCollection::loraHandle.send_adenuis(gps_adenuis_data);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
		}
		
		if (!accelerometer_alarm) {
			std::optional<devices::Accelerometer::motion> motion = DeviceCollection::accelerometer.get_motion_data();
			if (motion.value().flag_any()) {
				accelerometer_alarm = true;
				accelerometer_alarm_cutoff_timer = 0;
				DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::DISABLED);
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
				HAL_Delay(2000);
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
			}
		} else {
			++accelerometer_alarm_cutoff_timer;
			if (accelerometer_alarm_cutoff_timer >= accelerometer_alarm_cutoff_period) {
				accelerometer_alarm = false;
				DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, 10, 17);
			}
		}
		HAL_Delay(100);
	}
	
	
	
	
	char gps_buffer[256];
	DeviceCollection::gps.get_last_update_as_adeunis_alternative(20, 210, 50, gps_buffer, 256);
	DeviceCollection::loraHandle.send_adenuis(gps_buffer);
//	protocols::UART debugConsole = protocols::UART(&huart3);
//	
//	protocols::I2C i2cHandle(&hi2c1);
//	devices::TempSensor tempSensorHandle(i2cHandle, devices::TempSensor::DEVICE_ADDRESSES::A0);
//	devices::TempSensor::STATUS status = tempSensorHandle.set_resolution(devices::TempSensor::RESOLUTIONS::CELSIUS_05);
//	protocols::SerialMessage debugMessage = debugConsole.create_message(nullptr, 128).value();
//	while (true) {
//		float temp_sensor_reading = tempSensorHandle.get_temp().value();
//		char buffer[256];
//		const char *tmpSign = (temp_sensor_reading < 0) ? "-" : "";
//		float tmpVal = (temp_sensor_reading < 0) ? -temp_sensor_reading : temp_sensor_reading;
//		
//		int tmpInt1 = tmpVal; 
//		float tmpFrac = tmpVal - tmpInt1;
//		int tmpInt2 = tmpFrac * 100000;
//		
//		std::sprintf(buffer, "Temp: %s%d.%05d�C\r\n", tmpSign, tmpInt1, tmpInt2);
//		debugMessage.set_payload(reinterpret_cast<uint8_t*>(buffer), strlen(buffer));
//		debugConsole.write(debugMessage);
//		
//		HAL_Delay(50);
//	}
	
//	devices::Accelerometer accelerometerHandle(i2cHandle, devices::Accelerometer::DEVICE_ADDRESSES::SA0_LOW, devices::Accelerometer::RESOLUTION::RES_8BIT);
//	char acc_msg_text[512];
//	protocols::SerialMessage message;
//	auto set_alarm_result = accelerometerHandle.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, 10, 17);
//	accelerometerHandle.set_auto_wake_sleep(devices::Accelerometer::AUTO_WAKE_SLEEP_STATE::DISABLED, 10, devices::Accelerometer::SLEEP_MODE_SAMPLE_TIME::PER_1_56_SECS);
//	while (true) {
////		std::fill_n(acc_msg_text, 512, '\0');
//		auto temp = accelerometerHandle.read_acceleration();
//		if (!temp.has_value()) {
//			continue;
//		}
//		auto acceleration = temp.value();
//
//		
//		
//		//sprintf float bullshit fix...........
////		const char *tmpSign = (acceleration.x < 0) ? "-" : "";
////		float tmpVal = (acceleration.x < 0) ? -acceleration.x : acceleration.x;
////
////		int tmpInt1 = tmpVal; 
////		float tmpFrac = tmpVal - tmpInt1;
////		int tmpInt2 = tmpFrac * 100000;
////
////		sprintf(acc_msg_text + strlen(acc_msg_text), "x: %s%d.%05d ", tmpSign, tmpInt1, tmpInt2);
////		
////		tmpSign = (acceleration.y < 0) ? "-" : "";
////		tmpVal = (acceleration.y < 0) ? -acceleration.y : acceleration.y;
////
////		tmpInt1 = tmpVal; 
////		tmpFrac = tmpVal - tmpInt1;
////		tmpInt2 = tmpFrac * 100000;
////
////		sprintf(acc_msg_text + strlen(acc_msg_text), "y: %s%d.%05d ", tmpSign, tmpInt1, tmpInt2);
////		
////		tmpSign = (acceleration.z < 0) ? "-" : "";
////		tmpVal = (acceleration.z < 0) ? -acceleration.z : acceleration.z;
////
////		tmpInt1 = tmpVal; 
////		tmpFrac = tmpVal - tmpInt1;
////		tmpInt2 = tmpFrac * 100000;
////
////		sprintf(acc_msg_text + strlen(acc_msg_text), "z: %s%d.%05d\n", tmpSign, tmpInt1, tmpInt2);
//		
//		GPIO_PinState acc_int1 = HAL_GPIO_ReadPin(ACC_INT1_GPIO_Port, ACC_INT1_Pin);
//		GPIO_PinState acc_int2 = HAL_GPIO_ReadPin(ACC_INT2_GPIO_Port, ACC_INT2_Pin);
//		
//		if (acc_int2 == GPIO_PIN_SET) {
//			HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_SET);
//			HAL_Delay(10);
//			HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_RESET);
//		}
//		
//		//		
//		//		acc_msg_text[0] = (acc_int1 == GPIO_PIN_SET) ? '1' : '0';
//		//		acc_msg_text[1] = (acc_int2 == GPIO_PIN_SET) ? '1' : '0';
//		//		acc_msg_text[2] = '\r';
//		//		acc_msg_text[3] = '\n';
//		//		acc_msg_text[4] = '\0';
//		//		
//		//		message = debugConsole.create_message(reinterpret_cast<uint8_t*>(acc_msg_text), std::strlen(acc_msg_text)).value();
//		//		debugConsole.write(message);
//		std::optional<devices::Accelerometer::motion> motion = accelerometerHandle.get_motion_data();
//		auto asd = accelerometerHandle.read_register(devices::Accelerometer::REGISTERS::CTRL_REG5);
//		if (motion.value().flag_any()) {
//			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
//			HAL_Delay(10);
//			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
//		}
//	}
	
//	protocols::UART uartToGPS(&huart1, true);
//	devices::GPS gpsHandle(uartToGPS, GPS_WUP_GPIO_Port, GPS_WUP_Pin);
//	size_t read_bytes = 0;
//	char buffer[1024];
//	protocols::ISerial::STATUS status;
//	protocols::SerialMessage gpsToDebug = uartToGPS.create_message(nullptr, 1024).value();
//	std::optional<const devices::GPS::GPSData*> gps_data;
//	while (true) {
//		std::fill_n(buffer, 1024, '\0');
//		gpsHandle.read_and_parse();
//		auto last_update = gpsHandle.get_last_update();
//		
//		
//		std::sprintf(buffer, "20%u.%u.%u. %u:%u:%u ", last_update.date_time.segments.year, last_update.date_time.segments.month, last_update.date_time.segments.day, last_update.date_time.segments.hour, last_update.date_time.segments.minute, last_update.date_time.segments.second);
//		std::sprintf(buffer + strlen(buffer), "fix:%u ", last_update.fix);
//		
//		const char *tmpSign = (last_update.latitude < 0) ? "-" : "";
//		float tmpVal = (last_update.latitude < 0) ? -last_update.latitude : last_update.latitude;
//		
//		int tmpInt1 = tmpVal; 
//		float tmpFrac = tmpVal - tmpInt1;
//		int tmpInt2 = tmpFrac * 100000;
//		
//		std::sprintf(buffer + strlen(buffer), "latitude:%s%d.%05d ", tmpSign, tmpInt1, tmpInt2);
//		tmpSign = (last_update.longitude < 0) ? "-" : "";
//		tmpVal = (last_update.longitude < 0) ? -last_update.longitude : last_update.longitude;
//		
//		tmpInt1 = tmpVal; 
//		tmpFrac = tmpVal - tmpInt1;
//		tmpInt2 = tmpFrac * 100000;
//		
//		std::sprintf(buffer + strlen(buffer), "longitude:%s%d.%05d ", tmpSign, tmpInt1, tmpInt2);
//		std::sprintf(buffer + strlen(buffer), "satellites: %u\r\n", last_update.satellites);
//		gpsToDebug.set_payload(reinterpret_cast<uint8_t*>(buffer), strlen(buffer));
//		devices::DebugConsole::log(buffer);
//		
//		HAL_Delay(200);
//	}
	
//	HAL_GPIO_WritePin(LoRa_nRESET_GPIO_Port, LoRa_nRESET_Pin, GPIO_PIN_SET);
//	HAL_Delay(100);
//	protocols::UART uartToLoRa(&huart2);
//	protocols::SerialMessage message = uartToLoRa.create_message(nullptr, 35, 5000).value();
//	
//	protocols::SerialMessage debugMessage = debugConsole.create_message(nullptr, 100).value();
//	char buffer[100] = { 0 };
//	char debug_buffer[100] = { 0 };	
//	
//	std::sprintf(debug_buffer, "LoRa init: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	auto kek =debugConsole.write(debugMessage);
//	devices::LoRa loraHandle(uartToLoRa, 799, 8);
//	devices::LoRa::STATUS lora_status;
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting application identifier: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_application_identifier("BE0100000000010E");
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting device address: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_device_address("014690CB");
//	if(lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting network session key: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_network_session_key("7B496173EAB22399868EDE35CCCA1C8B");
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting application session key: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_application_session_key("831FF408F5E4CB93E20DF198D6FD9764");
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting class: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_class('c');
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	std::sprintf(debug_buffer, "OK\r\nSetting adaptive datarate: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.set_adaptive_datarate(true);
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	
//	std::sprintf(debug_buffer, "OK\r\nSaving settings: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.save_settings();
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	
//	std::sprintf(debug_buffer, "OK\r\nRestart LoRa chip: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.reset();
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	std::sprintf(debug_buffer, "OK\r\nInitialization finished!\r\nConnecting: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.connect(devices::LoRa::CONNECTION_MODE::ACTIVATION_BY_PERSONALIZATION);
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	
//	std::sprintf(debug_buffer, "OK\r\nSending packet: ");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	lora_status = loraHandle.send_data(devices::LoRa::TRANSMIT_TYPE::CONFIRMED, 1, "asd", 3);
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
////	HAL_Delay(100);	  
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
////	HAL_Delay(100);	  
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
////	HAL_Delay(100);	 
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
////	HAL_Delay(100);	  
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
////	HAL_Delay(100);	 
////	HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
////	HAL_Delay(2000);
////	lora_status = loraHandle.send_data(devices::LoRa::TRANSMIT_TYPE::CONFIRMED, 1, "lol", 3);
//	if (lora_status != devices::LoRa::STATUS::OK) debugConsole.write(debugMessage);
//	std::sprintf(debug_buffer, "OK");
//	debugMessage.set_payload(reinterpret_cast<uint8_t*>(debug_buffer), strlen(debug_buffer));
//	debugConsole.write(debugMessage);
//	debugMessage = debugConsole.create_message(nullptr, 100, 1000).value();
//	while (true)
//	{
//		auto msg = loraHandle.try_receive(1000);
//		if (msg.has_value()) {
//			uint8_t asd = 0;
//		}
//		HAL_Delay(50);
//	}
//	
//	
//	
//	char loraBuffer[512] = { 0 };
//	devices::LoRa::STATUS loraStatus = loraHandle.get_hweui(loraBuffer, 512);
	
//	HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, GPIO_PIN_SET);
//	protocols::CANBus a(&hcan1);
//	protocols::SerialMessage message = a.create_message(nullptr, 4, 100, 0).value();
//	while (true) {
//		uint8_t asd[4] = { 0 };
//		a.read(message);
//		std::copy_n(message.get_payload(), 4, asd);
//		HAL_Delay(200);
//		a.write(message);
//		HAL_Delay(1000);
//	}
	/* USER CODE END 2 */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of defaultTask */
//	osThreadStaticDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128, defaultTaskBuffer, &defaultTaskControlBlock);
//	defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

	/* definition and creation of sensorTask */
	osThreadStaticDef(sensorTask, sensorTask, osPriorityNormal, 0, 256, sensorTaskBuffer, &sensorTaskControlBlock);
	sensorTaskHandle = osThreadCreate(osThread(sensorTask), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* Start scheduler */
	osKernelStart();
  
	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
	{
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2
	                            | RCC_PERIPHCLK_USART3 | RCC_PERIPHCLK_I2C1
	                            | RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure the main internal regulator output voltage 
	*/
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

	/* USER CODE BEGIN ADC1_Init 0 */

	/* USER CODE END ADC1_Init 0 */

	ADC_MultiModeTypeDef multimode = { 0 };
	ADC_ChannelConfTypeDef sConfig = { 0 };

	/* USER CODE BEGIN ADC1_Init 1 */

	/* USER CODE END ADC1_Init 1 */
	/** Common config 
	*/
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = ENABLE;
	hadc1.Init.NbrOfConversion = 3;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.OversamplingMode = DISABLE;
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure the ADC multi-mode 
	*/
	multimode.Mode = ADC_MODE_INDEPENDENT;
	if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure Regular Channel 
	*/
	sConfig.Channel = ADC_CHANNEL_1;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure Regular Channel 
	*/
	sConfig.Channel = ADC_CHANNEL_3;
	sConfig.Rank = ADC_REGULAR_RANK_2;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure Regular Channel 
	*/
	sConfig.Channel = ADC_CHANNEL_15;
	sConfig.Rank = ADC_REGULAR_RANK_3;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN ADC1_Init 2 */

	/* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

	/* USER CODE BEGIN CAN1_Init 0 */

	/* USER CODE END CAN1_Init 0 */

	/* USER CODE BEGIN CAN1_Init 1 */

	/* USER CODE END CAN1_Init 1 */
	hcan1.Instance = CAN1;
	hcan1.Init.Prescaler = 8;
	hcan1.Init.Mode = CAN_MODE_NORMAL;
	hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan1.Init.TimeSeg1 = CAN_BS1_7TQ;
	hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
	hcan1.Init.TimeTriggeredMode = DISABLE;
	hcan1.Init.AutoBusOff = DISABLE;
	hcan1.Init.AutoWakeUp = DISABLE;
	hcan1.Init.AutoRetransmission = DISABLE;
	hcan1.Init.ReceiveFifoLocked = DISABLE;
	hcan1.Init.TransmitFifoPriority = DISABLE;
	if (HAL_CAN_Init(&hcan1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN CAN1_Init 2 */

	/* USER CODE END CAN1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x10909CEC;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure Analogue filter 
	*/
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure Digital filter 
	*/
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 9600;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART1_Init 2 */

	/* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 57600;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

	/* USER CODE BEGIN USART3_Init 0 */

	/* USER CODE END USART3_Init 0 */

	/* USER CODE BEGIN USART3_Init 1 */

	/* USER CODE END USART3_Init 1 */
	huart3.Instance = USART3;
	huart3.Init.BaudRate = 115200;
	huart3.Init.WordLength = UART_WORDLENGTH_8B;
	huart3.Init.StopBits = UART_STOPBITS_1;
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart3) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART3_Init 2 */

	/* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC,
		MBAT_MEAS_EN_Pin|LoRa_nRESET_Pin|BLE_RST_Pin|OUT2_TRG_Pin 
	                        |EN_BUZZER_Pin,
		GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, BLE_CS_Pin | GPS_WUP_Pin | CAN_EN_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, ANALOG1_EN_Pin | BBAT_MEAS_EN_Pin | SYS_LED_Pin | OUT1_TRG_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : PSU_VSELECTED_Pin IN1_TRG_Pin IN2_TRG_Pin */
	GPIO_InitStruct.Pin = PSU_VSELECTED_Pin | IN1_TRG_Pin | IN2_TRG_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : MBAT_MEAS_EN_Pin LoRa_nRESET_Pin BLE_RST_Pin OUT2_TRG_Pin 
	                         EN_BUZZER_Pin */
	GPIO_InitStruct.Pin = MBAT_MEAS_EN_Pin | LoRa_nRESET_Pin | BLE_RST_Pin | OUT2_TRG_Pin 
	                        | EN_BUZZER_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : BLE_CS_Pin GPS_WUP_Pin CAN_EN_Pin */
	GPIO_InitStruct.Pin = BLE_CS_Pin | GPS_WUP_Pin | CAN_EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : BLE_INT_Pin */
	GPIO_InitStruct.Pin = BLE_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BLE_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : ANALOG1_EN_Pin BBAT_MEAS_EN_Pin SYS_LED_Pin OUT1_TRG_Pin */
	GPIO_InitStruct.Pin = ANALOG1_EN_Pin | BBAT_MEAS_EN_Pin | SYS_LED_Pin | OUT1_TRG_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : ACC_INT2_Pin */
	GPIO_InitStruct.Pin = ACC_INT2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ACC_INT2_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : ACC_INT1_Pin */
	GPIO_InitStruct.Pin = ACC_INT1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ACC_INT1_GPIO_Port, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
    
    
    
    
    

	/* init code for BlueNRG_MS */
	MX_BlueNRG_MS_Init();

	/* USER CODE BEGIN 5 */
	/* Infinite loop */
	for (;;)
	{
		osDelay(1);
	}
	/* USER CODE END 5 */ 
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the sensorTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
	/* USER CODE BEGIN StartTask02 */
	/* Infinite loop */
	for (;;)
	{
		osDelay(1);
	}
	/* USER CODE END StartTask02 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM17 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM17) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	 /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
