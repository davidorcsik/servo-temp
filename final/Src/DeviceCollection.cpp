#include <DeviceCollection.h>

bool DeviceCollection::initialized = false;
protocols::UART		DeviceCollection::uartToGPS =	protocols::UART(&huart1, true);
protocols::UART		DeviceCollection::uartToLora =	protocols::UART(&huart2, false);
protocols::UART		DeviceCollection::uartToDebug =	protocols::UART(&huart3, false);
protocols::I2CBus	DeviceCollection::i2cBus =		protocols::I2CBus(&hi2c1);
protocols::CANBus	DeviceCollection::canBus =		protocols::CANBus(&hcan1);

devices::GPS		DeviceCollection::gps =					devices::GPS(uartToGPS, GPS_WUP_GPIO_Port, GPS_WUP_Pin);
devices::LoRa		DeviceCollection::loraHandle =			devices::LoRa(uartToLora, LoRa_nRESET_GPIO_Port, LoRa_nRESET_Pin);
devices::TempSensor	DeviceCollection::temperature_sensor =	devices::TempSensor(i2cBus, devices::TempSensor::DEVICE_ADDRESSES::A0);
devices::Accelerometer DeviceCollection::accelerometer =	devices::Accelerometer(i2cBus, devices::Accelerometer::DEVICE_ADDRESSES::SA0_LOW, devices::Accelerometer::RESOLUTION::RES_10BIT);
devices::PowerManager DeviceCollection::powerManager = devices::PowerManager(MBAT_MEAS_AD_GPIO_Port, MBAT_MEAS_AD_Pin, MBAT_MEAS_EN_GPIO_Port, MBAT_MEAS_EN_Pin, BBAT_MEAS_AD_GPIO_Port, BBAT_MEAS_AD_Pin, BBAT_MEAS_EN_GPIO_Port, BBAT_MEAS_EN_Pin, ANALOG1_AD_GPIO_Port, ANALOG1_AD_Pin, ANALOG1_EN_GPIO_Port, ANALOG1_EN_Pin, 12, 3.7);

void DeviceCollection::init()
{
	powerManager.init();
	bool debug_console_init_status = devices::DebugConsole::init(uartToDebug);
	if (!debug_console_init_status) {
		Utils::ERROR_GUARD();
	}
	
	devices::DebugConsole::log("CANBus init:");
	protocols::CANBus::STATUS can_init_status = canBus.init();
	if (can_init_status != protocols::CANBus::STATUS::OK) {
		devices::DebugConsole::log("ERROR!");
		Utils::ERROR_GUARD();
	}
	devices::DebugConsole::log("OK");
	
	
	devices::DebugConsole::log("GPS init:");
	bool gps_init_status = gps.init();
	if (!gps_init_status) {
		devices::DebugConsole::log("ERROR!");
		Utils::ERROR_GUARD();
	}
	devices::DebugConsole::log("OK");
	
	
	devices::DebugConsole::log("LoRa init:");
	devices::LoRa::STATUS lora_status = loraHandle.init(799, 8);
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("ERROR!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_application_identifier("BE0100000000010E");
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set application identifier!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_device_address("009C87CD"); //ny�k 1
//	lora_status = loraHandle.set_device_address("01BE1A7F"); //ny�k 2
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set device address!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_network_session_key("A38857981B5F9C2947FC939D5DFF0AAB"); // ny�k 1
//	lora_status = loraHandle.set_network_session_key("A2EB4F7290F0C5BD8F64CD2F5820A1D9"); // ny�k 2
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set network session key!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_application_session_key("1930B1670C92AB6D5AEC70C9E2F527FF"); //ny�k 1
//	lora_status = loraHandle.set_application_session_key("8F00344D1B0FF24D996500DF18B7B3DA"); //ny�k 2
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set application session key!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_class('c');
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set class!");
		Utils::ERROR_GUARD();
	}
	lora_status = loraHandle.set_adaptive_datarate(true);
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot set adaptive datarate!");
		Utils::ERROR_GUARD();
	}
//	lora_status = loraHandle.save_settings();
//	if (lora_status != devices::LoRa::STATUS::OK) {
//		devices::DebugConsole::log("Cannot save settings!");
//		Utils::ERROR_GUARD();
//	}
	lora_status = loraHandle.connect(devices::LoRa::CONNECTION_MODE::ACTIVATION_BY_PERSONALIZATION);
	if (lora_status != devices::LoRa::STATUS::OK) {
		devices::DebugConsole::log("Cannot connect to network!");
		Utils::ERROR_GUARD();
	}
	devices::DebugConsole::log("OK");
	
	
	devices::DebugConsole::log("Temperature sensor init:");
	devices::TempSensor::STATUS temp_sensor_init_status = temperature_sensor.set_resolution(devices::TempSensor::RESOLUTIONS::CELSIUS_05);
	if (temp_sensor_init_status != devices::TempSensor::STATUS::OK) {
		devices::DebugConsole::log("ERROR!");
		Utils::ERROR_GUARD();
	}
	devices::DebugConsole::log("OK");
	
	
	devices::DebugConsole::log("Accelerometer init:");
	devices::Accelerometer::STATUS acceletometer_init_status = accelerometer.init();
	if (acceletometer_init_status != devices::Accelerometer::STATUS::OK) {
		devices::DebugConsole::log("ERROR!");
		Utils::ERROR_GUARD();
	}
	devices::DebugConsole::log("OK");
}