/*
 * ble_common.h
 *
 *  Created on: Feb 9, 2019
 *      Author: mate_pc
 */


#ifndef BNRG_COMMON_H_
#define BNRG_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "string.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"

#include "bnrg_config.h"


typedef enum{							//peripheral state of the device
	p_disconnected = 0x00,
	p_connected = 0x01,
	p_idle = 0x02,
	p_advertising = 0x03,
	p_paired = 0x04,
}ble_peripheral_state_t;

typedef enum{							//central state of the device
	c_disconnected = 0x00,
	c_disconnecting = 0x01,
	c_scanning = 0x02,
	c_scanning_completed = 0x02,
	c_connecting = 0x03,
	c_connected = 0x04,
	c_pairing = 0x05,
	c_paired  = 0x06,
	c_service_discovery = 0x07,
	c_serivce_discovery_completed = 0x08,
	c_characteristic_discovery = 0x09,
	c_characteristic_discovery_completed = 0x0a,
	c_idle = 0x0b,
}ble_central_state_t;

typedef enum{							//procedure status of peripheral device
p_no_proc = 0x00,
p_pending_proc = 0x01,

}ble_peripheral_procedure_state_t;

typedef enum{							//procedure status of central device
c_no_proc = 0x00,
c_pending_proc = 0x01,

}ble_central_procedure_state_t;

typedef struct{
	uint8_t uuid[MAX_UUID_SIZE];		//maximum 16 byte
	uint8_t uuid_type;					//UUID_TYPE_128 or UUID_TYPE_16
	uint16_t handler;					//handler of the specific service
	uint16_t end_group_handler;			//end of service table
	uint8_t attr_max_value;				//maximum attribute record number in service
}service_t;

typedef struct{
	uint16_t service_handler;			//service handler
	uint16_t handler;					//handler of the characteristic to be add
	uint16_t value_handler;				//handler of the characteristic value
	uint8_t uuid[MAX_UUID_SIZE];		//uuid array of the characteristic (16 byte maxium)
	uint8_t uuid_type;					//UUID_TYPE_128 or UUID_TYPE_16
	uint8_t value_size;					//size of the value attribute in bytes
	uint8_t properties;					//porperties of the characteristic (read,write,notify,etc.)
	uint8_t authentication;				//authentication for characteristic
}characteristic_t;

typedef struct{							//uuid structure
	uint8_t array[MAX_UUID_SIZE];
	uint8_t size;
}uuid_t;

typedef struct{
	uint8_t addr[6];
}dev_MAC_t;

typedef struct{
	uint8_t io_cap;
	uint8_t mitm_mode;
	uint8_t use_fixed_pin;
	uint32_t fixed_pin;
	uint8_t bonding_mode;
}ble_security_t;

#define GUIDE_MAX_ATTR_VALUE 0x10
#define LINK_MAX_ATTR_VALUE 0x10

#define JOYSTICK_MAX_ATTR_VALUE 0x0d

/*
 * 1. service declaration
 * 2. settings characteristic declaration
 * 3. settings characteristic value
 * 4. button characteristic declaration
 * 5. button characteristic value
 * 6. (button characteristic) client characteristic configuration descriptor
 * 7. firmware characteristic declaration
 * 8. firmware characteristic value
 * 9. command characteristic declaration
 * 10.command characteristic value
 * 11.joystick characteristic declaration
 * 12.joystick characteristic value
 * 13.(joystick characteristic) client characteristic configuration descriptor
 */

#define BLE_MODE_PERIPHERAL GAP_PERIPHERAL_ROLE_IDB05A1
#define BLE_MODE_CENTRAL GAP_CENTRAL_ROLE_IDB05A1

typedef void (*ble_conn_cb_t) (uint8_t addr[6], uint16_t handle);										//callback types for specific callbacks
typedef void (*ble_disconn_cb_t)(uint16_t handler, uint8_t disconn_result);
typedef void (*ble_pairing_cmplt_cb_t)(uint16_t handler, uint8_t pairing_status);
typedef void (*ble_attr_mod_cb_t)(uint16_t attr_handler,uint8_t* attr_data,uint8_t attr_data_length);
typedef void(*ble_write_req_cb_t)(uint16_t conn_handler,uint16_t attr_handler,uint8_t* attr_data,uint8_t attr_data_length);
typedef void(*ble_dev_found_cb_t)(uint8_t dev_num,uint8_t* addr_array);
typedef void (*ble_serv_disc_cb_t)(uint16_t conn_handler,uint8_t event_data_length,uint8_t attribute_data_length,uint8_t* attribute_data);
typedef void(*ble_char_disc_cb_t)(uint16_t conn_handle, uint8_t  event_data_length,uint8_t  handle_value_pair_length, uint8_t * handle_value_pair);
typedef void (*ble_read_req_res_cb_t)(uint16_t conn_handle,uint8_t  event_data_length, uint8_t * attribute_value);
typedef void(*ble_notify_res_cb_t)(uint16_t conn_handle,uint8_t  event_data_length,uint16_t attr_handle, uint8_t  *attr_value);
typedef void(*ble_passkey_needed_cb_t)(uint32_t* passkey, uint16_t* conn_handler);
typedef void (*ble_read_req_cb_t)(uint16_t conn_handle,uint16_t attr_handle,uint8_t  data_length,uint16_t offset);

//////////////////////////////////////////////////////////////////////////////////////
////						Common functions									  ////
//////////////////////////////////////////////////////////////////////////////////////

tBleStatus ble_init_hw(uint8_t *addr, uint8_t addr_size,int mul_conn_enable);

tBleStatus ble_init_stack(uint8_t role, const char *board_name, uint8_t board_name_size, uint16_t *gap_service_handler, uint16_t *dev_name_char_handle, uint16_t *appearance_char_handle,uint8_t auth);

tBleStatus ble_service_init(uint8_t *uuid,uint8_t uuid_type,uint8_t attr_max_value, service_t *service);

tBleStatus ble_service_add(service_t *service);

tBleStatus ble_char_init(uint8_t *uuid, uint8_t uuid_type, uint8_t value_size,service_t service,uint8_t properties,uint8_t authentication,characteristic_t *characteristic);

tBleStatus ble_char_add(characteristic_t *characteristic);

uint8_t ble_get_role();

uint8_t ble_get_auth_req();

void uint8_array_to_uuid(uuid_t* uuid_result,uint8_t *uuid,uint8_t size);

tBleStatus ble_init_stack_extended(uint8_t role, const char *board_name, uint8_t board_name_size, uint16_t *gap_service_handler, uint16_t *dev_name_char_handle, uint16_t *appearance_char_handle, ble_security_t sec);

//////////////////////////////////////////////////////////////////////////////////////
////						Event callbacks										  ////
//////////////////////////////////////////////////////////////////////////////////////

void hci_event_cb(void* pData);

void ble_hci_event_cb_peripheral(void * pData);

void ble_hci_event_cb_central(void *pData);

#ifdef __cplusplus
}
#endif

#endif /* BNRG_COMMON_H_ */
