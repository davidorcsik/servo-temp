#pragma once

#include <stm32l4xx_hal_uart.h>
#include <stm32l4xx_hal_i2c.h>
#include <stm32l4xx_hal_can.h>

#include <UART.h>
#include <I2CBus.h>
#include <TemperatureSensor.h>
#include <Accelerometer.h>
#include <LoRa.h>
#include <GPS.h>
#include <CANBus.h>
#include <DebugConsole.h>
#include <power_manager.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern I2C_HandleTypeDef hi2c1;
extern CAN_HandleTypeDef hcan1;

class DeviceCollection {
private:
	static bool initialized;
public:
	static protocols::UART uartToGPS;
	static protocols::UART uartToLora;
	static protocols::UART uartToDebug;
	static protocols::CANBus canBus;
	static protocols::I2CBus i2cBus;
	static devices::GPS gps;
	static devices::LoRa loraHandle;
	static devices::TempSensor temperature_sensor;
	static devices::Accelerometer accelerometer;
	static devices::PowerManager powerManager;
	static void init();
};