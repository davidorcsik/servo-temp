/*
 * ble_central.h
 *
 *  Created on: Feb 15, 2019
 *      Author: mate_pc
 */

#ifndef BNRG_CENTRAL_H_
#define BNRG_CENTRAL_H_

#include "bnrg_common.h"
#ifdef __cplusplus
extern "C" {
#endif


void ble_set_max_num_of_scann(uint8_t num);

ble_central_procedure_state_t ble_get_central_proc_state();

uint8_t ble_is_found(uint8_t *addr,uint8_t size);

tBleStatus ble_enable_notification(uint16_t connection_handler,uint16_t characteristic_handler);

tBleStatus ble_disable_notification(uint16_t connection_handler,uint16_t characteristic_handler);

tBleStatus ble_start_general_connection();

tBleStatus ble_start_general_discovery();

tBleStatus ble_create_connection(uint8_t* addr,uint8_t addr_len);

tBleStatus ble_create_multiple_connection(uint8_t* addr,uint8_t addr_len);

tBleStatus ble_start_service_discovery(uint16_t connection_handler);

tBleStatus ble_start_char_discovery(uint16_t connection_handler,uint16_t service_handler,uint16_t service_end_handler);

tBleStatus ble_read_remote_char(uint16_t connection_handler,uint16_t char_val_handler);

ble_central_state_t ble_get_central_state();

tBleStatus ble_send_pairing_request(uint16_t connection_handler,uint8_t force_rebond);

void ble_terminate_procedure(uint8_t procedure_code);

void ble_diconnect(uint16_t conn_handler);

void ble_blocking_delay(uint32_t ms);

void ble_central_set_connection_complete_cb(ble_conn_cb_t cb);

void ble_central_set_disconnection_complete_cb(ble_disconn_cb_t cb);

void ble_central_set_pairing_complete_cb(ble_pairing_cmplt_cb_t cb);

void ble_central_set_device_found_cb(ble_dev_found_cb_t cb);

void ble_central_set_service_discovery_result_cb(ble_serv_disc_cb_t cb);

void ble_central_set_characteristic_discovery_result_cb(ble_char_disc_cb_t cb);

void ble_central_set_read_request_result_cb(ble_read_req_res_cb_t cb);

void ble_central_set_notification_result_cb(ble_notify_res_cb_t cb);

void ble_central_set_passkey_needed_cb(ble_passkey_needed_cb_t cb);


#ifdef __cplusplus
}
#endif
#endif /* BNRG_CENTRAL_H_ */
