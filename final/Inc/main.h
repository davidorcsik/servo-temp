/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

#include "hci_tl_interface.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PSU_VSELECTED_Pin GPIO_PIN_13
#define PSU_VSELECTED_GPIO_Port GPIOC
#define MBAT_MEAS_AD_Pin GPIO_PIN_0
#define MBAT_MEAS_AD_GPIO_Port GPIOC
#define MBAT_MEAS_EN_Pin GPIO_PIN_1
#define MBAT_MEAS_EN_GPIO_Port GPIOC
#define BBAT_MEAS_AD_Pin GPIO_PIN_2
#define BBAT_MEAS_AD_GPIO_Port GPIOC
#define LoRa_nRESET_Pin GPIO_PIN_3
#define LoRa_nRESET_GPIO_Port GPIOC
#define LoRa_TX_Pin GPIO_PIN_2
#define LoRa_TX_GPIO_Port GPIOA
#define LoRa_RX_Pin GPIO_PIN_3
#define LoRa_RX_GPIO_Port GPIOA
#define BLE_CS_Pin GPIO_PIN_4
#define BLE_CS_GPIO_Port GPIOA
#define BLE_SCK_Pin GPIO_PIN_5
#define BLE_SCK_GPIO_Port GPIOA
#define BLE_MISO_Pin GPIO_PIN_6
#define BLE_MISO_GPIO_Port GPIOA
#define BLE_MOSI_Pin GPIO_PIN_7
#define BLE_MOSI_GPIO_Port GPIOA
#define BLE_INT_Pin GPIO_PIN_4
#define BLE_INT_GPIO_Port GPIOC
#define BLE_INT_EXTI_IRQn EXTI4_IRQn
#define BLE_RST_Pin GPIO_PIN_5
#define BLE_RST_GPIO_Port GPIOC
#define ANALOG1_AD_Pin GPIO_PIN_0
#define ANALOG1_AD_GPIO_Port GPIOB
#define ANALOG1_EN_Pin GPIO_PIN_1
#define ANALOG1_EN_GPIO_Port GPIOB
#define BBAT_MEAS_EN_Pin GPIO_PIN_2
#define BBAT_MEAS_EN_GPIO_Port GPIOB
#define LOG_TX_Pin GPIO_PIN_10
#define LOG_TX_GPIO_Port GPIOB
#define LOG_RX_Pin GPIO_PIN_11
#define LOG_RX_GPIO_Port GPIOB
#define SYS_LED_Pin GPIO_PIN_14
#define SYS_LED_GPIO_Port GPIOB
#define OUT1_TRG_Pin GPIO_PIN_15
#define OUT1_TRG_GPIO_Port GPIOB
#define OUT2_TRG_Pin GPIO_PIN_6
#define OUT2_TRG_GPIO_Port GPIOC
#define IN1_TRG_Pin GPIO_PIN_7
#define IN1_TRG_GPIO_Port GPIOC
#define IN2_TRG_Pin GPIO_PIN_8
#define IN2_TRG_GPIO_Port GPIOC
#define EN_BUZZER_Pin GPIO_PIN_9
#define EN_BUZZER_GPIO_Port GPIOC
#define GPS_WUP_Pin GPIO_PIN_8
#define GPS_WUP_GPIO_Port GPIOA
#define GPS_TX_Pin GPIO_PIN_9
#define GPS_TX_GPIO_Port GPIOA
#define GPS_RX_Pin GPIO_PIN_10
#define GPS_RX_GPIO_Port GPIOA
#define CAN_RXD_Pin GPIO_PIN_11
#define CAN_RXD_GPIO_Port GPIOA
#define CAN_TXD_Pin GPIO_PIN_12
#define CAN_TXD_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define CAN_EN_Pin GPIO_PIN_15
#define CAN_EN_GPIO_Port GPIOA
#define ACC_INT2_Pin GPIO_PIN_4
#define ACC_INT2_GPIO_Port GPIOB
#define ACC_INT1_Pin GPIO_PIN_5
#define ACC_INT1_GPIO_Port GPIOB
#define ACC_INT1_EXTI_IRQn EXTI9_5_IRQn
#define SCL_Pin GPIO_PIN_6
#define SCL_GPIO_Port GPIOB
#define SDA_Pin GPIO_PIN_7
#define SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
