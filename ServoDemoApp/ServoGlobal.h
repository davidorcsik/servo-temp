#pragma once
#include <utils/servo.h>
#include <utils/utils.h>
enum AppState
{
	Init,
	Locked,
	Unlocked,
	Maintenance,
	ErrorMode
};
struct ReportData
{
	uint8_t appState;
	uint8_t deviceStatus;
	float lon;
	float lat;
	uint8_t speed;
	int16_t temp;
	float accX;
	float accY;
	float accZ;
	uint8_t battery1;
	uint8_t battery2;
	uint8_t extInputs;
	uint8_t extOutputs;
}; class ServoGlobal
{
  public:
	// Configuration constants
	static const uint8_t COMM_USE_BLE = 1;
	static const uint8_t COMM_USE_CAN = 1;
	static const uint8_t COMM_USE_LORA = 1;

	static const uint8_t DEV_GPS_INIT = 0x01;
	static const uint8_t DEV_GPS_FIX = 0x02;
	static const uint8_t DEV_LORA_INIT = 0x04;
	static const uint8_t DEV_ACC_INIT = 0x08;
	static const uint8_t DEV_ACC_ALARM = 0x10;
	static const uint8_t DEV_TEMP_INIT = 0x20;
	static const uint8_t DEV_CAN_INIT = 0x40;
	static const uint8_t DEV_UART_INIT = 0x80;

	static const size_t LORA_BUFFER_SIZE = 128;

	static Utils::STORAGE_DATA_STRUCT cfgConfig;
	
  public:
	// Buffers
	static uint8_t buffLoraBuffer[LORA_BUFFER_SIZE];

  public:
	// States
	static uint8_t stSensorState;
	static AppState stAppState;

	// Time Periods
	static uint16_t perReportPeriod;
	static uint16_t perSensorPeriod;

	static osThreadId tskSensorHandle;
	static osThreadId tskCommInHandle;
	static osThreadId tskCommOutHandle;
	static osThreadId tskMainHandle;
	static osThreadId tskLedHandle;

	// BLE Services
	static BatteryService srvBatteryService;
	static CommandService srvCmdService;
	static ConfigurationService srvConfigService;
	static DeviceInfoService srvDevInfoService;
	static FirmwareService srvFirmwareService;
	static TelemetryService srvTelemetryService;

	// Data containers - App
	static ReportData dReportData;
	static devices::GPS::GPSData dGPSData;

	// Data containers - CAN
	static Utils::CANStructs::Acceleration dAccelerationData;
	static Utils::CANStructs::Battery dBatteryData;
	static Utils::CANStructs::External dExtIoData;
	static Utils::CANStructs::Status dStatusData;
	static Utils::CANStructs::Temperature dTemperatureData;
	
};
