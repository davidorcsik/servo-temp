#pragma once

#include <stm32l4xx_hal.h>
#include <string.h>
#include <usart.h>
#ifdef DEBUG
extern UART_HandleTypeDef LOG_PORT;
#define LOG(msg) HAL_UART_Transmit(&LOG_PORT,(uint8_t*)msg, strlen(msg),100);

#else
#define LOG(msg)
#endif // DEBUG
