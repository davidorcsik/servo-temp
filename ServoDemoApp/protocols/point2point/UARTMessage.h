#pragma once

#include <SerialMessage.h>
#include <ISerial.h>

#include <inttypes.h>

namespace protocols {
	class UARTMessage : public SerialMessage {
	protected:
		UARTMessage() : SerialMessage(nullptr, 0, 0, 0, 0, class_id) {}
		UARTMessage(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout)
		: SerialMessage(_payload, _payload_length, _timeout, 0, 0, class_id) {}
	public:
		virtual ~UARTMessage() {}
		
		static std::optional<SerialMessage> create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT);
		static const uint8_t class_id = 1;
	};
}