#include <UART.h>
#include <UARTMessage.h>
#include <utils.h>

#include <vector>

namespace protocols
{
	UART::STATUS UART::read(SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return STATUS::INVALID_MESSAGE_TYPE;
		}
		if (message.get_payload_length() > BUFFER_SIZE) {
			return protocols::ISerial::STATUS::INVALID_MESSAGE_LENGTH;
		}
		
		int byte_count = 0;
		HAL_StatusTypeDef result = HAL_OK;
		for (byte_count = 0; byte_count < message.get_payload_length(); byte_count++)
		{
			result = HAL_UART_Receive(uartHandle, buffer + (byte_count), 1, message.get_timeout());
			if (is_interrupt_required) {
				HAL_UART_Abort_IT(uartHandle);
			}
			if (result != HAL_OK) {    
				break;
			}
		}
		buffer[byte_count] = '\0';
		message.set_payload(buffer, byte_count + 1);
		return static_cast<STATUS>(result);
	}
	
	UART::STATUS UART::write(const SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return STATUS::INVALID_MESSAGE_TYPE;
		}
		const HAL_StatusTypeDef result = HAL_UART_Transmit(this->uartHandle, const_cast<uint8_t*>(message.get_payload()), message.get_payload_length(), message.get_timeout());
		return static_cast<STATUS>(result);
	}
	
	bool UART::is_message_type_valid(const SerialMessage& message) const {
		return message.get_class_id() == UARTMessage::class_id;
	}
	
	std::optional<SerialMessage> UART::create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address, const uint8_t _register) {
		return UARTMessage::create(_payload, _payload_length, _timeout);
	}
}

