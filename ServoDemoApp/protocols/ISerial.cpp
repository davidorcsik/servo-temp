#include <ISerial.h>

#include <cstring>

namespace protocols
{
	const size_t ISerial::DEFAULT_READ_LENGTH = 100;
	const uint32_t ISerial::DEFAULT_TIMEOUT = 5000;
	const uint32_t ISerial::DEFAULT_ADDRESS = 0;
	const uint8_t ISerial::DEFAULT_REGISTER = 0;
	
	bool ISerial::is_message_type_valid(const SerialMessage& message) const {
		return false;
	}
	std::optional<SerialMessage> ISerial::create_message(const char* _payload, const uint32_t _timeout, const uint32_t _address, const uint8_t _register) {
		return create_message(reinterpret_cast<const uint8_t*>(_payload), strlen(_payload), _timeout, _address, _register);
	}
}