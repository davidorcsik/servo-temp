#include <SerialMessage.h>

#include <cstring>

#include <stm32l4xx_hal.h>

namespace protocols {
	const uint8_t* SerialMessage::get_payload() const {
		return payload;
	}
	uint32_t SerialMessage::get_payload_length() const {
		return payload_length;
	}
	uint32_t SerialMessage::get_timeout() const {
		return timeout;
	}
	SerialMessage::STATUS SerialMessage::set_timeout(uint32_t _timeout) {
		timeout = _timeout;
		return STATUS::OK;
	}
	uint8_t SerialMessage::set_payload(const uint8_t* _payload, const uint32_t _payload_length) {
		if (_payload_length > MAX_PAYLOAD_LENGTH) return 0;
		std::copy_n(_payload, _payload_length, payload);
		payload_length = _payload_length;
		return _payload_length;
	}
	uint8_t SerialMessage::set_payload(const char* _payload) {
		return set_payload(reinterpret_cast<const uint8_t*>(_payload), strlen(_payload));
	}
	
	SerialMessage::STATUS SerialMessage::is_contructor_parameters_valid(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout) {
		if (_payload_length < 0 || _payload_length > MAX_PAYLOAD_LENGTH) {
			return SerialMessage::STATUS::INVALID_PAYLOAD_LENGTH;
		}
		if (_timeout < 0 || _timeout > HAL_MAX_DELAY) {
			return SerialMessage::STATUS::INVALID_TIMEOUT;
		}
		return STATUS::OK;
	}
	SerialMessage::STATUS SerialMessage::set_payload_length(const uint32_t _payload_length) {
		if (_payload_length < 0 || _payload_length > MAX_PAYLOAD_LENGTH) {
			return SerialMessage::STATUS::INVALID_PAYLOAD_LENGTH;
		}
		payload_length = _payload_length;
		return STATUS::OK;
	}
	
	uint8_t SerialMessage::get_class_id() const {
		return class_id;
	}
	
	uint32_t SerialMessage::get_device_address() const {
		return device_address;
	}
	SerialMessage::STATUS SerialMessage::set_device_address(uint32_t _device_address) {
		device_address = _device_address;
		return STATUS::OK;
	}
	uint8_t SerialMessage::get_register_address() const {
		return register_address;
	}
	SerialMessage::STATUS SerialMessage::set_register_address(uint8_t _register_address) {
		register_address = _register_address;
		return STATUS::OK;
	}
}