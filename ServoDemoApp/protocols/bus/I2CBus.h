#pragma once
#include <ISerial.h>

#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <semphr.h>

namespace protocols {
	class I2C : public ISerial {
	private:
		I2C_HandleTypeDef* const i2cHandle;
		SemaphoreHandle_t i2cHandleMutex;
	protected:
		bool is_message_type_valid(const SerialMessage& message) const override;
	public:
		I2C() : i2cHandle(nullptr) {}
		I2C(I2C_HandleTypeDef* const _i2cHandle)
		: i2cHandle(_i2cHandle) {
			i2cHandleMutex = xSemaphoreCreateRecursiveMutex();
		}
		
		virtual ~I2C() {}
		
		virtual STATUS read(SerialMessage& message) override;
		
		virtual STATUS write(const SerialMessage& message) override;
		
		virtual std::optional<SerialMessage> create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS, const uint8_t _register = ISerial::DEFAULT_REGISTER) override;
	};
}