#pragma once

#include <SerialMessage.h>
#include <ISerial.h>

namespace protocols {
	class I2CMessage : public SerialMessage {
	protected:
		I2CMessage() : SerialMessage(nullptr, 0, 0, 0, 0, class_id) {}
		I2CMessage(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint8_t _device_address, const uint8_t _register_address)
		: SerialMessage(_payload, _payload_length, _timeout, _device_address, _register_address, class_id) {}
	public:
		virtual ~I2CMessage() {}
		static std::optional<SerialMessage> create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS, const uint8_t _register = 0);
		static const uint8_t class_id = 2;
	};
}