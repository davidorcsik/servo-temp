#include "CANBus.h"
#include "CANBusMessage.h"

namespace protocols {
	std::map<CAN_HandleTypeDef*, CANBus*> CANBus::instances = std::map<CAN_HandleTypeDef*, CANBus*>();
	void CANBus::rx_callback(CAN_HandleTypeDef* canHandle) {
		CANBus::instances[canHandle]->on_receive();
	}
	CANBus::STATUS CANBus::init() {
		canHandleMutex = xSemaphoreCreateRecursiveMutex();
			
		txHeader.ExtId = 0x01;
		txHeader.RTR = CAN_RTR_DATA;
		txHeader.IDE = CAN_ID_STD;
		txHeader.TransmitGlobalTime = DISABLE;
				
		filter.FilterBank = 0;
		filter.FilterMode = CAN_FILTERMODE_IDMASK;
		filter.FilterScale = CAN_FILTERSCALE_32BIT;
		filter.FilterIdHigh = 0x0000;
		filter.FilterIdLow = 0x0000;
		filter.FilterMaskIdHigh = 0x0000;
		filter.FilterMaskIdLow = 0x0000;
		filter.FilterFIFOAssignment = CAN_RX_FIFO0;
		filter.FilterActivation = ENABLE;
		filter.SlaveStartFilterBank = 14;
		if (HAL_CAN_ConfigFilter(canHandle, &filter) != HAL_OK)
		{
			while (1) {
				HAL_Delay(1);
			}
		}
		if (HAL_CAN_Start(canHandle) != HAL_OK)
		{
			/* Start Error */

			while (1) {
				HAL_Delay(1);
			}
		}
		//		if (HAL_CAN_ActivateNotification(canHandle, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
		//		{
		//			/* Notification Error */
		//			while (1) {
		//				HAL_Delay(1);
		//			}
		//		}
		return STATUS::OK;
	}
	CANBus::STATUS CANBus::read(SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return protocols::ISerial::STATUS::INVALID_MESSAGE_TYPE;
		}
		if (message.get_payload_length() > BUFFER_SIZE) {
			return protocols::ISerial::STATUS::INVALID_MESSAGE_LENGTH;
		}
		
		if (xSemaphoreTakeRecursive(canHandleMutex, static_cast<TickType_t>(message.get_timeout()) == pdTRUE)) {
			HAL_StatusTypeDef status = HAL_CAN_GetRxMessage(canHandle, CAN_RX_FIFO0, &rxHeader, buffer);
			message.set_payload(buffer, message.get_payload_length());
			xSemaphoreGiveRecursive(canHandleMutex);
			return static_cast<CANBus::STATUS>(status);
		}
		return STATUS::COULDNT_LOCK_HARDWARE;
	}
	
	CANBus::STATUS CANBus::write(const SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return ISerial::STATUS::INVALID_MESSAGE_TYPE;
		}
		
		txHeader.DLC = message.get_payload_length();
		txHeader.StdId = message .get_device_address();
		if (xSemaphoreTakeRecursive(canHandleMutex, static_cast<TickType_t>(message.get_timeout()) == pdTRUE)) {
			HAL_StatusTypeDef status = HAL_CAN_AddTxMessage(canHandle, &txHeader, const_cast<uint8_t*>(message.get_payload()), &txMailbox);
			xSemaphoreGiveRecursive(canHandleMutex);
			return static_cast<STATUS>(status);
		}
		return STATUS::COULDNT_LOCK_HARDWARE;
	}
	
	std::optional<SerialMessage> CANBus::create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address, const uint8_t _register) {
		return CANBusMessage::create(_payload, _payload_length, _timeout, _address);
	}
	
	void CANBus::on_receive() {
		if (xSemaphoreTakeRecursive(canHandleMutex, static_cast<TickType_t>(lastMessage.get_timeout()) == pdTRUE)) {
			HAL_StatusTypeDef status = HAL_CAN_GetRxMessage(canHandle, CAN_RX_FIFO0, &rxHeader, buffer);
			lastMessage.set_payload(buffer, lastMessage.get_payload_length());
			xSemaphoreGiveRecursive(canHandleMutex);
		}
	}
	
	bool CANBus::is_message_type_valid(const SerialMessage& message) const {
		return message.get_class_id() == CANBusMessage::class_id;
	}
}
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *canHandle) {
	protocols::CANBus::rx_callback(canHandle);
}