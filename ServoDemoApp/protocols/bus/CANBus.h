#pragma once
#include <ISerial.h>
#include <utils.h>

#include <stm32l4xx_hal.h>
#include <stm32l4xx_hal_can.h>
#include <FreeRTOS.h>
#include <semphr.h>

#include <map>

namespace protocols {
	class CANBus : public ISerial {
	private:
		SemaphoreHandle_t canHandleMutex;
		CAN_HandleTypeDef* canHandle;
		CAN_TxHeaderTypeDef txHeader;
		CAN_RxHeaderTypeDef rxHeader;
		CAN_FilterTypeDef filter;
		uint32_t txMailbox;
		SerialMessage lastMessage;
		static std::map<CAN_HandleTypeDef*, CANBus*> instances;
	protected:
		bool is_message_type_valid(const SerialMessage& message) const override;
	public:
		CANBus(CAN_HandleTypeDef* _canHandle)
		: canHandle(_canHandle) {
			instances.insert({canHandle, this});
		}
		virtual ~CANBus() {}
		virtual STATUS init();
		virtual STATUS read(SerialMessage& message) override;
		virtual STATUS write(const SerialMessage& message) override;
		virtual void on_receive();
		
		virtual std::optional<SerialMessage> create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS, const uint8_t _register = ISerial::DEFAULT_REGISTER) override;
		
		static void rx_callback(CAN_HandleTypeDef* canHandle);
	};
}