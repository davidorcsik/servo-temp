#include "I2CMessage.h"

namespace protocols {
	std::optional<SerialMessage> I2CMessage::create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address, const uint8_t _register) {
		if ((_address & 0xffffff1f) > 0x1f) {
			return {};
		}
		
		STATUS status = SerialMessage::is_contructor_parameters_valid(_payload, _payload_length, _timeout);
		if (status != STATUS::OK) {
			return {};
		}
		
		I2CMessage message(_payload, _payload_length, _timeout, _address, _register);
		return { message };
	}
}