#pragma once
#include <stm32l4xx_hal.h>
#include <cmsis_os.h>

// Device includes
#include "BLEDevice/BLEDevice.h"
#include "BLEDevice/characteristics/BLECharacteristic.h"
#include "BLEDevice/services/BLEService.h"
#include "BLEDevice/services/BatteryService.h"
#include "BLEDevice/services/CommandService.h"
#include "BLEDevice/services/ConfigurationService.h"
#include "BLEDevice/services/DeviceInfoService.h"
#include "BLEDevice/services/FirmwareService.h"
#include "BLEDevice/services/TelemetryService.h"

#include "accelerometer/accelerometer.h"
#include "tempsensor/TemperatureSensor.h"
#include "gps/GPS.h"
#include "lora/LoRa.h"
#include "debugconsole/DebugConsole.h"

// Data types
#include "utils/utils.h"

#include <DeviceCollection.h>
#include "ServoGlobal.h"

#ifdef DEBUG
#define LOG(msg) devices::DebugConsole::log(msg);
#else 
#define LOG(msg)
#endif // DEBUG
