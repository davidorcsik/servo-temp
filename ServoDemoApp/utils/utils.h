#pragma once
#include <array>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <float.h>

#include <stm32l4xx_hal.h>

namespace Utils
{
#pragma pack(push, 1)
	union DateTime
	{
		struct DateTimeStruct
		{
			// 32
			uint32_t year : 6;      //63
			uint32_t month : 4;      // 12 
			uint32_t day : 5;          //31    
			uint32_t hour : 5;      //23
			uint32_t minute : 6;      //59
			uint32_t second : 6;      //59
		} segments;
		uint32_t value;
	};
#pragma pack(pop)
	
	DateTime get_current_time();
	
	constexpr std::array<char, 16> hexchars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	size_t byte_array_to_hex_string(const char* bytes, const size_t length, char* output_buffer, const size_t output_buffer_length);
	size_t byte_array_to_hex_string(const uint8_t* bytes, const size_t length, uint8_t* output_buffer, const size_t output_buffer_length);
	size_t hex_string_to_byte_array(const char* hex_string, uint8_t* output, const size_t& output_length);
	int32_t atoi_with_bounds(const char* input, const size_t& start_index = 0, size_t length = 0);
	double atof_with_bounds(const char* input, const size_t& start_index = 0, const size_t& length = 0);
	size_t cstring_contains(const char* input, const size_t input_length, const char* value, const size_t value_length);
	
	uint8_t convert_uchararr_to_chararr(const uint8_t* source, const size_t& source_length, char* destination);
	
	void ERROR_GUARD();
	
	bool in_bounds_unsigned(uint32_t low_bound, uint32_t high_bound, uint32_t value);
	bool in_bounds_signed(int32_t low_bound, int32_t high_bound, int32_t value);
	
	namespace CANStructs {
		struct Temperature {
			float measured_temperature;
			Temperature() {}
			Temperature(float _measured_temperature) : measured_temperature(_measured_temperature) {}
		};
		struct Battery {
			uint16_t battery_1;
			uint16_t battery_2;
			Battery() {}
			Battery(uint16_t _battery_1, uint16_t _battery_2) : battery_1(_battery_1), battery_2(_battery_2) {}
		};
		struct Status {
			uint8_t device_status;
			uint8_t mode;
			uint8_t speed;
			Status() {}
			Status(uint8_t _device_status, uint8_t _mode, uint8_t _speed) : device_status(_device_status), mode(_mode), speed(_speed) {}
		};
		struct Acceleration {
			uint32_t xyz;
			Acceleration() {}
			Acceleration(uint32_t _xyz) : xyz(_xyz) {}
		};
		struct External {
			uint8_t digital_in;
			uint8_t digital_out;
			External() {}
			External(uint32_t _digital_in, uint32_t _digital_out) : digital_in(_digital_in), digital_out(_digital_out) {}
		};
	}
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//--------------------------------------------------------------------------------------------------------------------//
	//--------------------------------------------------------------------------------------------------------------------//
	// DO NOT MODIFY THE ORDER OF ATTRIBUTES IN THIS STRUCT OR THE DATA READ FROM FLASH WILL BE GARBAGE UNTIL NEXT WRITE! //
	// DO NOT MODIFY THE ORDER OF ATTRIBUTES IN THIS STRUCT OR THE DATA READ FROM FLASH WILL BE GARBAGE UNTIL NEXT WRITE! // 
	// DO NOT MODIFY THE ORDER OF ATTRIBUTES IN THIS STRUCT OR THE DATA READ FROM FLASH WILL BE GARBAGE UNTIL NEXT WRITE! //
	//--------------------------------------------------------------------------------------------------------------------//
	//--------------------------------------------------------------------------------------------------------------------//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct STORAGE_DATA_STRUCT {
		//LoRa
		char lora_application_identifier[16] = { 0 };
		char lora_devide_address[8] = { 0 };
		char lora_network_session_key[32] = { 0 };
		char lora_application_session_key[32] = { 0 };
		char lora_device_class = '\0';
			
		//GPS
		uint32_t gps_nmea_sentences = 0;
		
		//Bluetooth
		uint32_t pin;
		
		// Periods
		
	};
	
	
	const uint32_t STORAGE_START_MEMORY_ADDRESS = 0x080ffc00;   //decreasing addresses
	const uint16_t STORAGE_MAX_SIZE = 1024;   //in bytes
	
	bool storage_write_data(STORAGE_DATA_STRUCT* data);
	bool storage_read_data(STORAGE_DATA_STRUCT *data);
	
	void system_restart();

	struct OPERATION_MODE
	{
		enum class MODES { OFF, ON, SERVICE };
		static uint32_t OFF_LoRa_SEND_FREQ;
		static GPIO_PinState OFF_DIGIO_STATE;
		static uint32_t ON_LoRa_SEND_FREQ;
		static GPIO_PinState ON_DIGIO_STATE;
		static uint32_t SERVICE_LoRa_SEND_FREQ;
		static GPIO_PinState SERVICE_DIGIO_STATE;
		
		static uint32_t get_lora_send_freq(MODES mode);
		static GPIO_PinState get_digio_state(MODES mode);
	};
}