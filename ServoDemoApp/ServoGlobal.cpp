#include "ServoGlobal.h"

// Config
Utils::STORAGE_DATA_STRUCT ServoGlobal::cfgConfig;

// States
uint8_t ServoGlobal::stSensorState = 0;
AppState ServoGlobal::stAppState = AppState::Init;

// Periods
uint16_t ServoGlobal::perReportPeriod = 100;
uint16_t ServoGlobal::perSensorPeriod = 10;

// Task handles
osThreadId ServoGlobal::tskSensorHandle = nullptr;
osThreadId ServoGlobal::tskCommInHandle = nullptr;
osThreadId ServoGlobal::tskCommOutHandle = nullptr;
osThreadId ServoGlobal::tskMainHandle = nullptr;
osThreadId ServoGlobal::tskLedHandle = nullptr;

BatteryService ServoGlobal::srvBatteryService;
CommandService ServoGlobal::srvCmdService;
ConfigurationService ServoGlobal::srvConfigService;
DeviceInfoService ServoGlobal::srvDevInfoService;
FirmwareService ServoGlobal::srvFirmwareService;
TelemetryService ServoGlobal::srvTelemetryService;


// Containers
uint8_t ServoGlobal::buffLoraBuffer[ServoGlobal::LORA_BUFFER_SIZE];
ReportData ServoGlobal::dReportData;
devices::GPS::GPSData ServoGlobal::dGPSData;

Utils::CANStructs::Acceleration ServoGlobal::dAccelerationData;
Utils::CANStructs::Battery ServoGlobal::dBatteryData;
Utils::CANStructs::External ServoGlobal::dExtIoData;
Utils::CANStructs::Status ServoGlobal::dStatusData;
Utils::CANStructs::Temperature ServoGlobal::dTemperatureData;