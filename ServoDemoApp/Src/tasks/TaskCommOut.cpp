#include <utils/servo.h>
extern "C" void TaskCommOut(void const * argument)
{
	osEvent evt;
	for (;;)
	{
		evt =osSignalWait(0x02, 1000);
		if (evt.status == osEventSignal)
		{
			LOG("Comm - ALARM sending")
			devices::LoRa::STATUS loraRes = DeviceCollection::loraHandle.send_data(devices::LoRa::TRANSMIT_TYPE::CONFIRMED, 11, "12", 2);
			if (loraRes != devices::LoRa::STATUS::OK)
			{
				__NOP();
			}
			LOG("Comm - Alarm sent");
		}	
	}
}