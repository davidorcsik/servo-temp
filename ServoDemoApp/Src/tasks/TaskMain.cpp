#include <utils/servo.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

	uint16_t counter = 0;	
	void TaskMain(void const * argument)
	{
		bool sendData;
		DeviceCollection::init();

		DeviceCollection::bleDevice.Init();
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvBatteryService);
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvCmdService);
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvConfigService);
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvFirmwareService);
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvTelemetryService);
		DeviceCollection::bleDevice.addService(&ServoGlobal::srvDevInfoService);
		DeviceCollection::bleDevice.setPasskey(ServoGlobal::cfgConfig.pin);
		DeviceCollection::bleDevice.startAdvertising();

		DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, 10, 19);

		osSignalSet(ServoGlobal::tskLedHandle, 1);

		for (;;)
		{
			// GPS Stuff
			ServoGlobal::dGPSData = DeviceCollection::gps.get_last_update();
			ServoGlobal::dReportData.lat = ServoGlobal::dGPSData.latitude;
			ServoGlobal::dReportData.lon = ServoGlobal::dGPSData.longitude;
			ServoGlobal::dReportData.speed = ServoGlobal::dGPSData.speed;
			ServoGlobal::dReportData.speed = ServoGlobal::dGPSData.speed;

			// BLE State machine
			DeviceCollection::bleDevice.startAdvertising();
			DeviceCollection::bleDevice.runStateMachine();
			if (DeviceCollection::bleDevice.isPaired())
			{
				ServoGlobal::srvBatteryService.updateBatteryValue(counter % 100);
			}

			// Task management
			if ((counter % ServoGlobal::perSensorPeriod) == 0)
			{
				// Sensor task wakeup
				osSignalSet(ServoGlobal::tskSensorHandle, 0x02);
			}

			counter++;
			if (counter > 10000)
			{
				counter = 1;
			}
			if (DeviceCollection::accelerometer.get_motion_data().value().flag_any())
			{
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
				LOG("Main - ALARM");
				osSignalSet(ServoGlobal::tskCommOutHandle, 0x02);
				osDelay(1000);
			}
			else
			{
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
			}
			osDelay(100);
		}
	}
	
	
	
#ifdef __cplusplus
}
#endif // __cplusplus