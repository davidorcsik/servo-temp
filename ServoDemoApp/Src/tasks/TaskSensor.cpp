﻿
#include <utils/servo.h>

extern "C" void TaskSensor(void const * argument)
{
	osEvent evt;

	for (;;)
	{
		evt = osSignalWait(0x02, 1000);
		if (evt.status == osEventSignal)
		{
			
			LOG("Sensor - Task activated");
			ServoGlobal::dReportData.temp = DeviceCollection::tempSensor.get_temp().value();
			ServoGlobal::dReportData.accX = DeviceCollection::accelerometer.read_acceleration().value().x;
			ServoGlobal::dReportData.accY = DeviceCollection::accelerometer.read_acceleration().value().y;
			ServoGlobal::dReportData.accZ = DeviceCollection::accelerometer.read_acceleration().value().z;
			//		//ServoGlobal::accelerationData.xyz = DeviceCollection::accelerometer.read_acceleration().value();
			//		// TODO: ANALOG
			//		ServoGlobal::dReportData.battery1 = 0;
			//		ServoGlobal::dReportData.battery2 = 0;
			//
			//		ServoGlobal::dReportData.extInputs = DeviceCollection::in1.getValue()<<1 | DeviceCollection::in2.getValue();
			//		ServoGlobal::dReportData.extOutputs = DeviceCollection::out1.getValue() << 1 | DeviceCollection::out2.getValue();
			osDelay(500);

			LOG("Sensor - Task stopping");
		}
	}
}