#include <utils/servo.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus


void SingleBlink(uint16_t ms);
void DoubleBlink(uint16_t ms);	
void TaskLED(void const * argument)
{
	/* USER CODE BEGIN TaskMain */
	/* Infinite loop */
	osEvent evt;
	for (;;)
	{
		HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
		//osDelay(100);
		evt =osSignalWait(1, 100);
		if (evt.status == osEventSignal)
		{
			LOG("LED - Init done");
			break;
		}
	}

	for (;;)
	{
		HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
		osDelay(500);
	}
	/* USER CODE END TaskMain */
}

void SingleBlink(uint16_t ms) {
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_SET);
	osDelay(ms / 2);
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port, SYS_LED_Pin, GPIO_PIN_RESET);
	}
void DoubleBlink(uint16_t ms) {
	SingleBlink(ms / 3);
	osDelay(ms / 3);
	SingleBlink(ms / 3);
}

#ifdef __cplusplus
}
#endif // __cplusplus
