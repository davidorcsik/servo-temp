/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId xtLEDHandle;
uint32_t xtLEDBuffer[ 128 ];
osStaticThreadDef_t xtLEDControlBlock;
osThreadId xtSensorHandle;
uint32_t xtSensorBuffer[ 128 ];
osStaticThreadDef_t xtSensorControlBlock;
osThreadId xtCommInHandle;
uint32_t xtCommInBuffer[ 128 ];
osStaticThreadDef_t xtCommInControlBlock;
osThreadId xtCommOutHandle;
uint32_t xtCommOutBuffer[ 128 ];
osStaticThreadDef_t xtCommOutControlBlock;
osThreadId xtMainHandle;
uint32_t xtMainBuffer[ 128 ];
osStaticThreadDef_t xtMainControlBlock;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void TaskLED(void const * argument);
extern void TaskSensor(void const * argument);
extern void TaskCommIn(void const * argument);
extern void TaskCommOut(void const * argument);
extern void TaskMain(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];
  
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}                   
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of xtLED */
  osThreadStaticDef(xtLED, TaskLED, osPriorityLow, 0, 128, xtLEDBuffer, &xtLEDControlBlock);
  xtLEDHandle = osThreadCreate(osThread(xtLED), NULL);

  /* definition and creation of xtSensor */
  osThreadStaticDef(xtSensor, TaskSensor, osPriorityNormal, 0, 128, xtSensorBuffer, &xtSensorControlBlock);
  xtSensorHandle = osThreadCreate(osThread(xtSensor), NULL);

  /* definition and creation of xtCommIn */
  osThreadStaticDef(xtCommIn, TaskCommIn, osPriorityNormal, 0, 128, xtCommInBuffer, &xtCommInControlBlock);
  xtCommInHandle = osThreadCreate(osThread(xtCommIn), NULL);

  /* definition and creation of xtCommOut */
  osThreadStaticDef(xtCommOut, TaskCommOut, osPriorityIdle, 0, 128, xtCommOutBuffer, &xtCommOutControlBlock);
  xtCommOutHandle = osThreadCreate(osThread(xtCommOut), NULL);

  /* definition and creation of xtMain */
  osThreadStaticDef(xtMain, TaskMain, osPriorityIdle, 0, 128, xtMainBuffer, &xtMainControlBlock);
  xtMainHandle = osThreadCreate(osThread(xtMain), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_TaskLED */
/**
  * @brief  Function implementing the xtLED thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_TaskLED */
__weak void TaskLED(void const * argument)
{
    
    
    
    
    
    
    

  /* init code for BlueNRG_MS */
  MX_BlueNRG_MS_Init();

  /* USER CODE BEGIN TaskLED */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskLED */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
