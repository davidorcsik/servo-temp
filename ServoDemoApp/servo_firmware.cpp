﻿#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"
#include "app_x-cube-ble1.h"
#include "servo_firmware.h"

#include "servo.h"
#include "devices/DeviceCollection.h"
#include <utils/utils.h>

/*ITTEN HIBÁT KAPSZ, error: expected ‘}’ at end of input
 *MEGOLDÁS: custom_errno.h végére :
	#ifdef __cplusplus
	}
	#endif
 * hiányzik a generált file-ból a lezárás	
 * 
 *Következő hiba: DEBUG MACRO redefined
 *MEGOLDÁS:
 *bluenrg_conf.h-ban:
 *rename DEBUG 0 to DEBUG_BLE 0 refactorral
 */

int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */
  

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_CAN1_Init();
	MX_USART1_UART_Init();
	MX_I2C1_Init();
	MX_USART2_UART_Init();
	MX_ADC1_Init();
	MX_USART3_UART_Init();
	MX_DMA_Init();
	/* USER CODE BEGIN 2 */
	ServoGlobal::cfgConfig.pin = 123456;
	ServoGlobal::perReportPeriod = 100;
	ServoGlobal::perSensorPeriod = 50;

	/* Create the thread(s) */
	/* definition and creation of xtMain */
	MX_FREERTOS_Init();

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */
	extern osThreadId xtSensorHandle;
	extern osThreadId xtCommInHandle;
	extern osThreadId xtCommOutHandle;
	extern osThreadId xtMainHandle;
	extern osThreadId xtLEDHandle;
	ServoGlobal::tskSensorHandle = xtSensorHandle;
	ServoGlobal::tskCommInHandle = xtCommInHandle;
	ServoGlobal::tskCommOutHandle = xtCommOutHandle;
	ServoGlobal::tskMainHandle = xtMainHandle;
	ServoGlobal::tskLedHandle = xtLEDHandle;

	DeviceCollection::init();
	Utils::OPERATION_MODE::MODES operation_mode = (DeviceCollection::in1.getValue()) ? Utils::OPERATION_MODE::MODES::OFF : Utils::OPERATION_MODE::MODES::ON;

	const uint32_t lora_transmit_period = Utils::OPERATION_MODE::get_lora_send_freq(operation_mode);
	const uint32_t accelerometer_alarm_cutoff_period = 600000;
	const uint8_t alarm_debounce_treshold = 100;
	const uint8_t alarm_motion_treshold = 40;
	
	uint32_t lora_transmit_timer = 0;
	uint32_t accelerometer_alarm_cutoff_timer = 0;
	bool accelerometer_alarm = false;

	DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, alarm_debounce_treshold, alarm_motion_treshold);
	DeviceCollection::loraHandle.send_data(devices::LoRa::TRANSMIT_TYPE::CONFIRMED, 2, "START", 5);
	uint32_t reference_time = HAL_GetTick();
	HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
	DeviceCollection::bleDevice.Init();
	DeviceCollection::bleDevice.addService(&ServoGlobal::srvBatteryService);
//	DeviceCollection::bleDevice.addService(&ServoGlobal::srvCmdService);
//	DeviceCollection::bleDevice.addService(&ServoGlobal::srvConfigService);
//	DeviceCollection::bleDevice.addService(&ServoGlobal::srvFirmwareService);
	DeviceCollection::bleDevice.addService(&ServoGlobal::srvTelemetryService);
//	DeviceCollection::bleDevice.addService(&ServoGlobal::srvDevInfoService);
	DeviceCollection::bleDevice.setPasskey(ServoGlobal::cfgConfig.pin);
	DeviceCollection::bleDevice.startAdvertising();
	
	while (true) {
		uint32_t current_time = HAL_GetTick();
		lora_transmit_timer += current_time - reference_time;
		accelerometer_alarm_cutoff_timer += current_time - reference_time;
		reference_time = current_time;

		Utils::OPERATION_MODE::MODES operation_mode_new = (DeviceCollection::in1.getValue()) ? Utils::OPERATION_MODE::MODES::OFF : Utils::OPERATION_MODE::MODES::ON;
		if (operation_mode != operation_mode_new) {
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
		}
		operation_mode = operation_mode_new;

		HAL_GPIO_WritePin(OUT1_TRG_GPIO_Port, OUT1_TRG_Pin, Utils::OPERATION_MODE::get_digio_state(operation_mode));
		HAL_GPIO_WritePin(OUT2_TRG_GPIO_Port, OUT2_TRG_Pin, Utils::OPERATION_MODE::get_digio_state(operation_mode));
		
		devices::GPS::GPSData gps_data = DeviceCollection::gps.read_and_parse();

		if (lora_transmit_timer >= lora_transmit_period) {
			lora_transmit_timer = 0;
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);

			char gps_adenuis_data[128] = {0};
			if (gps_data.fix > 2) {
				DeviceCollection::gps.get_last_update_as_adeunis_alternative(0xd2, DeviceCollection::tempSensor.get_temp().value_or(16), DeviceCollection::powerManager.get_main_battery_voltage_in_milivolts(), gps_adenuis_data, 128);
			} else {
				std::sprintf(gps_adenuis_data, "00");
			}
			DeviceCollection::loraHandle.send_adenuis(gps_adenuis_data);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(SYS_LED_GPIO_Port, SYS_LED_Pin);
		}

		if (!accelerometer_alarm) {
			std::optional<devices::Accelerometer::motion> motion = DeviceCollection::accelerometer.get_motion_data();
			if (motion.value().flag_any()) {
				accelerometer_alarm = true;
				accelerometer_alarm_cutoff_timer = 0;
				DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::DISABLED);
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_SET);
				HAL_Delay(1000);
				HAL_GPIO_WritePin(EN_BUZZER_GPIO_Port, EN_BUZZER_Pin, GPIO_PIN_RESET);
			}
		} else {
			++accelerometer_alarm_cutoff_timer;
			if (accelerometer_alarm_cutoff_timer >= accelerometer_alarm_cutoff_period) {
				accelerometer_alarm = false;
				DeviceCollection::accelerometer.set_alarm(devices::Accelerometer::ALARM_STATE::MOTION, alarm_debounce_treshold, alarm_motion_treshold);
			}
		}

		// BLE State machine
		DeviceCollection::bleDevice.startAdvertising();
		DeviceCollection::bleDevice.runStateMachine();
		if (DeviceCollection::bleDevice.isPaired()) {
			ServoGlobal::srvBatteryService.updateBatteryValue(DeviceCollection::powerManager.get_main_battery_percentage());
		}

		uint8_t telemetry_data[18];
		telemetry_data[0] = gps_data.latitude_whole;
		telemetry_data[1] = gps_data.latitude_minutes;
		telemetry_data[2] = static_cast<uint8_t>(gps_data.latitude_seconds);
		telemetry_data[3] = static_cast<uint8_t>((gps_data.latitude_seconds - telemetry_data[2]) * 10) << 4 || (gps_data.latitude_pole == 'N') ? 0 : 1;
		telemetry_data[4] = gps_data.longitude_whole;
		telemetry_data[5] = gps_data.longitude_minutes;
		telemetry_data[6] = static_cast<uint8_t>(gps_data.latitude_seconds);
		telemetry_data[7] = static_cast<uint8_t>((gps_data.longitude_seconds - telemetry_data[6]) * 10) << 4 || (gps_data.latitude_pole == 'E') ? 0 : 1;
		*reinterpret_cast<uint16_t *>(telemetry_data + 8) = DeviceCollection::tempSensor.get_temp_raw().value_or(0);
		telemetry_data[10] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_Z_LSB).value_or(0);
		telemetry_data[11] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_Z_MSB).value_or(0);
		telemetry_data[12] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_Y_LSB).value_or(0);
		telemetry_data[13] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_Y_MSB).value_or(0);
		telemetry_data[14] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_X_LSB).value_or(0);
		telemetry_data[15] = DeviceCollection::accelerometer.read_register(devices::Accelerometer::REGISTERS::OUT_X_MSB).value_or(0);
		telemetry_data[16] = gps_data.speed;
		telemetry_data[17] = 0;

		ServoGlobal::srvTelemetryService.updateNotificationData(telemetry_data, 18);

		HAL_Delay(100);
	}
	
	
	
	
	
//	osThreadSuspend(ServoGlobal::tskSensorHandle);
//	osThreadSuspend(ServoGlobal::tskCommInHandle);
//	osThreadSuspend(ServoGlobal::tskCommOutHandle);
	
	
//	ServoGlobal::bleDevice = devices::BLEDevice(bleAddress, "ServoDemo", devices::BLERole::Peripheral, devices::BLEMode::Single, 1,123456);
//
//	BatteryService batteryService;
//	DeviceInfoService deviceInfoService;
//	CommandService commandService;
//
//	devices::BLEStatus res=ServoGlobal::bleDevice.Init();
//	if (res == devices::BLEStatus::OK)
//	{
//		
//		ServoGlobal::bleDevice.addService(&batteryService);
//		ServoGlobal::bleDevice.addService(&deviceInfoService);
//		ServoGlobal::bleDevice.addService(&commandService);
//
//		ServoGlobal::bleDevice.startAdvertising(); 
//	}
//	else
//	{
//		volatile uint8_t errorCode = ServoGlobal::bleDevice.getLastError();
//	}
//	uint8_t batt = 0;
//	while (1)
//	{
//		ServoGlobal::bleDevice.runStateMachine();
//		batteryService.updateBatteryValue(batt);
//		HAL_Delay(100);
//		if (batt < 99)
//		{
//			batt++;
//		}
//		else
//		{
//			batt = 0;
//		}
//	}
	//	BLEService batteryService;
	//	BLECharacteristic batteryChar;
	//	batteryService.

	/* Start scheduler */
	osKernelStart();
  
	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}





