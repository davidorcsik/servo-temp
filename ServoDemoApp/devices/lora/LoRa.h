#pragma once
#include <ISerial.h>
#include <utils.h>

#include <utility>

namespace devices
{
	class LoRa
	{
	  public:
#pragma pack(push, 1)
		union DEVICE_STATUS
		{
			struct Segments
			{
				uint32_t rfu : 14;
				uint32_t multicast_status : 1;
				uint32_t rejoin_needed : 1;
				uint32_t rx_timing_setup_updated : 1;
				uint32_t second_receive_window_parameters_updated : 1;
				uint32_t prescaler_updated : 1;
				uint32_t nbrep_updated : 1;
				uint32_t output_power_updated : 1;
				uint32_t channels_updated : 1;
				uint32_t link_check_status : 1;
				uint32_t rx_done_status : 1;
				uint32_t mac_pause_status : 1;
				uint32_t silent_immediately_status : 1;
				uint32_t automatic_reply_status : 1;
				uint32_t join_status : 1;
				uint32_t mac_state : 4;
			};
			uint32_t value;
		};
#pragma pack(pop)
		struct RX_MESSAGE
		{
			char data[128] = {0};
			uint8_t port;
			size_t data_length;
			RX_MESSAGE() : port(0), data_length(0) {}
			RX_MESSAGE(uint8_t _port, char *_data, size_t _data_length) : port(_port), data_length(_data_length)
			{
				std::copy_n(_data, _data_length, data);
			}
		};
		enum class STATUS
		{
			OK = HAL_OK,
			HARDWARE_ERROR = HAL_ERROR,
			HARDWARE_BUSY = HAL_BUSY,
			HARDWARE_TIMEOUT = HAL_TIMEOUT,
			WRITE_ERROR,
			READ_ERROR,
			COULD_NOT_CREATE_MESSAGE,
			INVALID_PARAMETER,
			COMMAND_ERROR,
			INVALID_DATA_LENGTH,
			RX_MESSAGE_RECEIVED,
		};
		enum class CONNECTION_MODE
		{
			OVER_THE_AIR_ACTIVATION,
			ACTIVATION_BY_PERSONALIZATION
		};
		enum class TRANSMIT_TYPE
		{
			CONFIRMED,
			UNCONFIRMED,
		};

	  private:
		static const std::pair<uint16_t, uint16_t> duty_cycle_bounds;
		static const std::pair<uint16_t, uint16_t> channel_id_bounds;
		static const std::pair<uint8_t, uint8_t> datarate_bounds;
		static const uint32_t channel_frequency_start = 867100000;
		static const uint32_t channel_frequency_step = 200000;
		static bool is_channel_id_valid(uint8_t channel);
		static const char *OK_REPLY;
		static const char *SUCCESSFULLY_CONNECTED_REPLY;
		static const char *SUCCESSFULLY_SENT_REPLY;
		static const char *RX_MESSAGE_PREFIX;
		static const uint16_t BUFFER_LENGTH = 256;
		static const uint32_t NORMAL_TIMEOUT = 50;
		static const uint16_t SEND_DATA_PAYLOAD_MAX_LENGTH = BUFFER_LENGTH - 17 - 2 - 1; //buffer length - max command segment before data [mac tx uncnf 233 ] (17) - CRLF - nullterminator
		GPIO_TypeDef *const gpio_enable_port;
		const uint16_t gpio_enable_pin;
		protocols::ISerial &serialHandle;
		protocols::SerialMessage serialMessage;
		char buffer[BUFFER_LENGTH];
		char receive_buffer[256];
		STATUS last_error;
		RX_MESSAGE last_rx_message;
		STATUS set_channel_frequency(uint8_t channel, uint32_t frequency);
		STATUS send_message(char *buffer, size_t buffer_length, const char *expected_reply = nullptr, uint32_t timeout = NORMAL_TIMEOUT, bool expect_rx = true); //if expected_reply is nullptr then reply check is skipped
		STATUS check_reply(const char *reply, const char *expected_reply);
		void parse_rx_message(const char *rx_message, size_t rx_message_length);

	  public:
		LoRa(protocols::ISerial &_serialHandle, GPIO_TypeDef *const _gpio_enable_port, uint16_t _gpio_enable_pin)
			: serialHandle(_serialHandle), gpio_enable_port(_gpio_enable_port), gpio_enable_pin(_gpio_enable_pin)
		{
			std::optional<protocols::SerialMessage> created_message = serialHandle.create_message(nullptr, 0, 250, protocols::ISerial::DEFAULT_ADDRESS);
			if (!created_message.has_value())
			{
				Utils::ERROR_GUARD();
			}
			serialMessage = created_message.value();
		}
		STATUS init(uint16_t duty_cycle, uint8_t channel_count);
		STATUS reset();
		STATUS get_hweui(char *reply, size_t buffer_length);
		std::optional<LoRa::DEVICE_STATUS> get_device_status();
		STATUS set_datarate(uint8_t datarate);
		STATUS set_datarate_range(uint8_t channel, uint8_t min_datarate, uint8_t max_datarate);
		std::optional<uint8_t> get_datarate();
		STATUS send_data(TRANSMIT_TYPE type, uint8_t port, const uint8_t *data, size_t data_length, uint8_t datarate = 0);
		STATUS send_data(TRANSMIT_TYPE type, uint8_t port, const char *data, size_t data_length, uint8_t datarate = 0);
		STATUS set_adaptive_datarate(bool enabled);
		std::optional<bool> get_adaptive_datarate();
		STATUS set_duty_cycle(uint8_t channel, uint16_t duty_cycle);
		std::optional<uint16_t> get_duty_cycle(uint8_t channel);
		STATUS set_drrange(uint8_t channel, uint8_t min_range, uint8_t max_range);
		STATUS set_channel_status(uint8_t channel, bool enabled);
		std::optional<bool> get_channel_status(uint8_t channel);
		STATUS set_device_address(const char *address);
		STATUS get_device_address(char *device_address, size_t device_address_length);
		STATUS set_network_session_key(const char *session_key);
		STATUS set_application_key(const char *application_key);
		STATUS set_application_session_key(const char *session_key);
		STATUS set_application_identifier(const char *appeui);
		STATUS get_application_identifier(char *buffer, size_t buffer_length);
		STATUS set_class(const char class_key);
		std::optional<char> get_class();
		STATUS save_settings();
		STATUS restore_factory_settings();
		STATUS connect(CONNECTION_MODE mode);
		RX_MESSAGE get_last_rx_message();
		std::optional<RX_MESSAGE> try_receive(uint32_t timeout);
		STATUS send_adenuis(const char *adenuis_buffer);
		void sync_uart();
	};
}