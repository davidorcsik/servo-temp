#pragma once
#include <stm32l4xx_hal.h>

namespace devices
{
	class PowerManager {
	public:
	  static const constexpr float MAIN_BATTERY_MAX_VOLTAGE = 12;
	  static const constexpr float BACKUP_BATTERY_MAX_VOLTAGE = 3.7;
	private:
	  static const constexpr uint16_t ADC_SETTLE_COUNT = 64;
	  static const constexpr uint8_t ADC_DEFAULT_RESOLUTION = 12;
	  static const constexpr float ADC_DEFAULT_REFERENCE_VOLTAGE = 3;
	  static const constexpr float MAIN_BATTERY_DEFAULT_DIVISION_RATIO = 0.025;
	  static const constexpr float BACKUP_BATTERY_DEFAULT_DIVISION_RATIO = 0.5;
	  static const constexpr float ANALOG_DEFAULT_DIVISION_RATIO = 0.25;
	  static const constexpr float MAIN_BATTERY_RAW_VALUE_CORRECTION = 1.023; //900. / 735.; //ny�k: M
	  static const constexpr float ANALOG_RAW_VALUE_CORRECTION = 1.058; //ny�k: M

	  const float main_battery_max_voltage;
	  const float backup_battery_max_voltage;

	  const float main_battery_division_ratio;
	  const float backup_battery_division_ratio;
	  const float analog_division_ratio;

	  const float adc_reference_voltage;
	  const uint8_t adc_resolution;
	  
	  GPIO_TypeDef *main_battery_adc_port;
	  uint16_t main_battery_adc_pin;
	  uint32_t main_battery_raw_value = 0;

	  GPIO_TypeDef *main_battery_adc_en_port;
	  uint16_t main_battery_adc_en_pin;

	  GPIO_TypeDef *backup_battery_adc_port;
	  uint16_t backup_battery_adc_pin;
	  uint32_t backup_battery_raw_value = 0;

	  GPIO_TypeDef *backup_battery_adc_en_port;
	  uint16_t backup_battery_adc_en_pin;

	  GPIO_TypeDef *analog_adc_port;
	  uint16_t analog_adc_pin;
	  uint32_t analog_raw_value = 0;

	  GPIO_TypeDef *analog_adc_en_port;
	  uint16_t analog_adc_en_pin;
	  void measure();
	  float inline convert_raw_value_to_voltage(uint32_t raw_value, float division_ratio, float raw_correction) const;

	public:
	  PowerManager(
		  GPIO_TypeDef *_main_battery_adc_port, uint16_t _main_battery_adc_pin,
		  GPIO_TypeDef *_main_battery_adc_en_port, uint16_t _main_battery_adc_en_pin,
		  GPIO_TypeDef *_backup_battery_adc_port, uint16_t _backup_battery_adc_pin,
		  GPIO_TypeDef *_backup_battery_adc_en_port, uint16_t _backup_battery_adc_en_pin,
		  GPIO_TypeDef *_analog_adc_port, uint16_t _analog_adc_pin,
		  GPIO_TypeDef *_analog_adc_en_port, uint16_t _analog_adc_en_pin,
		  float _main_battery_max_voltage, float _backup_battery_max_voltage,
		  float _main_battery_division_ratio = MAIN_BATTERY_DEFAULT_DIVISION_RATIO, float _backup_battery_division_ratio = BACKUP_BATTERY_DEFAULT_DIVISION_RATIO, float _analog_division_ratio = ANALOG_DEFAULT_DIVISION_RATIO,
		  float _adc_reference_voltage = ADC_DEFAULT_REFERENCE_VOLTAGE, uint8_t _adc_resolution = ADC_DEFAULT_RESOLUTION)
		  : main_battery_adc_port(_main_battery_adc_en_port), main_battery_adc_pin(_main_battery_adc_pin),
			main_battery_adc_en_port(_main_battery_adc_en_port), main_battery_adc_en_pin(_main_battery_adc_en_pin),
			backup_battery_adc_port(_backup_battery_adc_port), backup_battery_adc_pin(_backup_battery_adc_pin),
			backup_battery_adc_en_port(_backup_battery_adc_en_port), backup_battery_adc_en_pin(_backup_battery_adc_en_pin),
			analog_adc_port(_analog_adc_port), analog_adc_pin(_analog_adc_pin),
			analog_adc_en_port(_analog_adc_en_port), analog_adc_en_pin(_analog_adc_en_pin),
			main_battery_max_voltage(_main_battery_max_voltage), backup_battery_max_voltage(_backup_battery_max_voltage),
			main_battery_division_ratio(_main_battery_division_ratio), backup_battery_division_ratio(_backup_battery_division_ratio), analog_division_ratio(_analog_division_ratio),
			adc_reference_voltage(_adc_reference_voltage), adc_resolution(_adc_resolution){}

	  void init();
	  float get_main_battery_voltage_in_volts();
	  uint16_t get_main_battery_voltage_in_milivolts();
	  float get_backup_battery_voltage_in_volts();
	  uint16_t get_backup_battery_voltage_in_milivolts();
	  uint8_t get_main_battery_percentage();
	  uint8_t get_backup_battery_percentage();
	  float get_analog_voltage_in_volts();
	  uint16_t get_analog_voltage_in_milivolts();
	};
}