#include "GPS.h"
#include <utils.h>

#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

namespace devices
{
	const std::unordered_map<std::string, std::function<void(const char *, uint8_t, GPS::GPSData &)>> GPS::command_parser_map = {
		{"GPGGA", &GPS::parse_gga},
		{"GNGGA", &GPS::parse_gga},
		{"GPGSA", &GPS::parse_gsa},
		{"GNGSA", &GPS::parse_gsa},
		{"GPRMC", &GPS::parse_rmc},
		{"GNRMC", &GPS::parse_rmc},
		{"GPVTG", &GPS::parse_vtg},
		{"GNVTG", &GPS::parse_vtg},
		{"HCHDG", &GPS::parse_hchdg}};
	const std::array<uint32_t, 5> GPS::ENABLED_NMEA_SENTENCES = {
		NMEA_SENTENCES::GPGGA,
		NMEA_SENTENCES::GPGSA,
		NMEA_SENTENCES::GPRMC,
		NMEA_SENTENCES::GPVTG};
	const std::array<char, 9> GPS::COMMAND_DELIMETERS = {'*', ',', '!', '$', '\\', '^', '~', '\r', '\n'};
	uint32_t GPS::calculate_checksum(const char *command, uint8_t command_length)
	{
		uint32_t checksum = 0;
		for (size_t i = 1; i < command_length && command[i] != '*'; ++i)
		{ // starting from position 1 because the $ sign is not part of the calculation
			checksum ^= command[i];
		}
		return checksum;
	}
	bool GPS::is_checksum_valid(const char *command, uint8_t command_length)
	{
		const char *checksum_simbol_pos = std::find(command, command + command_length, '*');
		if (checksum_simbol_pos == command + command_length)
		{
			return false;
		}
		const uint32_t received_checksum = std::strtoul(&*(checksum_simbol_pos + 1), nullptr, 16);
		return calculate_checksum(command, command_length) == received_checksum;
	}

	uint8_t GPS::get_command_name(char *command, uint8_t length, char *command_name)
	{
		char *first_delimeter_pos = std::find_first_of(command + 1, command + length, GPS::COMMAND_DELIMETERS.cbegin(), GPS::COMMAND_DELIMETERS.cend());
		uint8_t command_name_length = std::distance(command + 1, first_delimeter_pos);
		char *command_name_end_pos = std::copy_n(command + 1, command_name_length, command_name);
		command_name[command_name_length] = '\0';
		return 0;
	}

	void GPS::parse_gga(const char *command, uint8_t length, GPSData &data)
	{
		char *token = std::strtok(const_cast<char *>(command) + 1, COMMAND_DELIMETERS.data());
		token = std::strtok(nullptr, COMMAND_DELIMETERS.data()); //skipping command name

		// time
		const double time_raw = std::atof(token);
		const uint32_t time_raw_whole = static_cast<uint32_t>(time_raw);
		const uint8_t hours = time_raw_whole / 10000;
		data.date_time.segments.hour = hours;
		const uint8_t minutes = (time_raw_whole - hours * 10000) / 100;
		data.date_time.segments.minute = minutes;
		const uint8_t seconds = time_raw_whole - hours * 10000 - minutes * 100;
		data.date_time.segments.second = seconds;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// latitude
		const double latitude_raw = std::atof(token);
		const uint8_t latitude_angle_whole = static_cast<uint8_t>(latitude_raw / 100);
		const uint8_t latitude_angle_minutes = static_cast<uint8_t>(latitude_raw - latitude_angle_whole * 100);
		const double latitude_angle_seconds = (latitude_raw - latitude_angle_whole * 100 - latitude_angle_minutes) * 60;
		const double latitude_polarized = latitude_angle_whole + latitude_angle_minutes / 60.0 + latitude_angle_seconds / 3600.0;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char latitude_pole = *token;
		data.latitude = ((latitude_pole == 'N') ? 1 : -1) * latitude_polarized;
		data.latitude_whole = latitude_angle_whole;
		data.latitude_minutes = latitude_angle_minutes;
		data.latitude_seconds = latitude_angle_seconds;
		data.latitude_pole = latitude_pole;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// longitude
		const double longitude_raw = std::atof(token);
		const uint8_t longitude_angle_whole = static_cast<uint16_t>(longitude_raw / 100);
		const uint8_t longitude_angle_minutes = static_cast<uint8_t>(longitude_raw - longitude_angle_whole * 100);
		const double longitude_angle_seconds = (longitude_raw - longitude_angle_whole * 100 - longitude_angle_minutes) * 60;
		const double longitude_polarized = longitude_angle_whole + longitude_angle_minutes / 60.0 + longitude_angle_seconds / 3600.0;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char longitude_pole = *token;
		data.longitude = ((longitude_pole == 'E') ? 1 : -1) * longitude_polarized;
		data.longitude_whole = longitude_angle_whole;
		data.longitude_minutes = longitude_angle_minutes;
		data.longitude_seconds = longitude_angle_seconds;
		data.longitude_pole = longitude_pole;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());
		// fix
		//const uint8_t fix = std::strtol(token, nullptr, 10);
		//data.fix = fix;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// satelites
		const uint8_t satellites = std::atoi(token);
		data.satellites = satellites;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// horizontal dilution of position
		//const double horizontal_dilution = std::strtol(token, nullptr, 10);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// altitude
		const double altitude = std::atoi(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char altitude_unit = *token;
		const double feat_to_meter_multiplier = 0.3048;
		data.altitude = ((altitude_unit == 'M') ? 1 : feat_to_meter_multiplier) * altitude; // if necessary converting ft to meter

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// mean sea level
		//const double mean_sea_level = std::stdtod(token, nullptr);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		//const char mean_sea_level_unit = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// time since last DGPS update
		//const std::string time_since_last_dgps_update(token); //empty

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// DPGS station id
		//const std::string dgpd_station_id(token); //empty

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// checksum
		//const std::string checksum(token); //checksum is a hex string
	}
	void GPS::parse_gsa(const char *command, uint8_t length, GPSData &data)
	{
		char *token = std::strtok(const_cast<char *>(command) + 1, COMMAND_DELIMETERS.data());
		token = std::strtok(nullptr, COMMAND_DELIMETERS.data()); //skipping command name

		// auto selection of 2D or 3D fix (A = auto, M = manual)
		//const char auto_selection = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// fix
		const uint8_t fix = std::atoi(token);
		data.fix = fix;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// satellite PRNs

		uint8_t empty_satelite_pna_count = 0;
		for (int i = 1; command[i] != '\0'; ++i)
		{
			if (command[i - 1] == ',' && command[i] == ',')
			{
				++empty_satelite_pna_count;
			}
		}

		const uint8_t satellite_prn_count = 12 - empty_satelite_pna_count; // defined by specifies space for 12
		for (uint8_t i = 0; i < satellite_prn_count; ++i)
		{
			//const std::string satellite_prn(token);
			token = std::strtok(nullptr, COMMAND_DELIMETERS.data());
		}

		// PDOP (dilution of precision)
		const double dilution_of_precision = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Horizontal dilution of precision (HDOP)
		//const double horizontal_dilution_of_precision = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Vertical dilution of precision (VDOP)
		//const double vertical_dilution_of_precision = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Checksum
		//const std::string checksum(token);
	}

	void GPS::parse_rmc(const char *command, uint8_t length, GPSData &data)
	{
		char *token = std::strtok(const_cast<char *>(command) + 1, COMMAND_DELIMETERS.data());
		token = std::strtok(nullptr, COMMAND_DELIMETERS.data()); //skipping command name
		// time
		const double time_raw = std::atof(token);
		const uint32_t time_raw_whole = static_cast<uint32_t>(time_raw);
		const uint8_t hours = time_raw_whole / 10000;
		data.date_time.segments.hour = hours;
		const uint8_t minutes = (time_raw_whole - hours * 10000) / 100;
		data.date_time.segments.minute = minutes;
		const uint8_t seconds = time_raw_whole - hours * 10000 - minutes * 100;
		data.date_time.segments.second = seconds;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// status (A = active, V = void)
		//const char status = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// latitude
		const double latitude_raw = std::atof(token);
		const uint8_t latitude_angle_whole = static_cast<uint8_t>(latitude_raw / 100);
		const uint8_t latitude_angle_minutes = static_cast<uint8_t>(latitude_raw - latitude_angle_whole * 100);
		const double latitude_angle_seconds = (latitude_raw - latitude_angle_whole * 100 - latitude_angle_minutes) * 60;
		const double latitude_polarized = latitude_angle_whole + latitude_angle_minutes / 60.0 + latitude_angle_seconds / 3600.0;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char latitude_pole = *token;
		data.latitude = ((latitude_pole == 'N') ? 1 : -1) * latitude_polarized;
		data.latitude_whole = latitude_angle_whole;
		data.latitude_minutes = latitude_angle_minutes;
		data.latitude_seconds = latitude_angle_seconds;
		data.latitude_pole = latitude_pole;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// longitude
		const double longitude_raw = std::atof(token);
		const uint8_t longitude_angle_whole = static_cast<uint16_t>(longitude_raw / 100);
		const uint8_t longitude_angle_minutes = static_cast<uint8_t>(longitude_raw - longitude_angle_whole * 100);
		const double longitude_angle_seconds = (longitude_raw - longitude_angle_whole * 100 - longitude_angle_minutes) * 60;
		const double longitude_polarized = longitude_angle_whole + longitude_angle_minutes / 60.0 + longitude_angle_seconds / 3600.0;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char longitude_pole = *token;
		data.longitude = ((longitude_pole == 'E') ? 1 : -1) * longitude_polarized;
		data.longitude_whole = longitude_angle_whole;
		data.longitude_minutes = longitude_angle_minutes;
		data.longitude_seconds = longitude_angle_seconds;
		data.longitude_pole = longitude_pole;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// speed over the ground in knots (not ground speed)
		//const double speed = std::atoi(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// track angle
		//const double track_angle = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// date
		const uint32_t date_raw = std::atoi(token);
		const uint8_t day = date_raw / 10000;
		data.date_time.segments.day = day;
		const uint8_t month = (date_raw - day * 10000) / 100;
		data.date_time.segments.month = month;
		const uint8_t year = date_raw - day * 10000 - month * 100;
		data.date_time.segments.year = year;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// magnetic variation
		//const double magnetic_variation = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		//const char magnetic_variation_pole = *token;

		//token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// checksum
		//const std::string checksum(token);
	}

	void GPS::parse_vtg(const char *command, uint8_t length, GPSData &data)
	{
		char *token = std::strtok(const_cast<char *>(command) + 1, COMMAND_DELIMETERS.data());
		token = std::strtok(nullptr, COMMAND_DELIMETERS.data()); //skipping command name

		// True track made good (degrees)
		//const double angle = std::strtol(token, nullptr);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		//const char track_indicator = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Magnetic track made good
		//const double magnetic_track = std::atof(token);
		//const char magnetic_track_indicator = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Ground speed, knots
		//const double ground_speed_knots = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		//const char nautical_speed_indicator = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// Ground speed, kilometers per hour
		const double ground_speed_kmh = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		const char speed_indicator = *token;
		data.speed = static_cast<uint8_t>(ground_speed_kmh); //flooring speed

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// checksum
		//const std::string checksum(token);
	}

	void GPS::parse_hchdg(const char *command, uint8_t length, GPSData &data)
	{
		char *token = std::strtok(const_cast<char *>(command) + 1, COMMAND_DELIMETERS.data());
		token = std::strtok(nullptr, COMMAND_DELIMETERS.data()); //skipping command name

		// heading
		data.heading = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// deviation
		//const std::string deviation(token); // empty

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// variation
		//const double variation = std::atof(token);

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		//const char variation_direction = *token;

		token = std::strtok(nullptr, COMMAND_DELIMETERS.data());

		// checksum
		//const std::string checksum(token);
	}

	std::optional<const GPS::GPSData *> GPS::parse()
	{
		for (char *command : last_update)
		{
			size_t command_length = strlen(command);
			if (!is_checksum_valid(command, command_length))
			{
				continue;
			}
			char command_name[8];
			get_command_name(command, command_length, command_name);
			if (command_parser_map.find(command_name) == command_parser_map.end())
			{
				continue;
			}
			command_parser_map.at(command_name)(command, command_length, parsedData);
		}
		return &parsedData;
	}

	size_t GPS::read(char *buffer, size_t buffer_length)
	{
		serialMessage.set_payload_length(512);

		protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK && status != protocols::ISerial::STATUS::HARDWARE_TIMEOUT)
		{
			return 0;
		}
		std::copy(serialMessage.get_payload(), serialMessage.get_payload() + serialMessage.get_payload_length(), buffer);
		buffer[serialMessage.get_payload_length()] = '\0';

		char *token = std::strtok(buffer, "\r\n");
		if (token == nullptr)
		{
			return 0;
		}
		std::copy_n(token, strlen(token), last_update[0]);
		last_update[0][strlen(token)] = '\0';
		size_t i = 1;
		for (; token != nullptr && i < LAST_UPDATE_COUNT; ++i)
		{
			token = std::strtok(nullptr, "\r\n");
			if (token == nullptr)
			{
				break;
			}
			std::copy_n(token, strlen(token), last_update[i]);
			last_update[i][strlen(token)] = '\0';
		}
		return i;
	}

	GPS::GPSData GPS::read_and_parse()
	{
		size_t length = read(read_buffer, READ_BUFFER_LENGTH);
		parse();
		return parsedData; // returns last SUCCESSFUL parse
	}

	std::array<char[GPS::UPDATE_MESSAGE_LENGTH], GPS::LAST_UPDATE_COUNT> GPS::get_last_update_message()
	{
		return last_update;
	}

	GPS::GPSData GPS::get_last_update()
	{
		return parsedData;
	}

	bool GPS::init()
	{
		//setting NMEA Sentences
		uint32_t nmea_register_value = 0;
		for (uint32_t nmea_sentence : ENABLED_NMEA_SENTENCES)
		{
			nmea_register_value |= nmea_sentence;
		}
		char register_value_buffer[10];
		std::sprintf(register_value_buffer, "%lX", nmea_register_value);
		uint8_t register_value_buffer_offset = 0;
		if (register_value_buffer[0] == '0' && (register_value_buffer[1] != 'x' || register_value_buffer[1] != 'X'))
		{
			register_value_buffer_offset = 2; // cppreference: "In the alternative implementation decimal point character is written even if no digits follow it." and we don't need the prefix
		}
		const uint16_t local_buffer_length = 256;
		char buffer[local_buffer_length];
		std::sprintf(buffer, "$PSTMSETPAR,1201,%s,0*", register_value_buffer + register_value_buffer_offset);
		uint32_t checksum = calculate_checksum(buffer, strlen(buffer));

		char checksum_buffer[6];
		std::sprintf(checksum_buffer, "%lX", checksum);

		uint8_t checksum_buffer_offset = 0;
		if (checksum_buffer[0] == '0' && (checksum_buffer[1] != 'x' || checksum_buffer[1] != 'X'))
		{
			checksum_buffer_offset = 2; // cppreference: "In the alternative implementation decimal point character is written even if no digits follow it." and we don't need the prefix
		}

		std::strcat(buffer, checksum_buffer + checksum_buffer_offset);
		std::strcat(buffer + strlen(buffer), "\r\n");

		if (!is_checksum_valid(buffer, strlen(buffer)))
		{
			return false;
		}

		serialMessage.set_payload_length(1024);
		protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		serialMessage.set_payload(buffer);

		std::fill_n(buffer, local_buffer_length, '\0');

		serialHandle.write(serialMessage);

		serialMessage.set_payload_length(1024);
		status = serialHandle.read(serialMessage);
		//
		//		if (status != protocols::ISerial::STATUS::OK && status != protocols::ISerial::STATUS::HARDWARE_TIMEOUT) {
		//			return false;
		//		}
		//		const uint8_t* sentence_start_pos = std::find(serialMessage.get_payload(), serialMessage.get_payload() + serialMessage.get_payload_length(), '$');
		//		const uint8_t* sentence_end_pos = std::find(sentence_start_pos, sentence_start_pos + READ_BUFFER_LENGTH, '\r');
		//		const uint16_t sentence_length = std::distance(sentence_start_pos, sentence_end_pos);
		//		if (sentence_length >= local_buffer_length) {
		//			return false;
		//		}
		//		std::copy(sentence_start_pos, sentence_end_pos, buffer);
		//		*(buffer + sentence_length) = '\0';
		//
		//		if (!is_checksum_valid(buffer, strlen(buffer))) {
		//			return false;
		//		}
		//
		//		char command_name[16] = { 0 };
		//		get_command_name(buffer, strlen(buffer), command_name);
		//		if (std::strcmp(command_name, "PSTMSETPAROK") != 0) {
		//			return false;
		//		}
		//
		return true;
	}
	void GPS::get_last_update_as_semtech(uint8_t battery_voltage, char *semtech_buffer, size_t semtech_buffer_length)
	{
		if (battery_voltage == 0)
		{
			battery_voltage = 1;
		}
		else if (battery_voltage == 255)
		{
			battery_voltage = 254;
		}

		std::fill_n(semtech_buffer, semtech_buffer_length, '\0');

		char local_buffer[64] = {0};
		uint8_t data_start_index = 0;
		std::fill_n(semtech_buffer, 16, '0');

		//battery voltage
		std::sprintf(local_buffer, "%X", battery_voltage);
		std::copy_n(local_buffer, strlen(local_buffer), semtech_buffer + strlen(semtech_buffer));

		//latitude

		int32_t converted_latitude = (pow(2, 23) / parsedData.latitude) * 180 - pow(2, 23);
		uint8_t *latitude_msb = reinterpret_cast<uint8_t *>(&converted_latitude);
		uint8_t *latitude_csb = reinterpret_cast<uint8_t *>(&converted_latitude) + 1;
		uint8_t *latitude_lsb = reinterpret_cast<uint8_t *>(&converted_latitude) + 2;

		std::sprintf(local_buffer, "%X%X%X", *latitude_msb, *latitude_csb, *latitude_lsb);
		std::copy_n(local_buffer, strlen(local_buffer), semtech_buffer + strlen(semtech_buffer));

		//longitude

		int32_t converted_longitude = (pow(2, 23) / parsedData.longitude) * 360 - pow(2, 23);
		uint8_t *longitude_msb = reinterpret_cast<uint8_t *>(&converted_latitude);
		uint8_t *longitude_csb = reinterpret_cast<uint8_t *>(&converted_latitude) + 1;
		uint8_t *longitude_lsb = reinterpret_cast<uint8_t *>(&converted_latitude) + 2;

		std::sprintf(local_buffer, "%X%X%X", *longitude_msb, *longitude_csb, *longitude_lsb);
		std::copy_n(local_buffer, strlen(local_buffer), semtech_buffer + strlen(semtech_buffer));

		//memcpy(ch, reinterpret_cast<unsigned char*>(&parsedData.longitude), sizeof(float));
		//std::sprintf(local_buffer, "%X%X%X%X", ch[3], ch[2], ch[1], ch[0]);
		//std::copy_n(local_buffer, strlen(local_buffer), semtech_buffer + strlen(semtech_buffer));

		//altitude
		uint16_t converted_altitude = static_cast<uint16_t>(parsedData.altitude);

		std::sprintf(local_buffer, "%04X", converted_altitude);
		std::copy_n(local_buffer, strlen(local_buffer), semtech_buffer + strlen(semtech_buffer));
	}
	void GPS::get_last_update_as_adeunis(uint8_t status, uint8_t temperature, uint16_t battery_voltage, char *adeunis_buffer, size_t adeunis_buffer_length)
	{
		std::fill_n(adeunis_buffer, adeunis_buffer_length, '\0');

		char local_buffer[64] = {0};

		//status
		std::sprintf(local_buffer, "%02X", status);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//temperature
		std::sprintf(local_buffer, "%02X", temperature);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//latitude
		uint8_t latitude_pole = (parsedData.latitude_pole == 'N') ? 0 : 1;
		uint8_t latitude_seconds_whole = static_cast<uint8_t>(parsedData.latitude_seconds);
		uint8_t latitude_seconds_per = static_cast<uint8_t>(parsedData.latitude_seconds - latitude_seconds_whole * 10);
		std::sprintf(local_buffer, "%02d%02d%02d%d%d", parsedData.latitude_whole, parsedData.latitude_minutes, latitude_seconds_whole, latitude_seconds_per, latitude_pole);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//longitude
		uint8_t longitude_pole = (parsedData.longitude_pole == 'E') ? 0 : 1;
		uint8_t longitude_seconds_whole = static_cast<uint8_t>(parsedData.longitude_seconds);
		uint8_t longitude_seconds_per = static_cast<uint8_t>(parsedData.longitude_seconds - longitude_seconds_whole * 10);
		std::sprintf(local_buffer, "%03d%02d%02d%d%d", parsedData.longitude_whole, parsedData.longitude_minutes, longitude_seconds_whole, latitude_seconds_per, longitude_pole);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//battery voltage
		std::sprintf(local_buffer, "%03X", battery_voltage);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));
	}
	void GPS::get_last_update_as_adeunis_alternative(uint8_t status, uint8_t temperature, uint16_t battery_voltage, char *adeunis_buffer, size_t adeunis_buffer_length)
	{
		std::fill_n(adeunis_buffer, adeunis_buffer_length, '\0');

		char local_buffer[64] = {0};

		//status
		std::sprintf(local_buffer, "%02X", status);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//temperature
		std::sprintf(local_buffer, "%02X", temperature);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//latitude
		uint8_t latitude_pole = (parsedData.latitude_pole == 'N') ? 0 : 1;
		double latitude_seconds_corrected = parsedData.latitude_seconds * (100.0 / 60.0);
		uint8_t latitude_seconds_whole = static_cast<uint8_t>(latitude_seconds_corrected);
		uint8_t latitude_seconds_per = static_cast<uint8_t>((latitude_seconds_corrected - latitude_seconds_whole) * 10);

		uint8_t latitude_seconds_per_carry = latitude_seconds_per / 10;
		latitude_seconds_whole += latitude_seconds_per_carry;
		latitude_seconds_per -= latitude_seconds_per_carry * 10;

		uint8_t latitude_seconds_whole_carry = latitude_seconds_whole / 60;
		latitude_seconds_whole -= latitude_seconds_whole_carry * 60;

		std::sprintf(local_buffer, "%02d%02d%02d%d%d", parsedData.latitude_whole, parsedData.latitude_minutes + latitude_seconds_whole_carry, latitude_seconds_whole, latitude_seconds_per, latitude_pole);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//longitude
		uint8_t longitude_pole = (parsedData.longitude_pole == 'E') ? 1 : 0;
		double longitude_seconds_corrected = parsedData.longitude_seconds * (100.0 / 60.0);
		uint8_t longitude_seconds_whole = static_cast<uint8_t>(longitude_seconds_corrected);
		uint8_t longitude_seconds_per = static_cast<uint8_t>((longitude_seconds_corrected - longitude_seconds_whole) * 10);

		uint8_t longitude_seconds_per_carry = longitude_seconds_per / 10;
		longitude_seconds_whole += longitude_seconds_per_carry;
		longitude_seconds_per -= longitude_seconds_per_carry;

		uint8_t longitude_seconds_whole_carry = longitude_seconds_whole / 60;
		longitude_seconds_whole -= longitude_seconds_whole_carry * 60;

		std::sprintf(local_buffer, "%03d%02d%02d%d%d", parsedData.longitude_whole, parsedData.longitude_minutes + longitude_seconds_whole_carry, longitude_seconds_whole, longitude_seconds_per, longitude_pole);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));

		//battery voltage

		std::sprintf(local_buffer, "%03X", battery_voltage);
		std::copy_n(local_buffer, strlen(local_buffer), adeunis_buffer + strlen(adeunis_buffer));
	}

}