#include "tempsensor.h"
#include <math.h>

Tempsensor::Tempsensor()
{
	addr.reserve(10);
	i2c = NULL;
}
Tempsensor::Tempsensor(I2C_HandleTypeDef *i2cHandle)
{
	addr.reserve(12);
	i2c = i2cHandle;
}


Tempsensor::~Tempsensor()
{
}

float Tempsensor::getTemp()
{
	uint8_t buffer[2] = { 0 };
	HAL_StatusTypeDef res = HAL_OK;
	addr.push_back(1);
	res = HAL_I2C_Mem_Read(i2c, devAddres, 0x00, I2C_MEMADD_SIZE_8BIT, buffer, 2, 200);
	int16_t raw = buffer[0] << 4 | buffer[1]>>4;
	if (buffer[0] & 0x80)
	{
		raw *= -1;
	}
	float temp=raw*(pow(2,-4));
	return temp;
}
