#pragma once

#include <ISerial.h>
#include <utils.h>

#include <stm32l4xx_hal.h>

#include <optional>

namespace devices {
	class TempSensor {
	public:
		enum class RESOLUTIONS {
			CELSIUS_05    = 0b00000000,
			CELSIUS_025   = 0b00100000,
			CELSIUS_0125  = 0b01000000,
			CELSIUS_00625 = 0b01100000
		};
		enum class DEVICE_ADDRESSES { 
			A0 = 0b01001000 << 1,
			A1 = 0b01001001 << 1,
			A2 = 0b01001010 << 1,
			A3 = 0b01001011 << 1,
			A4 = 0b01001100 << 1,
			A5 = 0b01001101 << 1,
			A6 = 0b01001110 << 1,
			A7 = 0b01001111 << 1
		};
		struct REGISTERS {
			struct REGISTER {
				const uint8_t address;
				const uint8_t length;
				REGISTER(uint8_t _address, uint8_t _length) : address(_address), length(_length) {}
			};
			static const REGISTER TEMP;
			static const REGISTER CONFIG;
			static const REGISTER TEMP_HYST;
			static const REGISTER TEMP_LIMIT;
		};
		enum class STATUS {
			OK =				HAL_OK,
			HARDWARE_ERROR =	HAL_ERROR,
			HARDWARE_BUSY =		HAL_BUSY,
			HARDWARE_TIMEOUT =	HAL_TIMEOUT,
			WRITE_ERROR,
			READ_ERROR,
			COULD_NOT_CREATE_MESSAGE,
		};
	private:
		protocols::ISerial& serialHandle;
		protocols::SerialMessage serialMessage;
		const DEVICE_ADDRESSES device_address;
		STATUS last_error = STATUS::OK;
		STATUS write_register(const REGISTERS::REGISTER& register_addr, const uint8_t* data);
	public:
		TempSensor(protocols::ISerial& _serialHandle, DEVICE_ADDRESSES _device_address)
		: serialHandle(_serialHandle), device_address(_device_address) {
			std::optional<protocols::SerialMessage> created_message = serialHandle.create_message(nullptr, 0, protocols::ISerial::DEFAULT_TIMEOUT, protocols::ISerial::DEFAULT_ADDRESS, protocols::ISerial::DEFAULT_REGISTER);
			if (!created_message.has_value()) {
				Utils::ERROR_GUARD();
			}
			serialMessage = created_message.value();
		}
		virtual ~TempSensor() {}
		
		STATUS set_resolution(RESOLUTIONS resolution);
		std::optional<float> get_temp();
		std::optional<uint16_t> get_temp_raw();
		std::optional<uint16_t> read_register(const REGISTERS::REGISTER& register_addr);
		std::optional<STATUS> get_last_error();
	};
}