#include "TemperatureSensor.h"

#include <cmath>

namespace devices {
	const TempSensor::REGISTERS::REGISTER TempSensor::REGISTERS::TEMP =			{ 0b00, 2 };
	const TempSensor::REGISTERS::REGISTER TempSensor::REGISTERS::CONFIG =		{ 0b01, 1 };
	const TempSensor::REGISTERS::REGISTER TempSensor::REGISTERS::TEMP_HYST =	{ 0b10, 2 };
	const TempSensor::REGISTERS::REGISTER TempSensor::REGISTERS::TEMP_LIMIT =	{ 0b11, 2 };
	
	TempSensor::STATUS TempSensor::set_resolution(TempSensor::RESOLUTIONS resolution) {
		const auto read_result = read_register(REGISTERS::CONFIG);
		if (!read_result.has_value()) {
			last_error = STATUS::READ_ERROR;
			return last_error;
		}
		uint16_t configuration = read_result.value();
		configuration |= static_cast<uint8_t>(resolution);
		return write_register(REGISTERS::CONFIG, reinterpret_cast<uint8_t*>(&configuration));
	}
	
	std::optional<uint16_t> TempSensor::read_register(const TempSensor::REGISTERS::REGISTER& register_addr) {
		serialMessage.set_payload_length(register_addr.length);
		serialMessage.set_timeout(protocols::ISerial::DEFAULT_TIMEOUT);
		serialMessage.set_device_address(static_cast<uint32_t>(device_address));
		serialMessage.set_register_address(register_addr.address);
		const protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return { };
		}
		if (register_addr.length == 1) {
			return serialMessage.get_payload()[0];
		}
		return serialMessage.get_payload()[0] << 8 | serialMessage.get_payload()[1];
	}
	
	TempSensor::STATUS TempSensor::write_register(const TempSensor::REGISTERS::REGISTER& register_addr, const uint8_t* data) {
		
		serialMessage.set_payload(data, register_addr.length);
		serialMessage.set_timeout(protocols::ISerial::DEFAULT_TIMEOUT);
		serialMessage.set_device_address(static_cast<uint32_t>(device_address));
		serialMessage.set_register_address(register_addr.address);
		const protocols::ISerial::STATUS status = serialHandle.write(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return last_error;
		}
		const auto read_result = read_register(register_addr);
		if (!read_result.has_value()) {
			last_error = STATUS::READ_ERROR;
			return last_error;
		}
		const uint16_t write_result = read_result.value();
		uint16_t data_temp = data[0];
		if (register_addr.address == 2) {
			data_temp = data_temp << 8;
			data_temp |= data[1];
		}
		if (write_result != data_temp) {
			last_error = STATUS::WRITE_ERROR;
			return last_error;
		}
		return STATUS::OK;
	}
	
	std::optional<float> TempSensor::get_temp() {
		const auto read_result = get_temp_raw();
		if (!read_result.has_value()) {
			return { };
		}
		uint16_t temp_raw = read_result.value();
		uint8_t* msb_lsb_ptr = reinterpret_cast<uint8_t*>(&temp_raw);
		uint8_t msb = msb_lsb_ptr[1];
		uint8_t lsb = msb_lsb_ptr[0];
		
		float temperature = (msb << 4) + (lsb >> 4);
		if (msb & 0x80) {
			temperature = -temperature;	
		}
		temperature *= pow(2, -4);
		return temperature;
	}
	
	std::optional<uint16_t> TempSensor::get_temp_raw() {
		return read_register(REGISTERS::TEMP);
	}
	
	std::optional<TempSensor::STATUS> TempSensor::get_last_error() {
		if (last_error == STATUS::OK) {
			return { };
		}
		return last_error;
	}
}