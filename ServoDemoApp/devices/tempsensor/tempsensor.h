#pragma once
#include <stm32l4xx_hal.h>
#include <vector>
class Tempsensor
{
private:
	I2C_HandleTypeDef *i2c;
	static const uint16_t devAddres = 0x48 << 1;
	std::vector<uint8_t> addr;
public:
	Tempsensor();
	Tempsensor(I2C_HandleTypeDef *i2cHandle);
	~Tempsensor();
	float getTemp();
};

