#include "BLEUUID.h"
#include <cstring>
#include <string>
#include <algorithm>
BLEUUID::BLEUUID()
{
	
}
BLEUUID::BLEUUID(const char *uuidStr) 
{
	std::string tempStr(uuidStr);
	// remove - signs if any
	tempStr.erase(std::remove(tempStr.begin(), tempStr.end(), '-'), tempStr.end());
	size_t len = strlen(uuidStr);
	if (tempStr.length() == 32)
	{
		uuid.reserve(16);
		type = UUIDType::UUID_16_BYTE;
	}
	else
	{
		if (tempStr.length() == 4)
		{
			uuid.reserve(2);
			type = UUIDType::UUID_2_BYTE;
		}
	}

	if (type)
	{
		for (unsigned int i = 0; i < tempStr.length(); i += 2)
		{
			std::string byteString = tempStr.substr(i, 2);
			uint8_t byte = (uint8_t)strtol(byteString.c_str(), NULL, 16);
			uuid.push_back(byte);
		}
		std::reverse(uuid.begin(), uuid.end());
	}
}

const uint8_t *BLEUUID::getUUID()
{
	return uuid.data();
}
const size_t BLEUUID::getLen()
{
	return uuid.size();
}

const UUIDType BLEUUID::getType()
{
	return type;
}
