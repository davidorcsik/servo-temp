#pragma once
#include <vector>
#include <map>

#include "BLEService.h"
#include <bluenrg_def.h>
namespace devices
{

enum BLERole
{
	Peripheral,
	Central,
	Mixed
};
enum BLEMode
{
	Single,
	Multi

};
enum BLEStatus
{
	OK,
	DRIVER_ERROR,
	UNKNOWN_ERROR
};

enum BLEDeviceState
{
	DISCONNECTED = 0X00,
	CONNECTED = 0X01,
	IDLE = 0X02,
	ADVERTISING = 0X03,
	PAIRED = 0X04,
};

//callback types for specific callbacks
typedef void (*BLEConnectionCompleteCb)(uint8_t addr[6], uint16_t handle); 
typedef void (*BLEDisconnectedCb)(uint16_t handler, uint8_t disconn_result);
typedef void (*BLEPairingCompleteCb)(uint16_t handler, uint8_t pairing_status);
typedef void (*BLEWriteRequestCb)(uint16_t conn_handler, uint16_t attr_handler, uint8_t *attr_data, uint8_t attr_data_length);
typedef void (*BLEAttributeModifiedCb)(uint16_t attr_handler, uint8_t *attr_data, uint8_t attr_data_length);
typedef void (*BLEReadRequestCb)(uint16_t conn_handle, uint16_t attr_handle, uint8_t data_length, uint16_t offset);

class BLEDevice
{
// Construtors & destructors
  public:
	BLEDevice();
	BLEDevice(uint8_t *macAddress, const char *advertisingName, BLERole role, BLEMode connectionMode, bool auth, uint32_t passkey);

	~BLEDevice();
// Constants
  private:
	static std ::vector<BLEDevice *> devices;
	static const size_t MAX_SERVICE_COUNT = 10;
	static const size_t MAX_NAME_SIZE = 25;
	static const uint16_t ADVINTERVMIN = 0x0020;
	static const uint16_t ADVINTERVMAX = 0x0100;
	static const size_t MAX_ADV_UUID_NUM = 10;
	
// General callback
  private:
	static void GlobalHCIEventCallback(void *pData);
	
// Control Functions
  public:
	BLEStatus Init();
	uint8_t getLastError();
	BLEStatus addService(BLEService *service);
	BLEStatus startAdvertising();
	void runStateMachine();
	void setPasskey(uint32_t passkey);
	bool isConnected();
	bool isPaired();

	// Helper functions
  private:
	BLEStatus InitHw();
	BLEStatus InitStack();

	
// Callback funtions
  private:
	BLEConnectionCompleteCb connectionCompletedCallback;
	BLEPairingCompleteCb pairingCompletedCallback;
	BLEDisconnectedCb disconnectedCallback;
	BLEWriteRequestCb writeRequestCallback;
	BLEAttributeModifiedCb attributeModifiedCallback;
	BLEReadRequestCb readRequestCallback;

  private:
	void
	HCIEventCallback(void *pData);
	void HCIEventMetaCallback(hci_event_pckt *event_pckt);
	void HCIEventVendorCallback(hci_event_pckt *event_pckt);
	void HCIEventTraditionalCallback(hci_event_pckt *event_pckt);
	
	void setConnectionCompleteCallback(BLEConnectionCompleteCb cb);
	void setDisconnectedCallback(BLEDisconnectedCb cb);
	void setPairingCompletedCallback(BLEPairingCompleteCb cb);

	
// Fields
  private:
	char advertisingName[BLEDevice::MAX_NAME_SIZE + 1];
	uint8_t uuidArray[BLEDevice::MAX_ADV_UUID_NUM * 16 + 2];
	uint16_t connectionHandler = 0;
	bool connected;
	bool authentication;
	uint8_t lastError=0;
	uint32_t passkey = 0;
	uint16_t gapServiceHandler = 0;
	uint16_t devNameCharacteristicHandler = 0;
	uint16_t appearenceCharHandler = 0;
	uint8_t *macAddress;
	BLEMode connectionMode;
	BLERole role;
	uint8_t gapRole = 0;
	std::vector<BLEService *> services;
	BLEDeviceState state;
	bool processRunning;
};
}