#include "BLEDevice.h"
#include <string.h>

extern "C" {
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
}
namespace devices
{

std ::vector<BLEDevice *> BLEDevice::devices;
BLEDevice::BLEDevice()
{
	services.reserve(BLEDevice::MAX_SERVICE_COUNT);
}

BLEDevice::BLEDevice(uint8_t *macAddress, const char *advertisingName, BLERole role, BLEMode connectionMode, bool auth, uint32_t passkey)
{
	connectionCompletedCallback=nullptr;
	pairingCompletedCallback = nullptr;
	disconnectedCallback = nullptr;
	writeRequestCallback = nullptr;
	attributeModifiedCallback = nullptr;
	readRequestCallback = nullptr;
	services.reserve(BLEDevice::MAX_SERVICE_COUNT);
	this->macAddress = macAddress;

	this->advertisingName[0] = AD_TYPE_COMPLETE_LOCAL_NAME;
	strcpy(this->advertisingName + 1, advertisingName);

	this->connectionMode = connectionMode;
	switch (role)
	{
	case BLERole::Peripheral:
		gapRole = GAP_PERIPHERAL_ROLE_IDB05A1;
		break;
	case BLERole::Central:
		gapRole = GAP_CENTRAL_ROLE_IDB05A1;
		break;
	case BLERole::Mixed:
		gapRole = GAP_CENTRAL_ROLE_IDB05A1 | GAP_PERIPHERAL_ROLE_IDB05A1;
		break;
	default:
		gapRole = GAP_PERIPHERAL_ROLE_IDB05A1;
		break;
	}
	this->role = role;
	this->passkey = passkey;
	this->authentication = auth;
	BLEDevice::devices.push_back(this);
}

BLEDevice::~BLEDevice()
{
}

BLEStatus BLEDevice::startAdvertising()
{
	if (!connected && !processRunning)
	{

		volatile tBleStatus ret;
		int i;
		uint8_t offset;

		uint8_t uuidArraySize = 0;
		uint8_t uuid_type_indicator = 0x00;

		size_t nameSize = strlen(advertisingName);
		processRunning = true;

		hci_le_set_scan_resp_data(0, NULL); //scan response disabled (not used)
		uint8_t uuidOffset = 0;
		bool has128bit = false;
		bool has16bit = false;
		for (size_t i = 0; i < services.size(); i++)
		{
			switch (services[i]->getUUID().getType())
			{
			case UUID_2_BYTE:
				has16bit = true;
				break;
			case UUID_16_BYTE:
				has128bit = true;
				break;
			default:
				break;
			}
		}
		// Add 128BIT
		if (has128bit)
		{
			uuidArray[uuidOffset] = AD_TYPE_128_BIT_SERV_UUID;
			uuidOffset += 1;
			for (size_t i = 0; i < services.size(); i++)
			{
				BLEUUID tempUUID = services[0]->getUUID();
				if (tempUUID.getType() == UUID_16_BYTE)
				{
					memcpy(uuidArray + uuidOffset, tempUUID.getUUID(), tempUUID.getLen());
					uuidOffset += 16;
				}
			}
		}
		if (has16bit)
		{
			// Add 16BIT UUID
			uuidArray[uuidOffset] = AD_TYPE_16_BIT_SERV_UUID;
			uuidOffset += 1;
			for (size_t i = 0; i < services.size(); i++)
			{
				BLEUUID tempUUID = services[0]->getUUID();
				if (tempUUID.getType() == UUID_2_BYTE)
				{
					memcpy(uuidArray + uuidOffset, tempUUID.getUUID(), tempUUID.getLen());
					uuidOffset += 2;
				}
			}
		}

		//TODO: returns invalid parameters in case of costume id (stack did not support costume UUID advertising)
		ret = aci_gap_set_discoverable(ADV_IND, BLEDevice::ADVINTERVMIN, BLEDevice::ADVINTERVMAX, PUBLIC_ADDR, NO_WHITE_LIST_USE, nameSize, advertisingName, uuidArraySize, uuidArray, 0x006, 0x008);
		//advertising interval is the period between advertising events (when advertising packet sent in each advertising channel at the same time)
		if (ret)
		{
			lastError = ret;
			return BLEStatus::DRIVER_ERROR;
		}
		return BLEStatus::OK;
	}
	else
	{
		return BLEStatus::OK;
	}
}

void BLEDevice::runStateMachine()
{
	hci_user_evt_proc();
}

BLEStatus BLEDevice::addService(BLEService *service)
{
	if (service != NULL)
	{
		uint16_t serviceHandler;
		service->registerService();
		service->registerCharacteristics();
		services.push_back(service);
	}
}

void BLEDevice::HCIEventCallback(void *pData)
{
	hci_uart_pckt *hci_pckt = (hci_uart_pckt *)pData;
	hci_event_pckt *event_pckt = (hci_event_pckt *)hci_pckt->data;

	if (hci_pckt->type != HCI_EVENT_PKT)
	{
		return;
	}

	switch (event_pckt->evt)
	{
	case EVT_LE_META_EVENT: //traditional hci meta events
		HCIEventMetaCallback(event_pckt);
		break;

	case EVT_VENDOR:
		HCIEventVendorCallback(event_pckt); //vendor specific events
		break;

	default:
		HCIEventTraditionalCallback(event_pckt); //traditional hci events
		break;
	}
}

void BLEDevice::HCIEventMetaCallback(hci_event_pckt *event_pckt)
{
	evt_le_meta_event *evt = (evt_le_meta_event *)event_pckt->data;
	//connection completed event
	switch (evt->subevent)
	{
	
	case EVT_LE_CONN_COMPLETE:
	{
		evt_le_connection_complete *ptr = (evt_le_connection_complete *)evt->data;
		if (authentication) //authentication was set up during initialization
		{
			connectionHandler = ptr->handle;
		}
		else
		{
			connectionHandler = ptr->handle;
			connected = true;
			processRunning = false;
		}
		if (connectionCompletedCallback)
		{
			(*connectionCompletedCallback)(ptr->peer_bdaddr, ptr->handle);
		}
	}
	break;

	default:
		break;
	}
}

void BLEDevice::HCIEventVendorCallback(hci_event_pckt *event_pckt)
{
	evt_blue_aci *blue_evt = (evt_blue_aci *)event_pckt->data;
	switch (blue_evt->ecode)
	{
	//central requested to read specific characteristic (read char value/read long char value)
	case EVT_BLUE_GATT_READ_PERMIT_REQ:
		if (readRequestCallback)
		{
			evt_gatt_read_permit_req *ptr = (evt_gatt_read_permit_req *)blue_evt->data;
			(*readRequestCallback)(ptr->conn_handle, ptr->attr_handle, ptr->data_length, ptr->offset);
		}
		break;
	//this event is generated in case of "passkey" key generation method (depends on IO capability)
	case EVT_BLUE_GAP_PASS_KEY_REQUEST:

		aci_gap_pass_key_response(connectionHandler, passkey);

		break;
	//pairing completed event
	case EVT_BLUE_GAP_PAIRING_CMPLT:
		if (true)
		{
			evt_gap_pairing_cmplt *ptr = (evt_gap_pairing_cmplt *)blue_evt->data;
			if (ptr->status == 0x00)
			{
				state = BLEDeviceState::PAIRED;
				processRunning = false;
			}
			if (pairingCompletedCallback)
			{
				(*pairingCompletedCallback)(ptr->conn_handle, ptr->status);
			}
		}
		break;
	//this event is  generated if client overwrites an attribute (for example: client characteristic descriptor in case of notification)
	case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:
		if (attributeModifiedCallback)
		{
			evt_gatt_attr_modified_IDB05A1 *ptr = (evt_gatt_attr_modified_IDB05A1 *)blue_evt->data;
			(*attributeModifiedCallback)(ptr->attr_handle, ptr->att_data, ptr->data_length);
		}
		break;
	//this event is generated when a client requested to overwrite an attribute- server must check whether the value can be allowed to be written or not
	case EVT_BLUE_GATT_WRITE_PERMIT_REQ:
		if (writeRequestCallback)
		{
			evt_gatt_write_permit_req *ptr = (evt_gatt_write_permit_req *)blue_evt->data;
			(*writeRequestCallback)(ptr->conn_handle, ptr->attr_handle, ptr->data, ptr->data_length);
		}
		break;

	default:
		break;
	}
}

void BLEDevice::HCIEventTraditionalCallback(hci_event_pckt *event_pckt)
{
	switch (event_pckt->evt)
	{
	case EVT_DISCONN_COMPLETE:

		processRunning = false;
		connected = false;
		if (disconnectedCallback)
		{
			disconnect_cp *ptr = (disconnect_cp *)event_pckt->data;
			(*disconnectedCallback)(ptr->handle, ptr->reason);
			state = BLEDeviceState::DISCONNECTED;
			processRunning = false;
		}
		break;

	default:
		break;
	}
}

BLEStatus BLEDevice::Init()
{
	BLEStatus initStatus = InitHw();
	if (initStatus != BLE_STATUS_SUCCESS)
	{
		return BLEStatus::DRIVER_ERROR;
	}
	initStatus = InitStack();
	if (initStatus != BLE_STATUS_SUCCESS)
	{
		return BLEStatus::DRIVER_ERROR;
	}
	return BLEStatus::OK;
}

BLEStatus BLEDevice::InitHw()
{

	//	uint8_t a = do_stuff();
	tBleStatus initStatus;

	//initialization of SPI and HCI
	hci_init(BLEDevice::GlobalHCIEventCallback, nullptr);
	if (connectionMode == BLEMode::Multi)
	{
		///TODO: Matet megkerdezni, hogy WTF, lehet ezt valahonnan lopni?
		uint8_t stack_mode_3 = 0x03; //if the device is peripheral then 2 central can connect to it. if central then it can connect 8 peripheral
									 //if dual-role than 1 central can connect to it and it can connect 8 peripheral

		hci_reset();
		initStatus = aci_hal_write_config_data(CONFIG_DATA_MODE_OFFSET, CONFIG_DATA_MODE_LEN, &stack_mode_3); //multiple connection (stack mode 3)

		if (initStatus != BLE_STATUS_SUCCESS)
		{
			lastError = initStatus;
			return BLEStatus::DRIVER_ERROR;
		}

		initStatus = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, macAddress);
		if (initStatus != BLE_STATUS_SUCCESS)
		{
			lastError = initStatus;
			return BLEStatus::DRIVER_ERROR;
		}
	}
	else
	{
		//reset device
		hci_reset();
		HAL_Delay(5);
		//set up address
		initStatus = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, macAddress);
		if (initStatus != BLE_STATUS_SUCCESS)
		{
			lastError = initStatus;
			return BLEStatus::DRIVER_ERROR;
		}
	}
	lastError = 0;
	return BLEStatus::OK;
}

BLEStatus BLEDevice::InitStack()
{
	tBleStatus initStatus;

	//initialization of gatt layer (this function automatically adds attribute profile service and service changed characteristic
	initStatus = aci_gatt_init();
	if (initStatus != BLE_STATUS_SUCCESS)
	{
		lastError = initStatus;
		return BLEStatus::DRIVER_ERROR;
	}

	//initializing gap layer (this function automatically adds gap service, device name and appearance characteristic
	initStatus = aci_gap_init_IDB05A1(gapRole, 0, 0, &gapServiceHandler, &devNameCharacteristicHandler, &appearenceCharHandler);

	if (initStatus != BLE_STATUS_SUCCESS)
	{
		lastError = initStatus;
		return BLEStatus::DRIVER_ERROR;
	}

	//set up basic characteristics
	initStatus = aci_gatt_update_char_value(gapServiceHandler, devNameCharacteristicHandler, 0, 0, (uint8_t *)"");

	if (initStatus != BLE_STATUS_SUCCESS)
	{
		lastError = initStatus;
		return BLEStatus::DRIVER_ERROR;
	}

	//set authentication and io capability for legacy pairing
	initStatus = aci_gap_set_io_capability(IO_CAP_DISPLAY_ONLY);

	if (initStatus != BLE_STATUS_SUCCESS)
	{
		lastError = initStatus;
		return BLEStatus::DRIVER_ERROR;
	}

	initStatus = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED, OOB_AUTH_DATA_ABSENT, NULL, 7, 16, DONOT_USE_FIXED_PIN_FOR_PAIRING, 0, BONDING);
	if (initStatus != BLE_STATUS_SUCCESS)
	{
		lastError = initStatus;
		return BLEStatus::DRIVER_ERROR;
	}

	aci_hal_set_tx_power_level(1, 4);

	return BLEStatus::OK;
}

uint8_t BLEDevice::getLastError()
{
	return lastError;
}

void BLEDevice::GlobalHCIEventCallback(void *pData)
{
	for (size_t i = 0; i < devices.size(); i++)
	{
		devices[i]->HCIEventCallback(pData);
	}
}
}

void devices::BLEDevice::setPasskey(uint32_t passkey)
{
	this->passkey = passkey;
}


bool devices::BLEDevice::isConnected()
{
	return connected;
}

bool devices::BLEDevice::isPaired()
{
	return state == devices::PAIRED;
}