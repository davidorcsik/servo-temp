#pragma once
#include "../BLEUUID.h"

extern "C" {
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
}

class BLECharacteristic
{
  public:
	BLECharacteristic(BLEUUID uuid, uint8_t properties, size_t valueSize, uint16_t serviceHandler, uint8_t authFlags);
	~BLECharacteristic();

  public:
	uint8_t updateValue(uint8_t *value);
	void setServiceHandler(uint16_t serviceHandler);
	uint8_t registerCharacteristic();

  protected:
	uint16_t handler;
	uint16_t serviceHandler;
	BLEUUID uuid;
	size_t valueSize;
	uint8_t propertiesFlags;
	uint8_t authFlags;
	static const uint8_t responseMask = GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP;	
};
