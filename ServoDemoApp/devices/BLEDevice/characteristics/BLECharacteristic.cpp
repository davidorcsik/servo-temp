#include "BLECharacteristic.h"

extern "C" {
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
}

BLECharacteristic::BLECharacteristic(BLEUUID uuid, uint8_t propertiesFlags, size_t valueSize, uint16_t serviceHandler, uint8_t authFlags)
{
	this->uuid = uuid;
	this->valueSize = valueSize;
	this->propertiesFlags = propertiesFlags;
	this->serviceHandler = serviceHandler;
	this->authFlags = authFlags;
}


BLECharacteristic::~BLECharacteristic()
{
}


uint8_t BLECharacteristic::updateValue(uint8_t *value)
{
	tBleStatus ret = aci_gatt_update_char_value(this->serviceHandler, this->handler, 0, this->valueSize, value);
}


void BLECharacteristic::setServiceHandler(uint16_t serviceHandler)
{
	this->serviceHandler = serviceHandler;
}


uint8_t BLECharacteristic::registerCharacteristic()
{
	tBleStatus ret;
	ret = aci_gatt_add_char(
		serviceHandler ,					//handler of service
		uuid.getType(),						//uuid type
		uuid.getUUID(),						//characteristic uuid
		valueSize,							//number of bytes
		propertiesFlags,					//characteristic properties
		authFlags,							//authentication
		BLECharacteristic::responseMask,	//read response will generated
		16,									//max encryption size
		0,									//attribute variables not vary
		&handler							//handler of the characteristic
	);
}
