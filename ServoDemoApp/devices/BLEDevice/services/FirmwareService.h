#pragma once
#include "BLEService.h"
class FirmwareService : public BLEService
{
  public:
	FirmwareService();
	~FirmwareService();

  private:
	static const char *serviceUUID;		//service uuid
	static const size_t attrValue = 5; //1x service declaration and 2x declaration + value
	static const char *charFirmwareCommandAndStatusUUID;
	static const char *charFirmwareDataUUID;

};