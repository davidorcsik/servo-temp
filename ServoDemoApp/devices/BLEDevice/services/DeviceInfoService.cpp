
#include "DeviceInfoService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char *DeviceInfoService::serviceUUID = "180A";
const char *DeviceInfoService::charManufacturerDataUUID = "2A29";
const char *DeviceInfoService::charModelNumberUUID = "2A24";
const char *DeviceInfoService::charSerialNumberUUID = "2A25";
const char *DeviceInfoService::charHWRevisionUUID = "2A27";
const char *DeviceInfoService::charSWRevisionUUID = "2A28";

DeviceInfoService::DeviceInfoService() : BLEService(DeviceInfoService::serviceUUID, 5, DeviceInfoService::attrValue)
{
	BLECharacteristic ManufacturerDataChar(DeviceInfoService::charManufacturerDataUUID, CHAR_PROP_READ | CHAR_PROP_WRITE,20, 0, ATTR_PERMISSION_AUTHEN_WRITE );	//maximum attribute size is 20 (this is the maxiumum length which can be sent in one packet)
	addChar(ManufacturerDataChar);
	BLECharacteristic ModelNumber(DeviceInfoService::charModelNumberUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_WRITE); 
	addChar(ModelNumber);
	BLECharacteristic SerialNumber(DeviceInfoService::charSerialNumberUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_WRITE); 
	addChar(SerialNumber);
	BLECharacteristic HWRevision(DeviceInfoService::charHWRevisionUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_WRITE); 
	addChar(HWRevision);
	BLECharacteristic SWRevision(DeviceInfoService::charSWRevisionUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_WRITE); 
	addChar(SWRevision);
	
}

DeviceInfoService::~DeviceInfoService()
{
}

uint8_t DeviceInfoService::updateManufacturerData(char *value)
{
	chars[0].updateValue((uint8_t *)value);
}
uint8_t DeviceInfoService::updateModelNumber(char *value)
{
	chars[1].updateValue((uint8_t *)value);
}
uint8_t DeviceInfoService::updateSerialNumber(char *value)
{
	chars[2].updateValue((uint8_t *)value);
}
uint8_t DeviceInfoService::updateHWRevision(char *value)
{
	chars[3].updateValue((uint8_t *)value);
}
uint8_t DeviceInfoService::updateSWRevision(char *value)
{
	chars[4].updateValue((uint8_t *)value);
}