#pragma once
#include "BLEService.h"
class  BatteryService : public BLEService
{
  public:
	BatteryService();
	~BatteryService();
	
  private:
	static const char* serviceUUID;
	static const char* charUUID;
	

  public:
	uint8_t updateBatteryValue(uint8_t value);
};