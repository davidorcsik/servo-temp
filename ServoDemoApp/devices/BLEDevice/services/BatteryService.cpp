
#include "BatteryService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char* BatteryService::serviceUUID = "180F";
const char *BatteryService::charUUID = "2A19";

BatteryService::BatteryService() : BLEService(BatteryService::serviceUUID, 1, 0x05)
{
	BLECharacteristic batteryLevelChar(BatteryService::charUUID, CHAR_PROP_READ | CHAR_PROP_NOTIFY, 1, 0, ATTR_PERMISSION_NONE);
	addChar(batteryLevelChar);
}

uint8_t BatteryService::updateBatteryValue(uint8_t value)
{
	chars[0].updateValue(&value);
}

BatteryService::~BatteryService()
{
}