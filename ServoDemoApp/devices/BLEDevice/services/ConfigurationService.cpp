#include "ConfigurationService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char *ConfigurationService::serviceUUID = "C2E68EEBA75A4ECAA6E3FC3B60FCFFEC";					
const char *ConfigurationService::charAdvertisingNameUUID = "A5124D8538AB4D65AD24B6631459A5A9"; 
const char *ConfigurationService::charPINUUID = "80FC466E8586436AB4861CD89B2592BF";
const char *ConfigurationService::charPowerLimitUUID = "B3A770A996C64E81A27B3E0626679B6C";
const char *ConfigurationService::charNotificationFrequencyUUID = "3409563C41B94891A549F4A77AF3C26A";
const char *ConfigurationService::charSleepUplinkFreqUUID = "65322EDE15AE4D789233D4FE79714577 ";
const char *ConfigurationService::charNormalUplinkFreqUUID = "3324009A1EA5492898A887452191C645";

ConfigurationService::ConfigurationService() : BLEService(ConfigurationService::serviceUUID, 6, ConfigurationService ::attrValue)
{
	BLECharacteristic AdvertisingName(ConfigurationService::charAdvertisingNameUUID, CHAR_PROP_READ|CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_READ|ATTR_PERMISSION_AUTHOR_WRITE); 
	addChar(AdvertisingName);
	BLECharacteristic PIN(ConfigurationService::charPINUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 4, 0, ATTR_PERMISSION_AUTHEN_READ | ATTR_PERMISSION_AUTHOR_WRITE); 
	addChar(PIN);
	BLECharacteristic PowerLimit(ConfigurationService::charPowerLimitUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 1, 0, ATTR_PERMISSION_AUTHEN_READ | ATTR_PERMISSION_AUTHOR_WRITE); 
	addChar(PowerLimit);
	BLECharacteristic NotificationFreq(ConfigurationService::charNotificationFrequencyUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 2, 0, ATTR_PERMISSION_AUTHEN_READ | ATTR_PERMISSION_AUTHOR_WRITE); 
	addChar(NotificationFreq);
	BLECharacteristic SleepUplinkFreq(ConfigurationService::charSleepUplinkFreqUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 2, 0, ATTR_PERMISSION_AUTHEN_READ | ATTR_PERMISSION_AUTHOR_WRITE); 
	addChar(SleepUplinkFreq);
	BLECharacteristic NormalUplinkFreq(ConfigurationService::charNormalUplinkFreqUUID, CHAR_PROP_READ | CHAR_PROP_WRITE, 2, 0, ATTR_PERMISSION_AUTHEN_READ | ATTR_PERMISSION_AUTHOR_WRITE);  
	addChar(NormalUplinkFreq);
}

ConfigurationService::~ConfigurationService()
{
}

uint8_t ConfigurationService::updateAdvertisingName(char *value)
{
	uint8_t ret = 0xff;

	ret = chars[0].updateValue((uint8_t *)value);
	return ret;
}

uint8_t ConfigurationService::updatePIN(uint8_t *value)
{
	uint8_t ret = 0xff;

	ret = chars[1].updateValue(value);
	return ret;
}

uint8_t ConfigurationService::updatePowerLimit(uint8_t value)
{
	uint8_t ret = 0xff;

	ret = chars[2].updateValue(&value);
	return ret;
}
uint8_t ConfigurationService::updateNotificationFreq(uint16_t value)
{
	uint8_t ret = 0xff;
	uint8_t value_8t[2] = {0};
	value_8t[0] = (uint8_t)(value & 0x00FF);
	value_8t[1] = (uint8_t)(value >> 8);

	ret = chars[3].updateValue(value_8t);
	return ret;
}
uint8_t ConfigurationService::updateSleepUplinkFreq(uint16_t value)
{
	uint8_t ret = 0xff;
	
	uint8_t value_8t[2] = {0};
	value_8t[0] = (uint8_t)(value & 0x00FF);
	value_8t[1] = (uint8_t)(value >> 8);

	ret = chars[4].updateValue(value_8t);
	return ret;
}
uint8_t ConfigurationService::updateNormalUplinkFreq(uint16_t value)
{
	uint8_t ret = 0xff;
	
	uint8_t value_8t[2] = {0};
	value_8t[0] = (uint8_t)(value & 0x00FF);
	value_8t[1] = (uint8_t)(value >> 8);

	ret = chars[5].updateValue(value_8t);
	return ret;
}