#include "BLEService.h"
extern "C" {
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
}

BLEService::BLEService()
{
	dataCallback = nullptr;
}

BLEService::BLEService(BLEUUID uuid, size_t numOfChars, uint8_t maxAttributeValue, bool visible)
{
	this->uuid = uuid;
	chars.reserve(numOfChars);
	this->maxAttributeValue = maxAttributeValue;
	this->visible=visible;
	dataCallback = nullptr;
}

BLEService::~BLEService()
{
}

void BLEService::addChar(BLECharacteristic charToAdd)
{
	charToAdd.setServiceHandler(handler);
	chars.push_back(charToAdd);
}

void BLEService::updateCharacteristics()
{
	for (size_t i = 0; i < chars.size(); i++)
	{
		chars[i].setServiceHandler(handler);
	}
}

uint8_t BLEService::getMaxAttributeValue()
{
	return maxAttributeValue;
}
BLEUUID BLEService::getUUID()
{
	return uuid;
}

void BLEService::setHandler(uint16_t handler)
{
	this->handler = handler;
}




uint8_t BLEService::registerCharacteristics()
{
	for (size_t i = 0; i < chars.size(); i++)
	{
		chars[i].setServiceHandler(handler);
		chars[i].registerCharacteristic();
	}
	return 0;
}


void BLEService::registerService()
{
	tBleStatus ret;
	ret = aci_gatt_add_serv(
		uuid.getType(),				//uuid type
		uuid.getUUID(),				//uuid
		PRIMARY_SERVICE,			//define service as a primary service
		maxAttributeValue,			//max attribute records with service declaration
		&handler					//handler of service
	);
}


bool BLEService::isVisible()
{
	return visible;
}


void BLEService::setCallback(DataReceivedCallback fn)
{
	dataCallback = fn;
}


void BLEService::runCallback(void* pData)
{
	if (dataCallback != nullptr)
	{
		dataCallback(pData);
	}
}
