#pragma once
#include "BLEService.h"
class  DeviceInfoService : public BLEService
{
  public:
	DeviceInfoService();
	~DeviceInfoService();
	
  private:
	static const char* serviceUUID;				//service uuid
	static const size_t attrValue = 11;			//1x service declaration and 5xvalue+declaration (no notification)
	static const char* charManufacturerDataUUID;
	static const char* charModelNumberUUID;
	static const char* charSerialNumberUUID;
	static const char* charHWRevisionUUID;
	static const char* charSWRevisionUUID;

  public:
	uint8_t updateManufacturerData(char* value);
	uint8_t updateModelNumber(char *value);
	uint8_t updateSerialNumber(char *value);
	uint8_t updateHWRevision(char *value);
	uint8_t updateSWRevision(char *value);
};