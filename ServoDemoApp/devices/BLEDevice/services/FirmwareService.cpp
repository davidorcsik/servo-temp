#include "FirmwareService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char *FirmwareService::serviceUUID = "514D57F921C549C0AAA761D8704361EF";	//UUID of the service
const char *FirmwareService::charFirmwareCommandAndStatusUUID = "36D07CF92F7C463884E8545D5952B2C9";	//UUID-s of the two included characteristics
const char *FirmwareService::charFirmwareDataUUID = "A8F6CFDD51144E429E8810ACCD17B8D0";

FirmwareService::FirmwareService() : BLEService(FirmwareService::serviceUUID, 2, FirmwareService ::attrValue)
{
	BLECharacteristic FirmwareCommandAndStatus(FirmwareService::charFirmwareCommandAndStatusUUID, CHAR_PROP_WRITE,3, 0, ATTR_PERMISSION_AUTHEN_WRITE); //maximum attribute size is 20 (this is the maxiumum length which can be sent in one packet)
	addChar(FirmwareCommandAndStatus);
	BLECharacteristic FirmwareData(FirmwareService::charFirmwareDataUUID, CHAR_PROP_WRITE, 20, 0, ATTR_PERMISSION_AUTHEN_WRITE); 
	addChar(FirmwareData);
}

FirmwareService::~FirmwareService()
{
}
