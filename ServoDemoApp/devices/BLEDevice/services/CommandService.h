#pragma once
#include "BLEService.h"
class CommandService : public BLEService
{
  public:
	CommandService();
	~CommandService();

  private:
	static const char *serviceUUID;
	static const char *charCommandCharacteristicUUID;
	static const size_t attrValue = 3;
};