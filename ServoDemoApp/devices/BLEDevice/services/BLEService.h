#pragma once
#include <vector>
#include "../BLEUUID.h"
#include "../characteristics/BLECharacteristic.h"
class BLEService
{
  public:
	typedef void (*DataReceivedCallback)(void *pData);

  public:
	BLEService();
	BLEService(BLEUUID uuid, size_t numOfChars, uint8_t maxAttributeValue, bool visible = true);
	~BLEService();

  public:
	void addChar(BLECharacteristic charToAdd);
	void setHandler(uint16_t handler);
	void registerService();
	void updateCharacteristics();
	uint8_t getMaxAttributeValue();
	BLEUUID getUUID();
	uint8_t registerCharacteristics();
	bool isVisible();
	void runCallback(void* pData);
	void setCallback(DataReceivedCallback fn);

  protected:
	std::vector<BLECharacteristic> chars;
	uint8_t maxAttributeValue;
	BLEUUID uuid;
	uint16_t handler = 0;
	bool visible = true;
	DataReceivedCallback dataCallback;
};

