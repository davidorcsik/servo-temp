
#include "CommandService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char *CommandService::serviceUUID = "E483531C16394780A6AA805B9CAFB1DE";
const char *CommandService::charCommandCharacteristicUUID = "14638B17F8AA4920B8C8D7630B9FE812";

CommandService::CommandService() : BLEService(CommandService::serviceUUID, 1, CommandService::attrValue)
{
	BLECharacteristic Command(CommandService::charCommandCharacteristicUUID, CHAR_PROP_WRITE, 1, 0, ATTR_PERMISSION_AUTHEN_WRITE);
	addChar(Command);
}

CommandService::~CommandService()
{
}