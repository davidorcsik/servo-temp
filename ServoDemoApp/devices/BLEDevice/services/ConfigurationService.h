#pragma once
#include "BLEService.h"
class ConfigurationService : public BLEService
{
  public:
	ConfigurationService();
	~ConfigurationService();

  private:
	static const char *serviceUUID;	//service uuid
	static const size_t attrValue = 13; //1x service declaration and 6x declaration + value (no notification)
	static const char *charAdvertisingNameUUID;
	static const char *charPINUUID;
	static const char *charPowerLimitUUID;
	static const char *charNotificationFrequencyUUID;
	static const char *charSleepUplinkFreqUUID;
	static const char *charNormalUplinkFreqUUID;
	
  public:
	uint8_t updateAdvertisingName(char *value);
	uint8_t updatePIN(uint8_t *value);
	uint8_t updatePowerLimit(uint8_t value);
	uint8_t updateNotificationFreq(uint16_t value);
	uint8_t updateSleepUplinkFreq(uint16_t value);
	uint8_t updateNormalUplinkFreq(uint16_t value);
};