
#include "TelemetryService.h"

#include "BLEService.h"

#include "../characteristics/BLECharacteristic.h"

const char *TelemetryService::serviceUUID = "9D984E4F862F4CB2B52A1761DE341AB4";
const char *TelemetryService::charNotificationDataUUID = "20001E298E864D61A3165C4AEDA30A07";
const char *TelemetryService::charBatteryLevelMainUUID = "2A19";
const char *TelemetryService::charBatteryLevelBackupUUID = "2A19";

TelemetryService::TelemetryService() : BLEService(TelemetryService::serviceUUID, 3, TelemetryService::attrValue)
{
	BLECharacteristic NotificationData(TelemetryService::charNotificationDataUUID, CHAR_PROP_NOTIFY | CHAR_PROP_READ , 18, 0, ATTR_PERMISSION_NONE); //no permission (notification should be started after successfull pairing)
	addChar(NotificationData);
	BLECharacteristic BatteryLevelMain(TelemetryService::charBatteryLevelMainUUID, CHAR_PROP_NOTIFY|CHAR_PROP_READ, 1, 0, ATTR_PERMISSION_NONE);
	addChar(BatteryLevelMain);
	BLECharacteristic BatteryLevelBackup(TelemetryService::charBatteryLevelBackupUUID, CHAR_PROP_NOTIFY|CHAR_PROP_READ, 1, 0, ATTR_PERMISSION_NONE);
	addChar(BatteryLevelBackup);
	
}

TelemetryService::~TelemetryService()
{
}

uint8_t TelemetryService::updateNotificationData(uint8_t *value,size_t value_size)
{
	uint8_t ret = 0xff;			//0x00 is BLE_STATUS_SUCCESS
	
	ret = chars[0].updateValue(value);
	return ret;
}
uint8_t TelemetryService::updateBatteryLevelMain(uint8_t value)
{
	uint8_t ret = 0xff;
	
	ret = chars[1].updateValue(&value);
	return ret;
}
uint8_t TelemetryService::updateBatteryLevelBackup(uint8_t value)
{
	uint8_t ret = 0xff;
	
	ret = chars[2].updateValue(&value);
	return ret;
}
