#pragma once
#include "BLEService.h"
class TelemetryService : public BLEService
{
  public:
	TelemetryService();
	~TelemetryService();

  private:
	static const char *serviceUUID;		//service uuid
	static const size_t attrValue = 10; //1x service declaration and 3x value + declaration + client configuration descriptor 
	static const char *charNotificationDataUUID;
	static const char *charBatteryLevelMainUUID;
	static const char *charBatteryLevelBackupUUID;

  public:
	uint8_t updateNotificationData(uint8_t *value, size_t value_size);
	uint8_t updateBatteryLevelMain(uint8_t value);
	uint8_t updateBatteryLevelBackup(uint8_t value);
};