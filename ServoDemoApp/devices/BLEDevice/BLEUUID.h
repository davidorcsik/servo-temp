#pragma once
#include <vector>
extern "C" {
#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
}
enum UUIDType
{
	UUID_2_BYTE = UUID_TYPE_16,
	UUID_16_BYTE = UUID_TYPE_128
};

class BLEUUID
{
  public:
	BLEUUID();
	BLEUUID(const char *uuidStr);

  public:
	const uint8_t *getUUID();
	const size_t getLen();
	const UUIDType getType();

  private:
	std::vector<uint8_t> uuid;
	UUIDType type;
};