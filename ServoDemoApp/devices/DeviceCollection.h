#pragma once

#include <stm32l4xx_hal_uart.h>
#include <stm32l4xx_hal_i2c.h>
#include <stm32l4xx_hal_can.h>

#include <protocols/point2point/UART.h>
#include <protocols/bus/I2CBus.h>
#include <protocols/bus/CANBus.h>

#include <tempsensor/TemperatureSensor.h>
#include <accelerometer/accelerometer.h>
#include <lora/LoRa.h>
#include <gps/GPS.h>
#include <debugconsole/DebugConsole.h>
#include <BLEDevice/BLEDevice.h>
#include <digitalio/digitalio.h>
#include <power_manager.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern I2C_HandleTypeDef hi2c1;
extern CAN_HandleTypeDef hcan1;

class DeviceCollection {
private:
	static bool initialized;
	static uint8_t bleAddress[6];

  public:
	static protocols::UART uartToGPS;
	static protocols::UART uartToLora;
	static protocols::UART uartToDebug;
	static protocols::CANBus canBus;
	static protocols::I2C i2cBus;
	
	static devices::GPS gps;
	static devices::LoRa loraHandle;
	static devices::TempSensor tempSensor;
	static devices::Accelerometer accelerometer;
	static devices::BLEDevice bleDevice;
	static devices::PowerManager powerManager;

	static devices::DigitalIO in1;
	static devices::DigitalIO in2;
	static devices::DigitalIO out1;
	static devices::DigitalIO out2;

	static void init();
};