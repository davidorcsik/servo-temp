#pragma once
#include <inttypes.h>
#include <array>

namespace Utils
{
	template <typename T, size_t SIZE>
	class StaticQueue {
	public:
		enum STATUS { OK, FULL, EMPTY, TOO_LONG };
	private:
		T container[SIZE];
		size_t pos = 0;
		size_t length = 0;
	public:
		StaticQueue() {}
		virtual ~StaticQueue() {}
		
		size_t size() const {
			return length;
		}
		
		STATUS push(T element) {
			if (length == SIZE) {
				return STATUS::FULL;
			}
			container[(pos + length) % SIZE] = element;
			++length;
		}
		
		STATUS push_all(T* elements, size_t element_count) {
			if (length + element_count > SIZE) {
				return STATUS::TOO_LONG;
			}
			for (size_t i = 0; i < element_count; ++i) {
				container[(pos + length + i) % SIZE] = elements[i];
			}
		}
		
		STATUS pop(T& output) {
			if (length == 0) {
				return STATUS::EMPTY;
			}
			--length;
			output = container[pos];
			pos = (pos + 1) % SIZE;
			return STATUS::OK;
		}
		
		bool empty() const {
			return length == 0;
		}
	};
}