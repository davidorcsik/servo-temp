#include "accelerometer.h"

namespace devices {
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::STATUS =				{ 0x00, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_X =				{ 0x01, 2 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_X_MSB =			{ 0x01, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_X_LSB =			{ 0x02, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Y =				{ 0x03, 2 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Y_MSB =			{ 0x03, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Y_LSB =			{ 0x04, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Z =				{ 0x05, 2 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Z_MSB =			{ 0x05, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OUT_Z_LSB =			{ 0x05, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::SYSMOD =				{ 0x0b, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::INT_SOURCE =			{ 0x0c, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::WHO_AM_I =			{ 0x0d, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::XYZ_DATA_CFG =		{ 0x0e, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::HP_FILTER_CUTOFF =	{ 0x0f, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PL_STATUS =			{ 0x10, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PL_CFG =				{ 0x11, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PL_COUNT =			{ 0x12, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PL_BF_ZCOMP =		{ 0x13, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PL_THS_REG =			{ 0x14, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::FF_MT_CFG =			{ 0x15, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::FF_MT_SRC =			{ 0x16, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::FF_MT_THS =			{ 0x17, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::FF_MT_COUNT =		{ 0x18, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::TRANSIENT_CFG =		{ 0x1d, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::TRANSIENT_SRC =		{ 0x1e, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::TRANSIENT_THS =		{ 0x1f, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::TRANSIENT_COUNT =	{ 0x20, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_CFG =			{ 0x21, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_SRC =			{ 0x22, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_THSX =			{ 0x23, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_THSY =			{ 0x24, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_THSZ =			{ 0x25, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_TMLT =			{ 0x26, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_LTCY =			{ 0x27, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::PULSE_WIND =			{ 0x28, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::ASLP_COUNT =			{ 0x29, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::CTRL_REG1 =			{ 0x2a, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::CTRL_REG2 =			{ 0x2b, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::CTRL_REG3 =			{ 0x2c, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::CTRL_REG4 =			{ 0x2d, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::CTRL_REG5 =			{ 0x2e, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OFF_X =				{ 0x2f, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OFF_Y =				{ 0x30, 1 };
	const Accelerometer::REGISTERS::REGISTER Accelerometer::REGISTERS::OFF_Z =				{ 0x31, 1 };
	
	const std::map<Accelerometer::RESOLUTION, float> Accelerometer::RESOLUTION_CORRECTION_MULTIPLIERS = { { Accelerometer::RESOLUTION::RES_10BIT, 0.0039f }, { Accelerometer::RESOLUTION::RES_8BIT, 0.0156f } };
	
	Accelerometer::STATUS Accelerometer::init() {
		// going to standby to configure
		STATUS status = go_standby();
		if (status != STATUS::OK) {
			return status;
		}
		auto read_result = read_register(REGISTERS::CTRL_REG1);
		if (!read_result.has_value()) {
			return STATUS::READ_ERROR;
		}
		uint8_t standby_register_value = read_result.value();
		
		//set resolution
		if (resolution == RESOLUTION::RES_10BIT) {
			standby_register_value = standby_register_value & static_cast<uint8_t>(resolution); //according to documentation
		} else {
			standby_register_value = standby_register_value | static_cast<uint8_t>(resolution); //according to documentation
		}
		standby_register_value = standby_register_value | active_mask; //wake
		STATUS write_status = write_register(REGISTERS::CTRL_REG1, &standby_register_value);
		if (write_status != STATUS::OK) {
			return write_status;
		}
		
		// activate
		status = go_active();
		if (status != STATUS::OK) {
			return status;
		}
		
		read_result = read_register(REGISTERS::XYZ_DATA_CFG);
		if (!read_result.has_value()) {
			return { };
		}
		range = read_result.value();
		
		return STATUS::OK;
	}
	Accelerometer::STATUS Accelerometer::go_standby() {
		auto read_result = read_register(REGISTERS::CTRL_REG1);
		if (!read_result.has_value()) {
			return STATUS::READ_ERROR;
		}
		uint8_t standby_register_value = read_result.value();
		standby_register_value = standby_register_value & ~active_mask;  //according to documentation
		STATUS write_status = write_register(REGISTERS::CTRL_REG1, &standby_register_value);
		if (write_status != STATUS::OK) {
			return write_status;
		}
	}
	Accelerometer::STATUS Accelerometer::go_active() {
		auto read_result = read_register(REGISTERS::CTRL_REG1);
		if (!read_result.has_value()) {
			return STATUS::READ_ERROR;
		}
		uint8_t standby_register_value = read_result.value();
		standby_register_value = standby_register_value | active_mask;   //according to documentation
		STATUS write_status = write_register(REGISTERS::CTRL_REG1, &standby_register_value);
		if (write_status != STATUS::OK) {
			return write_status;
		}
	}
	std::optional<uint16_t> Accelerometer::read_register(const Accelerometer::REGISTERS::REGISTER& register_addr) {
		serialMessage.set_payload_length(register_addr.length);
		serialMessage.set_register_address(register_addr.address);
		const protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return { };
		}
		if (register_addr.length == 1) {
			return serialMessage.get_payload()[0];
		}
		return static_cast<uint16_t>(serialMessage.get_payload()[0]) << 8 | static_cast<uint16_t>(serialMessage.get_payload()[1]);
	}
	
	Accelerometer::STATUS Accelerometer::write_register(const Accelerometer::REGISTERS::REGISTER& register_addr, const uint8_t* data) {
		serialMessage.set_payload(data, register_addr.length);
		serialMessage.set_register_address(register_addr.address);
		const protocols::ISerial::STATUS status = serialHandle.write(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return last_error;
		}
		const auto read_result = read_register(register_addr);
		if (!read_result.has_value()) {
			last_error = STATUS::READ_ERROR;
			return last_error;
		}
		const uint16_t write_result = read_result.value();
		uint16_t data_temp = data[0];
		if (register_addr.address == 2) {
			data_temp = data_temp << 8;
			data_temp |= data[1];
		}
		if (write_result != data_temp) {
			last_error = STATUS::WRITE_ERROR;
			return last_error;
		}
		return STATUS::OK;
	}
	
	std::optional<Accelerometer::DATA_STATE> Accelerometer::read_status() {
		serialMessage.set_payload_length(REGISTERS::STATUS.length);
		serialMessage.set_register_address(REGISTERS::STATUS.address);
		const protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return { };
		}
		DATA_STATE state;
		state.value = serialMessage.get_payload()[0];
		return state;
	}	
	std::optional<Accelerometer::acceleration> Accelerometer::read_acceleration() {
		serialMessage.set_payload_length(6);
		serialMessage.set_register_address(REGISTERS::OUT_X_MSB.address);
		protocols::ISerial::STATUS status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			return {};
		}
		
		int16_t raw_x = 0;
		int16_t raw_y = 0;
		int16_t raw_z = 0;
		
		if (resolution == RESOLUTION::RES_10BIT) {
			raw_x = (serialMessage.get_payload()[0] << 2) | ((serialMessage.get_payload()[1] >> 6) & 0x3);
			raw_y = (serialMessage.get_payload()[2] << 2) | ((serialMessage.get_payload()[3] >> 6) & 0x3);
			raw_z = (serialMessage.get_payload()[4] << 2) | ((serialMessage.get_payload()[5] >> 6) & 0x3);
		} else {
			raw_x = static_cast<int8_t>(serialMessage.get_payload()[0]);
			raw_y = static_cast<int8_t>(serialMessage.get_payload()[2]);
			raw_z = static_cast<int8_t>(serialMessage.get_payload()[4]);
		}
    
		if (raw_x > 511) {
			raw_x -= 1024;
		}
		if (raw_y > 511) {
			raw_y -= 1024;
		}
		if (raw_z > 511) {
			raw_z -= 1024;
		}
    
		float range_multiplier = (range * 2 + 1) * RESOLUTION_CORRECTION_MULTIPLIERS.at(resolution);
		
		acceleration result;
		result.x = raw_x * range_multiplier;
		result.y = raw_y * range_multiplier;
		result.z = raw_z * range_multiplier;
		return result;
	}
	
	std::optional<Accelerometer::STATUS> Accelerometer::get_last_error() {
		if (last_error == STATUS::OK) {
			return { };
		}
		return last_error;
	}
	std::optional<uint8_t> Accelerometer::read_alarm_register() {
		return read_register(REGISTERS::FF_MT_CFG);
	}
	Accelerometer::STATUS Accelerometer::set_alarm(Accelerometer::ALARM_STATE state, uint8_t debounce_treshold, uint8_t movement_treshold) {
		// going standby to configure
		STATUS status = go_standby();
		if (status != STATUS::OK) {
			return status;
		}
		
		// setting motion sensor event latch
		std::optional<uint8_t> read_result = read_register(REGISTERS::FF_MT_CFG);
		if (!read_result.has_value()) {
			return last_error;
		}
		uint8_t movement_config_register = read_result.value();
		
		if (state == ALARM_STATE::DISABLED) {
			movement_config_register &= static_cast<uint8_t>(state);
		} else {
			movement_config_register |= static_cast<uint8_t>(state);
		}
		
		status = write_register(REGISTERS::FF_MT_CFG, &movement_config_register);
		if (status != STATUS::OK) {
			return status;
		}
		
		// setting debounce counter
		status = write_register(REGISTERS::FF_MT_COUNT, &debounce_treshold);
		if (status != STATUS::OK) {
			return status;
		}
		
		// setting treshold
		if(movement_treshold > 127) {
			return STATUS::INVALID_MOTION_TRESHOLD_VALUE;
		}
		movement_treshold += 128;
		status = write_register(REGISTERS::FF_MT_THS, &movement_treshold);
		if (status != STATUS::OK) {
			return status;
		}
		
		// enable freefall/motion interupt
		read_result = read_register(REGISTERS::CTRL_REG4);
		if (!read_result.has_value()) {
			return last_error;
		}
		uint8_t control_register_4 = read_result.value();
		control_register_4 |= 0b00000100;
		
		status = write_register(REGISTERS::CTRL_REG4, &control_register_4);
		if (status != STATUS::OK) {
			return status;
		}
		
		// set interupt pin
		read_result = read_register(REGISTERS::CTRL_REG5);
		if (!read_result.has_value()) {
			return status;
		}
		uint8_t control_register_5 = read_result.value();
		control_register_5 |= 0b00000100;
		status = write_register(REGISTERS::CTRL_REG5, &control_register_5);
		
		status = go_active();
		if (status != STATUS::OK) {
			return status;
		}
		get_motion_data();
		return STATUS::OK;
	}
	std::optional<Accelerometer::motion> Accelerometer::get_motion_data() {
		std::optional<uint8_t> motion_data = read_register(devices::Accelerometer::REGISTERS::FF_MT_SRC);
		if (!motion_data.has_value()) {
			return { };
		}
		return motion_data.value();
	}
	Accelerometer::STATUS Accelerometer::set_auto_wake_sleep(Accelerometer::AUTO_WAKE_SLEEP_STATE state, uint8_t time, Accelerometer::SLEEP_MODE_SAMPLE_TIME sample_time) {
		STATUS status = go_standby(); // going standby for configuration
		
		// setting auto wake/sleep state
		std::optional<uint8_t> read_result = read_register(REGISTERS::CTRL_REG2);
		if (!read_result.has_value()) {
			return last_error;
		}
		uint8_t control_register_2 = read_result.value();
		
		if (state == AUTO_WAKE_SLEEP_STATE::DISABLED) {
			control_register_2 &= static_cast<uint8_t>(state);
		} else {
			control_register_2 |= static_cast<uint8_t>(state);
		}
		
		status = write_register(REGISTERS::CTRL_REG2, &control_register_2);
		if (status != STATUS::OK) {
			return status;
		}
		
		if (state == AUTO_WAKE_SLEEP_STATE::DISABLED) {
			status = go_active();
			return status;
		}
		
		// setting auto wake/sleep time
		status = write_register(REGISTERS::ASLP_COUNT, &time);
		
		// set sleep mode sample frequency
		read_result = read_register(REGISTERS::CTRL_REG1);
		if (!read_result.has_value()) {
			return last_error;
		}
		uint8_t control_register_1 = read_result.value();
		control_register_1 &= static_cast<uint8_t>(sample_time);
		
		status = write_register(REGISTERS::CTRL_REG1, &control_register_1);
		if (status != STATUS::OK) {
			return status;
		}
				
		status = go_active(); // activate
		return status;
	}
}