#pragma once

#include <ISerial.h>
#include <utils.h>

#include <stm32l4xx_hal.h>

#include <optional>
#include <map>

namespace devices {
	class Accelerometer {
	public:
#pragma pack(push, 1)
		union DATA_STATE {
			struct {
				uint8_t xyz_overwrite : 1;
				uint8_t z_overwrite : 1;
				uint8_t y_overwrite : 1;
				uint8_t x_overwrite : 1;
				uint8_t xyz_ready : 1;
				uint8_t z_ready : 1;
				uint8_t y_ready : 1;
				uint8_t x_ready : 1;
			} segments;
			uint8_t value;
		};
#pragma pack(pop)
		enum class STATUS {
			OK               = HAL_OK,
			HARDWARE_ERROR   = HAL_ERROR,
			HARDWARE_BUSY    = HAL_BUSY,
			HARDWARE_TIMEOUT = HAL_TIMEOUT,
			WRITE_ERROR,
			READ_ERROR,
			INVALID_MOTION_TRESHOLD_VALUE
		};
		struct REGISTERS {
			struct REGISTER {
				const uint8_t address;
				const uint8_t length;
				REGISTER(uint8_t _address, uint8_t _length) : address(_address), length(_length) {}
			};
			static const REGISTER STATUS;
			static const REGISTER OUT_X;
			static const REGISTER OUT_X_MSB;
			static const REGISTER OUT_X_LSB;
			static const REGISTER OUT_Y;
			static const REGISTER OUT_Y_MSB;
			static const REGISTER OUT_Y_LSB;
			static const REGISTER OUT_Z;
			static const REGISTER OUT_Z_MSB;
			static const REGISTER OUT_Z_LSB;
			static const REGISTER SYSMOD;
			static const REGISTER INT_SOURCE;
			static const REGISTER WHO_AM_I;
			static const REGISTER XYZ_DATA_CFG;
			static const REGISTER HP_FILTER_CUTOFF;
			static const REGISTER PL_STATUS;
			static const REGISTER PL_CFG;
			static const REGISTER PL_COUNT;
			static const REGISTER PL_BF_ZCOMP;
			static const REGISTER PL_THS_REG;
			static const REGISTER FF_MT_CFG;
			static const REGISTER FF_MT_SRC;
			static const REGISTER FF_MT_THS;
			static const REGISTER FF_MT_COUNT;
			static const REGISTER TRANSIENT_CFG;
			static const REGISTER TRANSIENT_SRC;
			static const REGISTER TRANSIENT_THS;
			static const REGISTER TRANSIENT_COUNT;
			static const REGISTER PULSE_CFG;
			static const REGISTER PULSE_SRC;
			static const REGISTER PULSE_THSX;
			static const REGISTER PULSE_THSY;
			static const REGISTER PULSE_THSZ;
			static const REGISTER PULSE_TMLT;
			static const REGISTER PULSE_LTCY;
			static const REGISTER PULSE_WIND;
			static const REGISTER ASLP_COUNT;
			static const REGISTER CTRL_REG1;
			static const REGISTER CTRL_REG2;
			static const REGISTER CTRL_REG3;
			static const REGISTER CTRL_REG4;
			static const REGISTER CTRL_REG5;
			static const REGISTER OFF_X;
			static const REGISTER OFF_Y;
			static const REGISTER OFF_Z;
		};
		enum class DEVICE_ADDRESSES {
			SA0_LOW =	0b0011100 << 1,
			SA0_HIGH =	0b0011101 << 1
		};
		enum class RESOLUTION {
			RES_8BIT =	~2,
			RES_10BIT =	2
		};
		enum class ALARM_STATE {
			DISABLED =	0b01000000,
			FREEFALL =	0b10111000,
			MOTION =	0b11111000
		};
		enum class AUTO_WAKE_SLEEP_STATE {
			DISABLED =	0b11111011,
			ENABLED =	0b00000100
		};
		enum class SLEEP_MODE_SAMPLE_TIME {
			PER_50_SECS =	0b00000000,
			PER_12_5_SECS =	0b01000000,
			PER_6_25_SECS =	0b10000000,
			PER_1_56_SECS =	0b11000000
		};
		struct acceleration {
			float x;
			float y;
			float z;
		};
		struct motion {
			uint8_t value;
			bool flag_x() { return (value & 0b00000010) != 0; }
			bool flag_y() { return (value & 0b00001000) != 0; }
			bool flag_z() { return (value & 0b00100000) != 0; }
			bool flag_any() { return flag_x() || flag_y() || flag_z(); }
			motion(uint8_t _value) : value(_value) { }
		};
	private:
		static const std::map<RESOLUTION, float> RESOLUTION_CORRECTION_MULTIPLIERS;
		
		protocols::ISerial& serialHandle;
		protocols::SerialMessage serialMessage;
		const DEVICE_ADDRESSES device_address;
		const uint8_t active_mask = 0x01;
		const RESOLUTION resolution;
		STATUS last_error = STATUS::OK;
		uint8_t range;
		
		STATUS write_register(const REGISTERS::REGISTER& register_addr, const uint8_t* data);
		STATUS go_standby();
		STATUS go_active();
	public:
		Accelerometer(protocols::ISerial& _serialHandle, DEVICE_ADDRESSES _device_address, RESOLUTION _resolution)
		: serialHandle(_serialHandle), device_address(_device_address), resolution(_resolution) {
			std::optional<protocols::SerialMessage> created_message = serialHandle.create_message(nullptr, 0, 100, static_cast<uint32_t>(device_address), protocols::ISerial::DEFAULT_REGISTER);
			if (!created_message.has_value()) {
				Utils::ERROR_GUARD();
			}
			serialMessage = created_message.value();
		}
		virtual ~Accelerometer() {}
		
		STATUS init();
		std::optional<Accelerometer::DATA_STATE>  read_status();
		std::optional<uint16_t> read_register(const REGISTERS::REGISTER& register_addr);
		std::optional<acceleration> read_acceleration();
		std::optional<STATUS> get_last_error();
		std::optional<uint8_t> read_alarm_register();
		STATUS set_alarm(ALARM_STATE state, uint8_t debounce_treshold = 0, uint8_t movement_treshold = 0);
		std::optional<motion> get_motion_data();
		STATUS set_auto_wake_sleep(AUTO_WAKE_SLEEP_STATE state, uint8_t time = 0, SLEEP_MODE_SAMPLE_TIME sample_time = SLEEP_MODE_SAMPLE_TIME::PER_50_SECS);
		
	};
}