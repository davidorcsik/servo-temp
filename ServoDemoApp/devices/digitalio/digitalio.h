#pragma once

namespace devices
{

class DigitalIO
{
  public:
	DigitalIO(GPIO_TypeDef* port, uint16_t pin);
	~DigitalIO();
	
  public:
	bool getValue();
	void setValue(bool level);

  private:
	GPIO_TypeDef* port;
	uint16_t pin;
};
}