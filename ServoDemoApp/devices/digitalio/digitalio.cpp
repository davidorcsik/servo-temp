#include "digitalio.h"
namespace devices
{
	DigitalIO::~DigitalIO()
	{
	}

	DigitalIO::DigitalIO(GPIO_TypeDef* port, uint16_t pin)
	{
		this->port = port;
		this->pin = pin;
	}
	bool DigitalIO::getValue()
	{
		return (bool )HAL_GPIO_ReadPin(port, pin);
	}
	
	void DigitalIO::setValue(bool level)
	{
		HAL_GPIO_WritePin(port, pin, (GPIO_PinState )level);
	}
}





