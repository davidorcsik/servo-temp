#pragma once

#include <ISerial.h>
#include <utils.h>
#include <SerialBuffer.hpp>

#include <unordered_map>
#include <functional>

namespace devices {
	class GPS {
#define BUFFER_SIZE 100
	public:
		struct GPSData {
			double longitude;
			double latitude;
			double altitude;
			double heading;
			Utils::DateTime date_time;
			uint8_t fix;
			uint8_t satellites;
			uint8_t speed;
		};
		
	private:
		static const std::array<char, 7> COMMAND_DELIMETERS;
		static uint32_t calculate_checksum(const std::string& command_string);
		static bool is_checksum_valid(const std::string& command_string);
		static uint8_t get_command_name(const std::string& command, char* command_name);
		static uint8_t parse_gga(const std::string& command, GPSData& data);
		static uint8_t parse_gsa(const std::string& command, GPSData& data);
		static uint8_t parse_rmc(const std::string& command, GPSData& data);
		static uint8_t parse_vtg(const std::string& command, GPSData& data);
		static uint8_t parse_hchdg(const std::string& command, GPSData& data);
		uint8_t parse(const std::string& command);
		
		static std::unordered_map<std::string, std::function<uint8_t(const std::string&, GPSData&)>> command_parser_map;
		std::string last_command_name = std::string(BUFFER_SIZE, '\0');
		static std::array<char, 2> delimeters;
		
		protocols::ISerial& serialHandle;
		typedef Utils::SerialBuffer<BUFFER_SIZE, Utils::SERIAL_BUFFER_LINE_DELIMETERS::CRLF> SerialBuffer_t;
		std::string readBuffer = std::string(BUFFER_SIZE, '\0');
		SerialBuffer_t serialBuffer;
		GPSData parsedData;
	public:
		GPS(protocols::ISerial& _serialHandle) : serialHandle(_serialHandle), serialBuffer(_serialHandle) {
			uint8_t INIT_FAIL = 0;
			while (INIT_FAIL != 0) {
				volatile uint8_t breakpoint_placeholder = 0;
			}
		}
		virtual ~GPS() {}
		
		uint8_t read(std::string& buffer);
		GPSData read_and_parse();
	};
}