#include <GPS.h>
#include <utils.h>
#include <serial_helper.h>

#include <string>
#include <vector>
#include <algorithm>

namespace devices
{
	std::array<char, 2> GPS::delimeters = { ',', '*' };
	std::unordered_map<std::string, std::function<uint8_t(const std::string&, GPS::GPSData&)>> GPS::command_parser_map = {
		{ "GPGGA", &GPS::parse_gga },
		{ "GPGSA", &GPS::parse_gsa },
		{ "GPRMC", &GPS::parse_rmc },
		{ "GPVTG", &GPS::parse_vtg },
		{ "HCHDG", &GPS::parse_hchdg }
	};
	const std::array<char, 7> GPS::COMMAND_DELIMETERS = { '!', '$', '*', ',', '\\', '^', '~' };
	uint32_t GPS::calculate_checksum(const std::string& command_string) {
		uint32_t checksum = 0;
		for (size_t i = 1; i < command_string.length() && command_string[i] != '*'; ++i) {
			checksum ^= command_string[i];
		}
		return checksum;
	}
	bool GPS::is_checksum_valid(const std::string& command_string) {
		std::string::const_iterator checksum_simbol_pos = std::find(command_string.cbegin(), command_string.cend(), '*');
		uint32_t received_checksum = std::strtoul(&*(checksum_simbol_pos + 1), nullptr, 16);
		return calculate_checksum(command_string) == received_checksum;
	}
	
	uint8_t GPS::get_command_name(const std::string& command, char* command_name) {
		std::string::const_iterator first_delimeter_pos = std::find_first_of(command.cbegin() + 1, command.cend(), GPS::COMMAND_DELIMETERS.cbegin(), GPS::COMMAND_DELIMETERS.cend());
		char* command_name_end_pos = std::copy(command.cbegin(), command.cend(), command_name);
		*(command_name_end_pos + 1) = '\0';
		return 0;
	}
	
	uint8_t GPS::parse_gga(const std::string& command, GPSData& data) {
		const size_t SEGMENT_LENGTH = 16, SEGMENT_COUNT = 16;
		std::array<std::array<char, SEGMENT_LENGTH>, SEGMENT_COUNT> splitted_data;
		int8_t splits = Utils::split_string_no_heap(command, delimeters.data(), 2, splitted_data);
		if (splits == -1) {
			return false;
		}
		
		// time
		Utils::DateTime date_time = Utils::get_current_time(); //RTC should be initialized
		date_time.segments.hour = Utils::atoi_with_bounds(splitted_data[1].data(), 0, 2);
		date_time.segments.minute = Utils::atoi_with_bounds(splitted_data[1].data(), 2, 2);
		date_time.segments.second = Utils::atoi_with_bounds(splitted_data[1].data(), 4);
		data.date_time = date_time;
		
		// latitude
		uint8_t latitude_angle_whole = Utils::atoi_with_bounds(splitted_data[2].data(), 0, 2);
		double latitude_angle_minutes = Utils::atof_with_bounds(splitted_data[2].data(), 2);
		double latitude_polarized = latitude_angle_whole + latitude_angle_minutes / 60;
		char latitude_pole = splitted_data[3][0];
		data.latitude = ((latitude_pole == 'N') ? 1 : -1) * latitude_polarized;
		
		// longitude
		uint8_t longitude_angle_whole = Utils::atoi_with_bounds(splitted_data[4].data(), 0, 3);
		double longitude_angle_minutes = Utils::atof_with_bounds(splitted_data[4].data(), 3);
		double longitude_polarized = longitude_angle_whole + longitude_angle_minutes / 60;
		char longitude_pole = splitted_data[5][0];
		data.longitude = ((longitude_pole == 'E') ? 1 : -1) * longitude_polarized;
		
		// fix
		//uint8_t fix = splitted_data[6][0] - 48;
		//data.fix = fix;
		
		// satelites
		uint8_t satellites = Utils::atoi_with_bounds(splitted_data[7].data());
		data.satellites = satellites;
		
		// horizontal dilution of position
		//double horizontal_dilution = Utils::atof_with_bounds(splitted_data[8].data());
		
		// altitude
		double altitude = Utils::atof_with_bounds(splitted_data[9].data());
		char altitude_unit = splitted_data[10][0];
		data.altitude = ((altitude_unit == 'M') ? 1 : 0.3048) * altitude; // if necessary converting ft to meter
		
		// mean sea level
		//double mean_sea_level = Utils::atof_with_bounds(splitted_data[11].data());
		//char mean_sea_level_unit = splitted_data[12][0];
		
		// time since last DGPS update
		//std::string time_since_last_dgps_update = splitted_data[13]; //empty
		
		// DPGS station id
		//std::string dgpd_station_id = splitted_data[14]; //empty
		
		// checksum
		//std::string checksum =  splitted_data[15]; //checksum is a hex string
		return true;
	}
	uint8_t GPS::parse_gsa(const std::string& command, GPSData& data) {
		const size_t SEGMENT_LENGTH = 8, SEGMENT_COUNT = 19;
		std::array<std::array<char, SEGMENT_LENGTH>, SEGMENT_COUNT> splitted_data;
		int8_t splits = Utils::split_string_no_heap(command, delimeters.data(), 2, splitted_data);
		if (splits == -1) {
			return false;
		}
		
		// auto selection of 2D or 3D fix (A = auto, M = manual)
		//char auto_selection = splitted_data[1][0];
		
		// fix
		uint8_t fix = splitted_data[2][0] - 48;
		data.fix = fix;
		
		// satellite PRNs
		uint8_t satellite_prn_count = splitted_data.size() - 7;
		//std::vector<uint8_t> satellite_prns(satellite_prn_count);
		//for (uint8_t i = 3; i < 3 + satellite_prn_count; ++i) {
		//	satellite_prns[i] = std::atoi(splitted_data[i].c_str());
		//}
		
		// PDOP (dilution of precision) 
		//double dilution_of_precision = Utils::atof_with_bounds(splitted_data[3 + satellite_prn_count].data());
		
		// Horizontal dilution of precision (HDOP)
		//double horizontal_dilution_of_precision = Utils::atof_with_bounds(splitted_data[3 + satellite_prn_count + 1].data());
		
		// Vertical dilution of precision (VDOP)
		//double vertical_dilution_of_precision = Utils::atof_with_bounds(splitted_data[3 + satellite_prn_count + 2].data());
		
		// Checksum
		//std::string checksum = splitted_data[3 + satellite_prn_count + 3];
		return true;
	}
	
	uint8_t GPS::parse_rmc(const std::string& command, GPSData& data) {
		const size_t SEGMENT_LENGTH = 16, SEGMENT_COUNT = 13;
		std::array<std::array<char, SEGMENT_LENGTH>, SEGMENT_COUNT> splitted_data;
		int8_t splits = Utils::split_string_no_heap(command, delimeters.data(), 2, splitted_data);
		if (splits == -1) {
			return false;
		}
		// time
		data.date_time.segments.hour = Utils::atoi_with_bounds(splitted_data[1].data(), 0, 2);
		data.date_time.segments.minute = Utils::atoi_with_bounds(splitted_data[1].data(), 2, 2);
		data.date_time.segments.second = Utils::atoi_with_bounds(splitted_data[1].data(), 4);
		
		// status (A = active, V = void)
		//char status = splitted_data[2][0];
		
		// latitude
		//uint8_t latitude_angle_whole = Utils::atoi_with_bounds(splitted_data[3].data(), 0, 2);
		//double latitude_angle_minutes = Utils::atof_with_bounds(splitted_data[3].data(), 2);
		//double latitude_polarized = latitude_angle_whole + latitude_angle_minutes / 60;
		//char latitude_pole = splitted_data[4][0];
		
		// longitude
		//uint8_t longitude_angle_whole = Utils::atoi_with_bounds(splitted_data[5].data(), 0, 3);
		//double longitude_angle_minutes = Utils::atof_with_bounds(splitted_data[5].data(), 3);
		//double longitude_polarized = longitude_angle_whole + longitude_angle_minutes / 60;
		//char longitude_pole = splitted_data[6][0];
		
		// speed over the ground in knots (not ground speed)
		//double speed = Utils::atoi_with_bounds(splitted_data[7].data());
		
		// track angle
		//double track_angle = Utils::atof_with_bounds(splitted_data[8].data());
		
		// date
		data.date_time.segments.day = Utils::atoi_with_bounds(splitted_data[9].data(), 0, 2);
		data.date_time.segments.month = Utils::atoi_with_bounds(splitted_data[9].data(), 2, 2);
		data.date_time.segments.year = Utils::atoi_with_bounds(splitted_data[9].data(), 4, 2);
		
		// magnetic variation
		//double magnetic_variation = Utils::atoi_with_bounds(splitted_data[10].data());
		//char magnetic_variation_pole = splitted_data[11][0];
		
		// checksum
		//std::string checksum = splitted_data[10];
		return true;
	}
	
	uint8_t GPS::parse_vtg(const std::string& command, GPSData& data) {
		const size_t SEGMENT_LENGTH = 8, SEGMENT_COUNT = 10;
		std::array<std::array<char, SEGMENT_LENGTH>, SEGMENT_COUNT> splitted_data;
		int8_t splits = Utils::split_string_no_heap(command, delimeters.data(), 2, splitted_data);
		if (splits == -1) {
			return false;
		}
		
		// True track made good (degrees)
		//double angle = Utils::atoi_with_bounds(splitted_data[1].data());
		//char track_indicator = splitted_data[2][0];
		
		// Magnetic track made good
		//double magnetic_track = Utils::atof_with_bounds(splitted_data[3].data());
		//char magnetic_track_indicator = splitted_data[4][0];
		
		// Ground speed, knots
		//double ground_speed_knots = Utils::atof_with_bounds(splitted_data[5].data());
		//char nautical_speed_indicator = splitted_data[6][0];
		
		// Ground speed, kilometers per hour
		double ground_speed_kmh = Utils::atof_with_bounds(splitted_data[7].data());
		char speed_indicator = splitted_data[8][0];
		data.speed = static_cast<uint8_t>(ground_speed_kmh); //flooring speed
		
		// checksum
		//std::string checksum = splitted_data[9];
		return true;
	}
	
	uint8_t GPS::parse_hchdg(const std::string& command, GPSData& data) {
		const size_t SEGMENT_LENGTH = 8, SEGMENT_COUNT = 7;
		std::array<std::array<char, SEGMENT_LENGTH>, SEGMENT_COUNT> splitted_data;
		int8_t splits = Utils::split_string_no_heap(command, delimeters.data(), 2, splitted_data);
		if (splits == -1) {
			return false;
		}
		
		// heading
		data.heading = Utils::atof_with_bounds(splitted_data[1].data());
		
		// deviation
		//std::string deviation = splitted_data[2]; // empty
		
		// variation
		//double variation = Utils::atof_with_bounds(splitted_data[3].data());
		//char variation_direction = splitted_data[4][0];
		
		// checksum
		//std::string checksum = splitted_data[5];
		return true;
	}
	
	uint8_t GPS::parse(const std::string& command) {
		char command_name[8];
		get_command_name(command, command_name);
		if (!is_checksum_valid(command) || command_parser_map.find(command_name) == command_parser_map.end()) {
			return false;
		}
		command_parser_map[command_name](command, parsedData);
		return true;
	}
	
	uint8_t GPS::read(std::string& buffer) {
		SerialBuffer_t::STATUS recv_status = serialBuffer.read_line(buffer);
		if (recv_status != SerialBuffer_t::STATUS::OK) {
			return 1;
		}
		return 0;
	}
	
	GPS::GPSData GPS::read_and_parse() {
		read(readBuffer);
		parse(readBuffer);
		return parsedData; // returns last SUCCESSFUL parse
	}
}