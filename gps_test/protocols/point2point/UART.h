#pragma once
#include "ISerial.h"
#include "stm32l4xx_hal.h"
#include <string>

namespace protocols
{
	class UART : public ISerial
	{
	private:
		UART_HandleTypeDef* const uartHandle;
	public:
		UART(UART_HandleTypeDef* const _uartHandle)
		: uartHandle(_uartHandle) { }
		
		virtual uint8_t read(uint8_t* buffer, const size_t& length, const uint32_t& timeout) override;
		
		virtual uint8_t write(const uint8_t* buffer, const size_t& length, const uint32_t& timeout) override;
		
		virtual ~UART() {}
	};
}

