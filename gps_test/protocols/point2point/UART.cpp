#include "UART.h"
#include <vector>
#include <utils.h>

namespace protocols
{
	uint8_t UART::read(uint8_t* buffer, const size_t& length, const uint32_t& timeout)
	{
		const HAL_StatusTypeDef result = HAL_UART_Receive(this->uartHandle, buffer, length, timeout);
		return static_cast<uint8_t>(result);
	}
	
	uint8_t UART::write(const uint8_t* buffer, const size_t& length, const uint32_t& timeout)
	{
		const HAL_StatusTypeDef result = HAL_UART_Transmit(this->uartHandle, const_cast<uint8_t*>(buffer), length, timeout);
		return static_cast<uint8_t>(result);
	}
}

