#include <ISerial.h>

namespace protocols
{
	const size_t ISerial::DEFAULT_READ_LENGTH = 100;
	const uint32_t ISerial::DEFAULT_TIMEOUT = 5000;
}