#pragma once
#include <inttypes.h>
#include <stdlib.h>
#include <string>

namespace protocols
{
	class ISerial
	{
	public:
		static const size_t DEFAULT_READ_LENGTH;
		static const uint32_t DEFAULT_TIMEOUT;
		virtual uint8_t read(uint8_t* buffer, const size_t& length = DEFAULT_READ_LENGTH, const uint32_t& timeout = DEFAULT_TIMEOUT) = 0;
		virtual uint8_t write(const uint8_t* buffer, const size_t& length, const uint32_t& timeout = DEFAULT_TIMEOUT) = 0;
		virtual ~ISerial() {}
	};
}