#pragma once
#include <iterator>

template <typename CONTAINER_TYPE, typename ITEM_TYPE>
class SplitIterator : std::iterator<std::input_iterator_tag, CONTAINER_TYPE> {
private:
	const CONTAINER_TYPE& data;
	const ITEM_TYPE& delimeter;
	
public:
	SplitIterator(const CONTAINER_TYPE& _data, const ITEM_TYPE& _delimeter)
	: data(_data) , delimeter(_delimeter) {}
	virtual ~SplitIterator() {}
	
	
};