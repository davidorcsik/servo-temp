#pragma once
#include <ISerial.h>

#include <string>

namespace SerialHelper {
	std::string read(protocols::ISerial& serial, const size_t& length = protocols::ISerial::DEFAULT_READ_LENGTH, const uint32_t& timeout = protocols::ISerial::DEFAULT_TIMEOUT);
	uint8_t write(protocols::ISerial& serial, const std::string& buffer, const uint32_t& timeout = protocols::ISerial::DEFAULT_TIMEOUT);
}