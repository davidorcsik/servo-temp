#include <serial_helper.h>
#include <ISerial.h>

#include "stm32l4xx_hal.h"

#include <vector>
#include <algorithm>

namespace SerialHelper {
	std::string read(protocols::ISerial& serial, const size_t& length, const uint32_t& timeout) {
		std::vector<uint8_t> local_buffer(length, 0);
		uint8_t read_status = serial.read(local_buffer.data(), length, timeout);
		std::vector<uint8_t>::iterator first_zero_item = std::find_if(local_buffer.begin(), local_buffer.end(), [](uint8_t& c) { return c == 0;});
		//status will be timeout if the length is grater than the actual message length so we check if any value in the buffer is not 0
		if(read_status != HAL_OK && (read_status == HAL_TIMEOUT && first_zero_item == local_buffer.end())) {
			return "";
		}
		
		std::vector<uint8_t>::difference_type received_byte_count = std::distance(local_buffer.begin(), first_zero_item);
		std::string reply(received_byte_count, 0);
		std::vector<uint8_t>::iterator local_buffer_it = local_buffer.begin();
		std::string::iterator reply_it = reply.begin();
		
		for (std::vector<uint8_t>::difference_type i = 0; i < received_byte_count; ++reply_it, ++local_buffer_it, ++i) {
			*reply_it = static_cast<char>(*local_buffer_it);
		}
		return reply;
	}
	uint8_t write(protocols::ISerial& serial, const std::string& buffer, const uint32_t& timeout) {
		return serial.write(const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(buffer.data())), buffer.length(), timeout);
	}
}