#pragma once
#include <array>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <float.h>

namespace Utils
{
#pragma pack(push, 1)
	union DateTime
	{
		struct DateTimeStruct
		{
			// 32
			uint32_t year : 6;      //63
			uint32_t month : 4;      // 12 
			uint32_t day : 5;          //31    
			uint32_t hour : 5;      //23
			uint32_t minute : 6;      //59
			uint32_t second : 6;      //59
		} segments;
		uint32_t value;
	};
#pragma pack(pop)
	
	DateTime get_current_time();
	
	uint8_t byte_array_to_string(const uint8_t* bytes, const size_t& length, char* output, const size_t& output_length);
	constexpr std::array<char, 16> hexchars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	size_t byte_array_to_hex_string(const uint8_t* bytes, const size_t& length, char* output, const size_t& output_length);
	size_t hex_string_to_byte_array(const std::string& hex_string, uint8_t* output, const size_t& output_length);
		
	template <size_t SPLITTED_LENGTH, size_t SPLITS>
	int8_t split_string_no_heap(const std::string& data, char* delimeters, const uint8_t& delimeter_count, std::array<std::array<char, SPLITTED_LENGTH>, SPLITS>& output) {
		size_t splits = 0;
		size_t current_output_char_pos = 0;
		
		for (size_t i = 0; i < data.length(); ++i) {
			size_t j = 0;
			while (j < delimeter_count && delimeters[j] != data[i]) {
				++j;
			}
			if (j == delimeter_count) {
				if (current_output_char_pos >= SPLITTED_LENGTH - 1 || splits >= SPLITS) {
					return -1;
				}
				output[splits][current_output_char_pos] = data[i];
				++current_output_char_pos;
			}
			else {
				output[splits][current_output_char_pos + 1] = '\0';
				++splits;
				current_output_char_pos = 0;
			}
		}
		return splits;
	}
	int32_t atoi_with_bounds(const char* input, const size_t& start_index = 0, size_t length = 0);
	double atof_with_bounds(const char* input, const size_t& start_index = 0, const size_t& length = 0);
}