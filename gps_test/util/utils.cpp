#include <utils.h>

#include <vector>
#include <string>
#include <array>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <float.h>

namespace Utils
{
	DateTime get_current_time() {
		std::time_t time = std::time(0);
		std::tm* sys_time_now = std::localtime(&time);
		DateTime datetime;
		datetime.segments.year = sys_time_now->tm_year;
		datetime.segments.month = sys_time_now->tm_mon;
		datetime.segments.day = sys_time_now->tm_mday;
		datetime.segments.hour = sys_time_now->tm_hour;
		datetime.segments.minute = sys_time_now->tm_min;
		datetime.segments.second = sys_time_now->tm_sec;
		return datetime;
	}
	
	uint8_t byte_array_to_string(const uint8_t* bytes, const size_t& length, char* output, const size_t& output_length) {
		size_t i = 0;
		for ( ; i < length && i < output_length - 1; ++i) {
			output[i] = static_cast<char>(bytes[i]);
		}
		output[i + 1] = '\0';
		return 0;
	}
	
	size_t byte_array_to_hex_string(const uint8_t* bytes, const size_t& length, char* output, const size_t& output_length) {
		size_t i = 0;
		for ( ; i < length && i < output_length - 1; ++i) {
			output[i * 2] = hexchars[(bytes[i] & 0xf0) >> 4];
			output[i * 2 + 1] = hexchars[(bytes[i] & 0x0f)];
		}
		std::find(output + length * 2, output + output_length - 1, '0');
		output[i + 1] = '\0';
		return i;
	}
	
	size_t hex_string_to_byte_array(const std::string& hex_string, uint8_t* output, const size_t& output_length) {
		auto hex_to_dec = [](const char& c) { return std::distance(hexchars.begin(), std::find(hexchars.begin(), hexchars.end(), c)); };
		size_t i = 0;
		for ( ; i < hex_string.length() / 2 && i < output_length; ++i) {
			output[i] = hex_to_dec(hex_string[i * 2]) << 4 | hex_to_dec(hex_string[i * 2 + 1]);
		}
		return i;
	}
	int32_t atoi_with_bounds(const char* input, const size_t& start_index, size_t length) {
		if (length == 0) {
			length = std::strlen(input);
		}
		char split[10]; //max uint32_t length + null byte
		for (size_t i = 0; i < length; ++i) {
			split[i] = input[i + start_index];
		}
		split[length] = '\0';
		return std::atoi(split);
	}
	double atof_with_bounds(const char* input, const size_t& start_index, const size_t& length) {
		char split[DBL_MAX_10_EXP + 2 + 1]; //max double length + null byte (<=311)
		for (size_t i = 0; i < length; ++i) {
			split[i] = input[i + start_index];
		}
		split[length] = '\0';
		return std::atof(split);
	}
}