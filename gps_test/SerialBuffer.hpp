#pragma once
#include <ISerial.h>
#include <StaticQueue.hpp>

#include "stm32l4xx_hal.h"

#include <cstring>

namespace Utils
{
	enum class SERIAL_BUFFER_LINE_DELIMETERS { LF = 0, CRLF };
	template  <size_t BUFFER_SIZE, SERIAL_BUFFER_LINE_DELIMETERS LINE_DELIMETER>
	class SerialBuffer {
	public:
		enum class STATUS { OK, ERROR, TIMEOUT };
		private:
		const char* line_delimeter;
		size_t line_delimeter_length; 
		StaticQueue<char, BUFFER_SIZE> buffer;
		protocols::ISerial& serialHandle;
	public:
		SerialBuffer(protocols::ISerial& _serialHandle) : serialHandle(_serialHandle) {
			if (LINE_DELIMETER == SERIAL_BUFFER_LINE_DELIMETERS::LF) {
				line_delimeter = "\n";
			} else if (LINE_DELIMETER == SERIAL_BUFFER_LINE_DELIMETERS::CRLF) {
				line_delimeter = "\r\n";
			}
			line_delimeter_length = strlen(line_delimeter);
		}
		STATUS read(char& output) {
			uint8_t recv;
			uint8_t read_status = serialHandle.read(&recv, 1, 1000);
			if (read_status == HAL_ERROR || read_status == HAL_BUSY) return STATUS::ERROR;
			
			if (buffer.empty()) {
				if (read_status != HAL_OK) return STATUS::TIMEOUT;
				output = static_cast<char>(recv);
			} else {
				buffer.pop(output);
				buffer.push(recv);
			}
			return STATUS::OK;
		}
		STATUS read_line(std::string& output) {
			char local_buffer[BUFFER_SIZE];
			size_t pos = 0;
			STATUS status = STATUS::OK;
			while (pos < line_delimeter_length && status == STATUS::OK) {
				status = read(local_buffer[pos]);
				++pos;
			}
			
			auto found_new_line_at_end = [&]() {
				size_t i = 0;
				while (i < pos && local_buffer[pos - line_delimeter_length + i] == line_delimeter[i]) {
					++i;
				}
				return i == pos;
			};
			
			while (pos < BUFFER_SIZE && status == STATUS::OK && !found_new_line_at_end()) {
				status = read(local_buffer[pos]);
				++pos;
			}
			output = std::string(local_buffer, pos);
			return status;
		}
		virtual ~SerialBuffer() {}
	};
}