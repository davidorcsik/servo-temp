#pragma once
#include <ISerial.h>
#include <SerialMessage.h>
#include "stm32l4xx_hal.h"

namespace protocols
{
	class UART : public ISerial
	{
	private:
		UART_HandleTypeDef* const uart_handle;
		const bool is_interrupt_required;
	protected:
		bool is_message_type_valid(const SerialMessage& message) const override;
	public:
		UART(UART_HandleTypeDef* const _uartHandle, bool _is_interrupt_required)
		: uart_handle(_uartHandle), is_interrupt_required(_is_interrupt_required) { }
		
		virtual STATUS read(SerialMessage& message) override;
		
		virtual STATUS write(const SerialMessage& message) override;
		
		virtual ~UART() {}
		
		virtual std::optional<SerialMessage> create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS, const uint8_t _register = ISerial::DEFAULT_REGISTER) override;
	};
}

