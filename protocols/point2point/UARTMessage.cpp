#include <UARTMessage.h>

#include <cstring>
#include <inttypes.h>

namespace protocols {
		
	std::optional<SerialMessage> UARTMessage::create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout) {
		STATUS status = SerialMessage::is_contructor_parameters_valid(_payload, _payload_length, _timeout);
		if (status != STATUS::OK) {
			return {};
		}
		UARTMessage message(_payload, _payload_length, _timeout);
		return { message };
	}
}