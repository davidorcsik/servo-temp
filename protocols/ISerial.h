#pragma once
#include <inttypes.h>
#include <stdlib.h>
#include <string>
#include <optional>

#include <SerialMessage.h>

#include <stm32l4xx_hal.h>

namespace protocols
{
	class ISerial
	{
	public:
		enum class STATUS {
			OK =						HAL_OK,
			HARDWARE_ERROR =			HAL_ERROR,
			HARDWARE_BUSY =				HAL_BUSY,
			HARDWARE_TIMEOUT =			HAL_TIMEOUT,
			INVALID_MESSAGE_TYPE, 
			INVALID_MESSAGE_LENGTH, 
			COULDNT_LOCK_HARDWARE,
		};
	protected:
		const static size_t BUFFER_SIZE = 1024;
		uint8_t buffer[BUFFER_SIZE] = { 0 };
		virtual bool is_message_type_valid(const SerialMessage& message) const;
	public:
		ISerial() {}
		static const size_t DEFAULT_READ_LENGTH;
		static const uint32_t DEFAULT_TIMEOUT;
		static const uint32_t DEFAULT_ADDRESS;
		static const uint8_t DEFAULT_REGISTER;
		virtual STATUS read(SerialMessage& message) = 0;
		virtual STATUS write(const SerialMessage& message) = 0;
		virtual ~ISerial() {}
		
		virtual std::optional<SerialMessage> create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = DEFAULT_TIMEOUT, const uint32_t _address = DEFAULT_ADDRESS, const uint8_t _register = DEFAULT_REGISTER) = 0;
		virtual std::optional<SerialMessage> create_message(const char* _payload, const uint32_t _timeout = DEFAULT_TIMEOUT, const uint32_t _address = DEFAULT_ADDRESS, const uint8_t _register = DEFAULT_REGISTER);
	};
}