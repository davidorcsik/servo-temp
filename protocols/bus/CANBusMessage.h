#pragma once

#include <SerialMessage.h>
#include <ISerial.h>

namespace protocols {
	class CANBusMessage : public SerialMessage {
	protected:
		CANBusMessage() : SerialMessage(nullptr, 0, 0, 0, 0, class_id) {}
		CANBusMessage(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address)
		: SerialMessage(_payload, _payload_length, _timeout, _address, 0, class_id) {}
	public:
		virtual ~CANBusMessage() {}
		static std::optional<SerialMessage> create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS);
		static const uint8_t class_id = 3;
	};
}