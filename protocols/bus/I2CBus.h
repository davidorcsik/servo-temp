#pragma once
#include <ISerial.h>

#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <semphr.h>

namespace protocols {
	class I2CBus : public ISerial {
	private:
		I2C_HandleTypeDef* const i2c_handle;
		SemaphoreHandle_t i2c_handle_mutex;
	protected:
		bool is_message_type_valid(const SerialMessage& message) const override;
	public:
		I2CBus() : i2c_handle(nullptr) {}
		I2CBus(I2C_HandleTypeDef* const _i2cHandle)
		: i2c_handle(_i2cHandle) {
			i2c_handle_mutex = xSemaphoreCreateRecursiveMutex();
		}
		
		virtual ~I2CBus() {}
		
		virtual STATUS read(SerialMessage& message) override;
		
		virtual STATUS write(const SerialMessage& message) override;
		
		virtual std::optional<SerialMessage> create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout = ISerial::DEFAULT_TIMEOUT, const uint32_t _address = ISerial::DEFAULT_ADDRESS, const uint8_t _register = ISerial::DEFAULT_REGISTER) override;
	};
}