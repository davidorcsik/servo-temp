#include <CANBusMessage.h>
#include <cstring>

namespace protocols {	
	std::optional<SerialMessage> CANBusMessage::create(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address) {
		STATUS status;
		if (_payload_length > 7) {
			return {};
		}
		
		status = SerialMessage::is_contructor_parameters_valid(_payload, _payload_length, _timeout);
		if (status != STATUS::OK) {
			return {};
		}
		
		CANBusMessage message(_payload, _payload_length, _timeout, _address);
		return { message };
	}
}