
#include <I2CBus.h>
#include <I2CMessage.h>

#include <portmacro.h>
namespace protocols
{
	I2CBus::STATUS I2CBus::write(const SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return ISerial::STATUS::INVALID_MESSAGE_TYPE;
		}
		
		if (xSemaphoreTakeRecursive(i2c_handle_mutex, (TickType_t) message.get_timeout()) == pdTRUE) {
			HAL_StatusTypeDef status = HAL_I2C_Mem_Write(i2c_handle, message.get_device_address(), message.get_register_address(), I2C_MEMADD_SIZE_8BIT, const_cast<uint8_t*>(message.get_payload()), message.get_payload_length(), message.get_timeout());
			xSemaphoreGiveRecursive(i2c_handle_mutex);
			return static_cast<STATUS>(status);
		}
		return STATUS::COULDNT_LOCK_HARDWARE;
	}
	I2CBus::STATUS I2CBus::read(SerialMessage& message) {
		if (!is_message_type_valid(message)) {
			return ISerial::STATUS::INVALID_MESSAGE_TYPE;
		}
		if (message.get_payload_length() > BUFFER_SIZE) {
			return ISerial::STATUS::INVALID_MESSAGE_LENGTH;
		}
		
		if (xSemaphoreTakeRecursive(i2c_handle_mutex, (TickType_t) message.get_timeout()) == pdTRUE) {
			HAL_StatusTypeDef status = HAL_I2C_Mem_Read(i2c_handle, message.get_device_address(), message.get_register_address(), I2C_MEMADD_SIZE_8BIT, buffer, message.get_payload_length(), message.get_timeout());
			xSemaphoreGiveRecursive(i2c_handle_mutex);
			message.set_payload(buffer, message.get_payload_length());
			return static_cast<STATUS>(status);
		}
		return STATUS::COULDNT_LOCK_HARDWARE;
	}
	
	bool I2CBus::is_message_type_valid(const SerialMessage& message) const {
		return message.get_class_id() == I2CMessage::class_id;
	}
	
	std::optional<SerialMessage> I2CBus::create_message(const uint8_t* _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _address, const uint8_t _register) {
		return I2CMessage::create(_payload, _payload_length, _timeout, _address, _register);
	}
}