#pragma once
#include <inttypes.h>
#include <algorithm>

namespace protocols {
	class SerialMessage {
	public:
		static const size_t MAX_PAYLOAD_LENGTH = 1024;
		enum class STATUS {
			OK = 0, INVALID_PAYLOAD_LENGTH, INVALID_PAYLOAD, INVALID_TIMEOUT, INVALID_ADDRESS
		};
	private:
		uint32_t payload_length;
		uint32_t timeout;
		uint32_t device_address;
		uint8_t register_address;
		uint8_t payload[MAX_PAYLOAD_LENGTH];
		uint8_t class_id;
	protected:
		SerialMessage(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout, const uint32_t _device_address, const uint8_t _register_address, const uint8_t _class_id)
		: payload_length(_payload_length), timeout(_timeout), device_address(_device_address), register_address(_register_address), class_id(_class_id) {
			std::copy_n(_payload, _payload_length, payload);
		}
		static STATUS is_contructor_parameters_valid(const uint8_t* const _payload, const uint32_t _payload_length, const uint32_t _timeout);
	public:
		SerialMessage() {}
		virtual ~SerialMessage() {}
		virtual const uint8_t* get_payload() const;
		virtual uint32_t get_payload_length() const;
		virtual uint32_t get_timeout() const;
		virtual STATUS set_timeout(uint32_t _timeout);
		virtual uint8_t set_payload(const uint8_t* _payload, const uint32_t _payload_length);
		virtual uint8_t set_payload(const char* _payload);
		virtual STATUS set_payload_length(const uint32_t _payload_length);
		virtual uint8_t get_class_id() const;
		virtual uint32_t get_device_address() const;
		virtual STATUS set_device_address(uint32_t _device_address);
		virtual uint8_t get_register_address() const;
		virtual STATUS set_register_address(uint8_t _register_address);
	};
}