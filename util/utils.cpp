#include <utils.h>

#include <vector>
#include <string>
#include <array>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <float.h>

#include <stm32l4xx_hal_flash.h>
Utils::DateTime Utils::get_current_time() {
	std::time_t time = std::time(0);
	std::tm* sys_time_now = std::localtime(&time);
	DateTime datetime;
	datetime.segments.year = sys_time_now->tm_year;
	datetime.segments.month = sys_time_now->tm_mon;
	datetime.segments.day = sys_time_now->tm_mday;
	datetime.segments.hour = sys_time_now->tm_hour;
	datetime.segments.minute = sys_time_now->tm_min;
	datetime.segments.second = sys_time_now->tm_sec;
	return datetime;
}
std::array<char, 16> Utils::hexchars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
uint8_t Utils::byte_array_to_string(const uint8_t* bytes, const size_t& length, char* output, const size_t& output_length) {
	size_t i = 0;
	for ( ; i < length && i < output_length - 1; ++i) {
		output[i] = static_cast<char>(bytes[i]);
	}
	output[i + 1] = '\0';
	return 0;
}

size_t Utils::byte_array_to_hex_string(const char* bytes, const size_t length, char* output_buffer, const size_t output_buffer_length) {
	return byte_array_to_hex_string(reinterpret_cast<const uint8_t*>(bytes), length, reinterpret_cast<uint8_t*>(output_buffer), output_buffer_length);
}
size_t Utils::byte_array_to_hex_string(const uint8_t* bytes, const size_t length, uint8_t* output_buffer, const size_t output_buffer_length) {
	size_t i = 0;
	for ( ; i < length && i < output_buffer_length; ++i) {
		output_buffer[i * 2] = hexchars[(bytes[i] & 0xf0) >> 4];
		output_buffer[i * 2 + 1] = hexchars[(bytes[i] & 0x0f)];
	}
	return i * 2 + 1;
}

size_t Utils::hex_string_to_byte_array(const char* hex_string, uint8_t* output, const size_t& output_length) {
	auto hex_to_dec = [](const char& c) { return std::distance(hexchars.begin(), std::find(hexchars.begin(), hexchars.end(), c)); };
	size_t i = 0;
	for ( ; i < strlen(hex_string) / 2 && i < output_length; ++i) {
		output[i] = hex_to_dec(hex_string[i * 2]) << 4 | hex_to_dec(hex_string[i * 2 + 1]);
	}
	return i;
}
int32_t Utils::atoi_with_bounds(const char* input, const size_t& start_index, size_t length) {
	if (length == 0) {
		length = std::strlen(input);
	}
	char split[10]; //max uint32_t length + null byte
	for (size_t i = 0; i < length; ++i) {
		split[i] = input[i + start_index];
	}
	split[length] = '\0';
	return std::atoi(split);
}
double Utils::atof_with_bounds(const char* input, const size_t& start_index, const size_t& length) {
	char split[DBL_MAX_10_EXP + 2 + 1]; //max double length + null byte (<=311)
	for (size_t i = 0; i < length; ++i) {
		split[i] = input[i + start_index];
	}
	split[length] = '\0';
	return std::atof(split);
}
size_t Utils::cstring_contains(const char* input, const size_t input_length, const char* value, const size_t value_length) {
	if (input_length < value_length) {
		return -1;
	}
	size_t i = 0;
	bool found = false;
	while (i <= input_length - value_length && !found) {
		size_t j = 0;
		while (j < value_length && input[i + j] == value[j]) {
			++j;
		}
		found |= j == value_length;
		++i;
	}
	return (found) ? i - 1 : -1;
}
uint8_t Utils::convert_uchararr_to_chararr(const uint8_t* source, const size_t& source_length, char* destination) {
	for (size_t i = 0; i < source_length; ++i) {
		destination[i] = static_cast<int8_t>(source[i]);
	}
}

void Utils::ERROR_GUARD() {
	while (true) {
		volatile uint8_t ROSES_ARE_RED_VIOLETS_ARE_BLUE_SO_BAD_THIS_IS_HAPPENING_TO_YOU = 0;
	}
}

bool Utils::in_bounds_unsigned(uint32_t low_bound, uint32_t high_bound, uint32_t value) {
	return low_bound <= value && value <= high_bound;
}
bool Utils::in_bounds_signed(int32_t low_bound, int32_t high_bound, int32_t value) {
	return low_bound <= value && value <= high_bound;
}
Utils::STORAGE_DATA_STRUCT STORAGE_DATA;
bool Utils::storage_write_data() {
	if (STORAGE_MAX_SIZE < sizeof(STORAGE_DATA_STRUCT)) {
		return false;
	}
	
	uint16_t dword_count = sizeof(STORAGE_DATA_STRUCT) / 8;
	const uint64_t* data_struct_start = reinterpret_cast<uint64_t*>(&STORAGE_DATA);
	
	HAL_StatusTypeDef status;
	status = HAL_FLASH_Unlock();
	if (status != HAL_OK) {
		return false;
	}
	
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);
	
	FLASH_EraseInitTypeDef eraseStruct;
	eraseStruct.Banks = FLASH_BANK_2;
	eraseStruct.TypeErase = TYPEERASE_PAGES;
	eraseStruct.NbPages = 1;
	eraseStruct.Page = 255;
	
	uint32_t asd = 0;
	status = HAL_FLASHEx_Erase(&eraseStruct, &asd);
	if (status != HAL_OK) {
		HAL_FLASH_Lock();
		return false;
	}
	
	for (uint16_t i = 0; i < dword_count; ++i) {
		const uint64_t* current_dword = data_struct_start + i;
		status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, STORAGE_START_MEMORY_ADDRESS + (i * 8), *current_dword);
		if (status != HAL_OK) {
			HAL_FLASH_Lock();
			return false;
		}
	}
	
	uint8_t remaining_byte_count = sizeof(STORAGE_DATA_STRUCT) - dword_count * 8;
	if (remaining_byte_count == 0) {
		return true;
	}
	
	uint8_t remaining_bytes_buffer[8] = { 0 };
	const uint8_t* remaining_bytes_start = reinterpret_cast<const uint8_t*>(data_struct_start + dword_count);
	std::copy(remaining_bytes_start, remaining_bytes_start + remaining_byte_count, remaining_bytes_buffer);
	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, STORAGE_START_MEMORY_ADDRESS + dword_count * 8, *reinterpret_cast<const uint64_t*>(remaining_bytes_buffer));
	if (status != HAL_OK) {
		HAL_FLASH_Lock();
		return false;
	}
	
	uint16_t trailing_null_padding_size = STORAGE_MAX_SIZE - (dword_count * 8) - remaining_byte_count;
	uint32_t trailing_null_padding_start_address = STORAGE_START_MEMORY_ADDRESS + STORAGE_MAX_SIZE;
	
	for (uint16_t i = 0; i < trailing_null_padding_size; ++i) {
		status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, STORAGE_START_MEMORY_ADDRESS + trailing_null_padding_start_address, NULL);
		if (status != HAL_OK) {
			HAL_FLASH_Lock();
			return false;
		}
	}
	
	status = HAL_FLASH_Lock();
	if (status != HAL_OK) {
		return false;
	}
	
	return true;
}
bool Utils::storage_read_data() {
	if (STORAGE_MAX_SIZE < sizeof(STORAGE_DATA_STRUCT)) {
		return false;
	}
	
	void* storage_start = reinterpret_cast<void*>(STORAGE_START_MEMORY_ADDRESS);
	
	memcpy(reinterpret_cast<void*>(&STORAGE_DATA), storage_start, sizeof(STORAGE_DATA_STRUCT));
	return true;
}

void Utils::system_restart() {
	NVIC_SystemReset();
}

uint32_t OPERATION_MODE::OFF_LoRa_SEND_FREQ = 600000;
GPIO_PinState OPERATION_MODE::OFF_DIGIO_STATE = GPIO_PinState::GPIO_PIN_RESET;
uint32_t OPERATION_MODE::ON_LoRa_SEND_FREQ = 60000;
GPIO_PinState OPERATION_MODE::ON_DIGIO_STATE = GPIO_PinState::GPIO_PIN_SET;
uint32_t OPERATION_MODE::SERVICE_LoRa_SEND_FREQ = 600000;
GPIO_PinState OPERATION_MODE::SERVICE_DIGIO_STATE = GPIO_PinState::GPIO_PIN_RESET;
uint32_t OPERATION_MODE::get_lora_send_freq(MODES mode) {
	switch (mode) {
	case MODES::OFF:
		return OFF_LoRa_SEND_FREQ;
	case MODES::ON:
		return ON_LoRa_SEND_FREQ;
	case MODES::SERVICE:
		return SERVICE_LoRa_SEND_FREQ;
	default:
		return 0;
	}
}
GPIO_PinState OPERATION_MODE::get_digio_state(MODES mode) {
	switch (mode)
	{
	case MODES::OFF:
		return OFF_DIGIO_STATE;
	case MODES::ON:
		return ON_DIGIO_STATE;
	case MODES::SERVICE:
		return SERVICE_DIGIO_STATE;
	default:
		return GPIO_PinState::GPIO_PIN_RESET;
	}
}