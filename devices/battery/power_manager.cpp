#include "power_manager.h"

#include <cmath>
#include <numeric>

extern ADC_HandleTypeDef hadc1;

namespace devices
{
	void PowerManager::init() {
		HAL_GPIO_WritePin(main_battery_adc_en_port, main_battery_adc_en_pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(backup_battery_adc_en_port, backup_battery_adc_en_pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(analog_adc_en_port, analog_adc_en_pin, GPIO_PIN_SET);
	}
	void PowerManager::measure()
	{
		for (uint8_t i = 0; i < ADC_SETTLE_COUNT; ++i)
		{
			HAL_ADC_Start(&hadc1);
			if (HAL_ADC_PollForConversion(&hadc1, 500) == HAL_OK)
			{
				main_battery_raw_value = (main_battery_raw_value * 15 + HAL_ADC_GetValue(&hadc1) + 80) / 16.;
			}
			if (HAL_ADC_PollForConversion(&hadc1, 500) == HAL_OK)
			{
				backup_battery_raw_value = HAL_ADC_GetValue(&hadc1);
			}
			if (HAL_ADC_PollForConversion(&hadc1, 500) == HAL_OK)
			{
				analog_raw_value = (analog_raw_value * 15 + HAL_ADC_GetValue(&hadc1)) / 16.;
			}
			HAL_ADC_Stop(&hadc1);
		}
	}
	float inline PowerManager::convert_raw_value_to_voltage(uint32_t raw_value, float division_ratio, float raw_correction) const {
		return raw_value * raw_correction * (adc_reference_voltage / pow(2, adc_resolution)) / division_ratio;
	}
	float PowerManager::get_main_battery_voltage_in_volts() {
		measure();
		return convert_raw_value_to_voltage(main_battery_raw_value, main_battery_division_ratio, MAIN_BATTERY_RAW_VALUE_CORRECTION);
	}
	uint16_t PowerManager::get_main_battery_voltage_in_milivolts() {
		return get_main_battery_voltage_in_volts() * 1000;
	}
	
	float PowerManager::get_backup_battery_voltage_in_volts() {
		measure();
		return convert_raw_value_to_voltage(backup_battery_raw_value, backup_battery_division_ratio, 1);
	}
	
	uint16_t PowerManager::get_backup_battery_voltage_in_milivolts() {
		return get_backup_battery_voltage_in_volts() * 1000;
	}
	uint8_t PowerManager::get_main_battery_percentage() {
		return get_main_battery_voltage_in_volts() / main_battery_max_voltage * 100;
	}
	uint8_t PowerManager::get_backup_battery_percentage() {
		return get_backup_battery_voltage_in_volts() / backup_battery_max_voltage * 100;
	}

	float PowerManager::get_analog_voltage_in_volts()
	{
		measure();
		return convert_raw_value_to_voltage(analog_raw_value, analog_division_ratio, ANALOG_RAW_VALUE_CORRECTION);
	}

	uint16_t PowerManager::get_analog_voltage_in_milivolts()
	{
		return get_analog_voltage_in_volts() * 1000;
	}
}