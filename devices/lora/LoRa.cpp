#include <LoRa.h>

#include <algorithm>
#include <inttypes.h>

namespace devices {
	const std::pair<uint16_t, uint16_t> LoRa::duty_cycle_bounds = { 0, 65535 };
	const std::pair<uint16_t, uint16_t> LoRa::channel_id_bounds = { 0, 65535 };
	const std::pair<uint8_t, uint8_t> LoRa::datarate_bounds = { 0, 7 };
	const char* LoRa::OK_REPLY = "ok";
	const char* LoRa::SUCCESSFULLY_CONNECTED_REPLY = "ok\r\naccepted";
	const char* LoRa::SUCCESSFULLY_SENT_REPLY = "ok\r\nmac_tx_ok";
	const char* LoRa::RX_MESSAGE_PREFIX = "mac_rx";

	bool LoRa::is_channel_id_valid(uint8_t channel) {
		return Utils::in_bounds_unsigned(channel_id_bounds.first, channel_id_bounds.second, channel);
	}
	LoRa::STATUS LoRa::init(uint16_t duty_cycle, uint8_t channel_count) {
		//enable lora chip
		HAL_GPIO_WritePin(gpio_enable_port, gpio_enable_pin, GPIO_PIN_SET);
		//duty_cycle is always valid because all u16bit values are valid
		STATUS status;
		sync_uart();
		status = reset();
		sync_uart();
		if (status != STATUS::OK) {
			return status;
		}
		for (uint8_t i = 3; i < std::max(static_cast<uint8_t>(3), channel_count); ++i) {
			status = set_channel_frequency(i, channel_frequency_start + channel_frequency_step*(i - 3));
			if (status != STATUS::OK) {
				return status;
			}
		}
		for (uint8_t i = 0; i < channel_count; ++i) {
			status = set_duty_cycle(i, duty_cycle);
			if (status != STATUS::OK) {
				return status;
			}
		}
		for (uint8_t i = 0; i < channel_count; ++i) {
			status = set_datarate_range(i, datarate_bounds.first, datarate_bounds.second);
			if (status != STATUS::OK) {
				return status;
			}
		}
		for (uint8_t i = 0; i < channel_count; ++i) {
			status = set_channel_status(i, true);
			if (status != STATUS::OK) {
				return status;
			}
		}
		return STATUS::OK;
	}
	LoRa::STATUS LoRa::set_channel_frequency(uint8_t channel, uint32_t frequency) {
		if (!is_channel_id_valid(channel)) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		std::sprintf(buffer, "mac set ch freq %d %lu\r\n", channel, frequency);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	LoRa::STATUS LoRa::connect(LoRa::CONNECTION_MODE mode) {
		std::sprintf(buffer, "mac join ");
		if (mode == CONNECTION_MODE::ACTIVATION_BY_PERSONALIZATION) {
			std::strcat(buffer, "abp");
		} else {
			std::strcat(buffer, "otaa");
		}
		std::strcat(buffer, "\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH, SUCCESSFULLY_CONNECTED_REPLY, 500, false);
		return status;
	}
	LoRa::STATUS LoRa::send_message(char* buffer, size_t buffer_length, const char* expected_reply, uint32_t timeout, bool expect_rx) {
		std::optional<RX_MESSAGE> received_data;
		if (expect_rx) {
			received_data = try_receive(100);
		}
		
		serialMessage.set_payload(buffer);
		serialMessage.set_timeout(timeout);
		
		protocols::ISerial::STATUS status = serialHandle.write(serialMessage);
		if (status != protocols::ISerial::STATUS::OK) {
			last_error = static_cast<STATUS>(status);
			return last_error;
		}
		
		serialMessage.set_payload_length(buffer_length);
		status = serialHandle.read(serialMessage);
		if (status != protocols::ISerial::STATUS::OK && status != protocols::ISerial::STATUS::HARDWARE_TIMEOUT) {
			last_error = static_cast<STATUS>(status);
			return last_error;
		}
		std::strcpy(buffer, reinterpret_cast<const char*>(serialMessage.get_payload()));
		
		STATUS success_status = devices::LoRa::STATUS::OK;
		
		size_t rx_message_pos = Utils::cstring_contains(buffer, strlen(buffer), RX_MESSAGE_PREFIX, strlen(RX_MESSAGE_PREFIX));
		if (rx_message_pos != -1) {
			char* rx_message_first_char_pos = buffer + rx_message_pos + 1;
			size_t rx_message_length = Utils::cstring_contains(rx_message_first_char_pos, strlen(rx_message_first_char_pos), "\r\n", 2) + 2; //+2 because of crlf
			parse_rx_message(rx_message_first_char_pos, rx_message_length);
			std::rotate(rx_message_first_char_pos, rx_message_first_char_pos + rx_message_length + 1, buffer + strlen(buffer));
			success_status = STATUS::RX_MESSAGE_RECEIVED;
			expected_reply = OK_REPLY;
		}
		if (received_data.has_value()) {
			success_status = STATUS::RX_MESSAGE_RECEIVED;
		}
		
		uint8_t buffer_offset = (expected_reply != nullptr) ? strlen(expected_reply) - 1 : 0;
		size_t crlf_pos = Utils::cstring_contains(buffer + buffer_offset, strlen(buffer), "\r\n", 2) + buffer_offset;
		if (crlf_pos != -1) {
			buffer[crlf_pos] = '\0';
		}
		
		if (expected_reply != nullptr && success_status != STATUS::RX_MESSAGE_RECEIVED) {
			return check_reply(buffer, expected_reply);
		}
		return success_status;
	}
	LoRa::STATUS LoRa::check_reply(const char* reply, const char* expected_reply) {
		if (strcmp(reply, expected_reply) != 0) {
			last_error = STATUS::COMMAND_ERROR;
			return last_error;
		}
		return STATUS::OK;
	}
	void LoRa::parse_rx_message(const char* rx_message, size_t rx_message_length) {
		char rx_message_buffer[128];
		std::copy_n(rx_message, rx_message_length, rx_message_buffer);
		char* token = std::strtok(rx_message_buffer, " ");
		
		token = std::strtok(nullptr, " "); // skip prefix
		uint8_t port = std::atoi(token);
		token = std::strtok(nullptr, "\r");
		last_rx_message = RX_MESSAGE(port, token, strlen(token));
	}
	LoRa::STATUS LoRa::reset() {
		std::sprintf(buffer, "sys reset\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH, nullptr, NORMAL_TIMEOUT, false);
		serialMessage.set_payload_length(34);
		serialMessage.set_timeout(1000);
		serialHandle.read(serialMessage);
		return status;
	}
	
	LoRa::STATUS LoRa::get_hweui(char* reply, size_t reply_length) {
		std::fill_n(buffer, BUFFER_LENGTH, '\0');
		std::sprintf(buffer, "sys get hweui\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return status;
		}
		const size_t chars_to_copy = std::min(strlen(buffer), reply_length - 1); //leaving place for null terminator
		std::copy_n(buffer, chars_to_copy, reply);
		reply[chars_to_copy] = '\0';
		return STATUS::OK;
	}
	std::optional<LoRa::DEVICE_STATUS> LoRa::get_device_status() {
		std::sprintf(buffer, "mac get status\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		DEVICE_STATUS device_status;
		Utils::hex_string_to_byte_array(buffer, reinterpret_cast<uint8_t*>(&device_status.value), 4);
		return device_status;
	}
	LoRa::STATUS LoRa::set_datarate(uint8_t datarate) {
		if (!Utils::in_bounds_unsigned(datarate_bounds.first, datarate_bounds.second, datarate)) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		std::sprintf(buffer, "mac set dr %d\r\n", datarate);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY);
	}
	LoRa::STATUS LoRa::set_datarate_range(uint8_t channel, uint8_t min_datarate, uint8_t max_datarate) {
		std::sprintf(buffer, "mac set ch drrange %u %u %u\r\n", channel, min_datarate, max_datarate);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	std::optional<uint8_t> LoRa::get_datarate() {
		std::sprintf(buffer, "mac get dr\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		const uint8_t datarate = std::atoi(buffer);
		if (datarate == 0 && std::strcmp(buffer, "0") != 0) {
			last_error = STATUS::COMMAND_ERROR;
			return { };
		}
		return datarate;
	}
	LoRa::STATUS LoRa::send_data(LoRa::TRANSMIT_TYPE type, uint8_t port, const uint8_t* data, size_t data_length, uint8_t datarate) {
		char buffer[SEND_DATA_PAYLOAD_MAX_LENGTH];
		std::copy_n(data, data_length, buffer);
		buffer[data_length] = '\0';
		return send_data(type, port, buffer, data_length, datarate);
	}
	LoRa::STATUS LoRa::send_data(LoRa::TRANSMIT_TYPE type, uint8_t port, const char* data, size_t data_length, uint8_t datarate) {
		if (!Utils::in_bounds_unsigned(1, 223, port)) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		if (!Utils::in_bounds_unsigned(1, SEND_DATA_PAYLOAD_MAX_LENGTH, data_length)) {
			last_error = STATUS::INVALID_DATA_LENGTH;
			return last_error;
		}
		STATUS status;
		if (datarate != 0) {
			status = set_datarate(datarate);
			if (status != STATUS::OK) {
				return status;
			}
		}
		const char* type_str = (type == TRANSMIT_TYPE::CONFIRMED) ? "cnf" : "uncnf";
		
		char data_hex_buffer[SEND_DATA_PAYLOAD_MAX_LENGTH] = { 0 };
		Utils::byte_array_to_hex_string(data, data_length, data_hex_buffer, SEND_DATA_PAYLOAD_MAX_LENGTH);
		std::sprintf(buffer, "mac tx %s %u %s\r\n", type_str, port, data_hex_buffer);
		status = send_message(buffer, BUFFER_LENGTH, SUCCESSFULLY_SENT_REPLY, 5000);
		return status;
	}
	LoRa::STATUS LoRa::send_adenuis(const char* adenuis_buffer) {
		std::sprintf(buffer, "mac tx cnf 1 %s\r\n", adenuis_buffer);
		return send_message(buffer, BUFFER_LENGTH, SUCCESSFULLY_SENT_REPLY, 5000);
	}
	LoRa::STATUS LoRa::set_adaptive_datarate(bool enabled) {
		const char* enabled_str = (enabled) ? "on" : "off";
		std::sprintf(buffer, "mac set adr %s\r\n", enabled_str);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	std::optional<bool> LoRa::get_adaptive_datarate() {
		std::sprintf(buffer, "mac get adr\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		return std::strcmp(buffer, "on") == 0;
	}
	LoRa::STATUS LoRa::set_duty_cycle(uint8_t channel, uint16_t duty_cycle) {
		if (channel > 8) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		std::sprintf(buffer, "mac set ch dcycle %u %u\r\n", channel, duty_cycle);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	std::optional<uint16_t> LoRa::get_duty_cycle(uint8_t channel) {
		if (channel > 8) {
			last_error = STATUS::INVALID_PARAMETER;
			return { };
		}
		std::sprintf(buffer, "mac get ch dcycle %u\r\n", channel);
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		const uint16_t duty_cycle = std::atoi(buffer);
		if (duty_cycle == 0 && std::strcmp(buffer, "0") != 0) {
			last_error = STATUS::COMMAND_ERROR;
			return { };
		}
		return duty_cycle;
	}
	LoRa::STATUS LoRa::set_drrange(uint8_t channel, uint8_t min_range, uint8_t max_range) {
		if (channel > 8 || min_range > 7 || max_range > 7) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		std::sprintf(buffer, "mac set ch drange %u %u %u\r\n", channel, min_range, max_range);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY);
	}
	LoRa::STATUS LoRa::set_channel_status(uint8_t channel, bool enabled) {
		if (channel > 8) {
			last_error = STATUS::INVALID_PARAMETER;
			return last_error;
		}
		const char* enabled_str = (enabled) ? "on" : "off";
		std::sprintf(buffer, "mac set ch status %u %s\r\n", channel, enabled_str);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	std::optional<bool> LoRa::get_channel_status(uint8_t channel) {
		if (channel > 8) {
			last_error = STATUS::INVALID_PARAMETER;
			return { };
		}
		std::sprintf(buffer, "mac get ch status %u\r\n", channel);
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		return std::strcmp(buffer, "on") == 0;
	}
	LoRa::STATUS LoRa::set_device_address(const char* address) {
		std::sprintf(buffer, "mac set devaddr %s\r\n", address);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	LoRa::STATUS LoRa::get_device_address(char* device_address, size_t device_address_length) {
		std::sprintf(buffer, "mac get devaddr\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return status;
		}
		const size_t chars_to_copy = std::min(strlen(buffer), device_address_length - 1);  //leaving place for null terminator
		std::copy_n(buffer, chars_to_copy, device_address);
		return STATUS::OK;
	}
	LoRa::STATUS LoRa::set_network_session_key(const char* session_key) {
		std::sprintf(buffer, "mac set nwkskey %s\r\n", session_key);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	LoRa::STATUS LoRa::set_application_key(const char* application_key) {
		std::sprintf(buffer, "mac set appkey %s\r\n", application_key);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY);
	}
	LoRa::STATUS LoRa::set_application_session_key(const char* session_key) {
		std::sprintf(buffer, "mac set appskey %s\r\n", session_key);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	LoRa::STATUS LoRa::set_application_identifier(const char* identifier) {
		std::sprintf(buffer, "mac set appeui %s\r\n", identifier);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	LoRa::STATUS LoRa::get_application_identifier(char* identifier, size_t identifier_length) {
		std::sprintf(buffer, "mac get devaddr\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return status;
		}
		size_t chars_to_copy = std::min(strlen(buffer), identifier_length - 1); //leaving place for null terminator
		std::copy_n(buffer, chars_to_copy, identifier);
		return STATUS::OK;
	}
	LoRa::STATUS LoRa::save_settings() {
		std::sprintf(buffer, "mac save\r\n");
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, 2000, false);
	}
	LoRa::STATUS LoRa::set_class(const char class_key) {
		std::sprintf(buffer, "mac set class %c\r\n", class_key);
		return send_message(buffer, BUFFER_LENGTH, OK_REPLY, NORMAL_TIMEOUT, false);
	}
	std::optional<char> LoRa::get_class() {
		std::sprintf(buffer, "mac get class\r\n");
		STATUS status = send_message(buffer, BUFFER_LENGTH);
		if (status != STATUS::OK) {
			return { };
		}
		return buffer[0];
	}
	LoRa::STATUS LoRa::restore_factory_settings() {
		std::sprintf(buffer, "sys factoryRESET\r\n");
		return send_message(buffer, BUFFER_LENGTH);
	}
	LoRa::RX_MESSAGE LoRa::get_last_rx_message() {
		return last_rx_message;
	}
	std::optional<LoRa::RX_MESSAGE> LoRa::try_receive(uint32_t timeout) {
		serialMessage.set_payload_length(256);
		serialMessage.set_timeout(timeout);
		serialHandle.read(serialMessage);
		std::copy_n(serialMessage.get_payload(), serialMessage.get_payload_length(), receive_buffer);
		size_t rx_message_pos = Utils::cstring_contains(receive_buffer, strlen(receive_buffer), RX_MESSAGE_PREFIX, strlen(RX_MESSAGE_PREFIX));
		if (rx_message_pos != -1) {
			char* rx_message_first_char_pos = receive_buffer + rx_message_pos;
			size_t rx_message_length = Utils::cstring_contains(rx_message_first_char_pos, strlen(rx_message_first_char_pos), "\r\n", 2) + 2;  //+2 because of crlf
			parse_rx_message(rx_message_first_char_pos, rx_message_length);
			return last_rx_message;
		}
		return {};
	}
	void LoRa::sync_uart() {
		std::sprintf(buffer, "\r\n");
		send_message(buffer, BUFFER_LENGTH, nullptr, NORMAL_TIMEOUT, false);
	}
}