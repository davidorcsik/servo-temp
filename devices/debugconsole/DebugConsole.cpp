#include <DebugConsole.h>

#include <optional>
#include <cstring>
#include <algorithm>

namespace devices {
	protocols::ISerial* DebugConsole::serialHandle = nullptr;
	protocols::SerialMessage DebugConsole::serialMessage;
	char DebugConsole::buffer[512] = { 0 };
	bool DebugConsole::init(protocols::ISerial& _serialHandle) {
		serialHandle = &_serialHandle;
		std::optional<protocols::SerialMessage> created_message = serialHandle->create_message(nullptr, 0, 10, protocols::ISerial::DEFAULT_ADDRESS);
		if (!created_message.has_value()) {
			return false;
		}
		serialMessage = created_message.value();
		return true;
	}

	void DebugConsole::log(const char* message) {
		std::sprintf(buffer, "%s\r\n", message);
		serialMessage.set_payload(buffer);
		serialHandle->write(serialMessage);
	}

	void DebugConsole::log(uint32_t value) {
		std::sprintf(buffer, "%lu", value);
		log(buffer);
	}

	void DebugConsole::log(int32_t value) {
		std::sprintf(buffer, "%ld", value);
		log(buffer);
	}

	void DebugConsole::log(uint16_t value) {
		log(static_cast<uint32_t>(value));
	}

	void DebugConsole::log(int16_t value) {
		log(static_cast<int32_t>(value));
	}

	void DebugConsole::log(uint8_t value) {
		log(static_cast<uint32_t>(value));
	}

	void DebugConsole::log(int8_t value) {
		log(static_cast<int32_t>(value));
	}
	
	void DebugConsole::log(bool value) {
		if (value) {
			log("True");
		} else {
			log("False");
		}
	}
	
	void DebugConsole::log(double value) {
		const char *tmpSign = (value < 0) ? "-" : "";
		double tmpVal = (value < 0) ? -value : value;

		int32_t tmpInt1 = tmpVal;                   // Get the integer (678).
		double tmpFrac = tmpVal - tmpInt1;        // Get fraction (0.0123).
		uint32_t tmpInt2 = tmpFrac * 10000000;   // Turn into integer (123).

		// Print as parts, note that you need 0-padding for fractional bit.

		std::sprintf(buffer, "%s%ld.%04lu", tmpSign, tmpInt1, tmpInt2);
		log(buffer);
	}
}