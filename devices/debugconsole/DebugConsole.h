#pragma once

#include <ISerial.h>
#include <UART.h>

namespace devices {
	class DebugConsole {
	private:
		static protocols::ISerial* serialHandle;
		static protocols::SerialMessage serialMessage;
		static char buffer[512];
	public:
		static void log(const char* message);
		static void log(uint32_t value);
		static void log(int32_t value);
		static void log(uint16_t value);
		static void log(int16_t value);
		static void log(uint8_t value);
		static void log(int8_t value);
		static void log(bool value);
		static void log(double value);
		static bool init(protocols::ISerial& _serialHandle);
	};
}