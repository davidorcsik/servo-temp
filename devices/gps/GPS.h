#pragma once

#include <ISerial.h>
#include <utils.h>

#include <unordered_map>
#include <functional>

#include <stm32l4xx_hal.h>

namespace devices {
	class GPS {
	public:
		struct GPSData {
			double latitude = 0;
			uint8_t latitude_whole = 0;
			uint8_t latitude_minutes = 0;
			double latitude_seconds = 0;
			char latitude_pole = '\0';
			double longitude = 0;
			uint8_t longitude_whole = 0;
			uint8_t longitude_minutes = 0;
			double longitude_seconds = 0;
			char longitude_pole = '\0';
			double altitude = 0;
			double heading = 0;
			Utils::DateTime date_time;
			
			uint8_t fix = 0;
			uint8_t satellites = 0;
			uint8_t speed = 0;
			GPSData() { date_time.value = 0; }
		};
#pragma pack(push, 1)
		struct GPSDataSemtech {
			uint64_t battery_voltage : 8;
			int64_t latitude : 24;
			int64_t longitude : 24;
			uint64_t altitude : 16;
		};
#pragma pack(pop)
#pragma pack(push, 1)
		struct GPSDataAdeunis {
			uint32_t status : 1;
			uint32_t temperature : 1;
			int32_t latitude : 4;
			int32_t longitude : 4;
			uint32_t altitude : 16;
			uint32_t battery_voltage : 2;
		};
#pragma pack(pop)
		static const size_t LAST_UPDATE_COUNT = 10;
		static const size_t UPDATE_MESSAGE_LENGTH = 256;
	private:
		struct NMEA_SENTENCES {
			static const uint32_t GPGNS =				0x1;
			static const uint32_t GPGGA =				0x2;
			static const uint32_t GPGSA =				0x4;
			static const uint32_t GPGST =				0x8;
			static const uint32_t GPVTG =				0x10;
			static const uint32_t PSTMNOISE =			0x20;
			static const uint32_t GPRMC =				0x40;
			static const uint32_t PSTMRF =				0x80;
			static const uint32_t PSTMTG =				0x100;
			static const uint32_t PSTMTS =				0x200;
			static const uint32_t PSTMPA =				0x400;
			static const uint32_t PSTMSAT =				0x800;
			static const uint32_t PSTMRES =				0x1000;
			static const uint32_t PSTMTIM =				0x2000;
			static const uint32_t PSTMWAAS =			0x4000;
			static const uint32_t PSTMDIFF =			0x8000;
			static const uint32_t PSTMCORR =			0x10000;
			static const uint32_t PSTMSBAS =			0x20000;
			static const uint32_t PSTMTESTRF =			0x40000;
			static const uint32_t GPGSV =				0x80000;
			static const uint32_t GPGLL =				0x100000;
			static const uint32_t PSTMPPSDATA =			0x200000;
			static const uint32_t PSTMCPU =				0x800000;
			static const uint32_t GPZDA =				0x1000000;
			static const uint32_t PSTMTRAIMSTATUS =		0x2000000;
			static const uint32_t PSTMPOSHOLD =			0x4000000;
			static const uint32_t PSTMKFCOV =			0x8000000;
			static const uint32_t PSTMAGPS =			0x10000000;
			static const uint32_t PSTMLOWPOWERDATA =	0x20000000;
			static const uint32_t PSTMNOTCHSTATUS =		0x40000000;
			static const uint32_t PSTMTM =				0x80000000;
		};
		static const std::array<uint32_t, 5> ENABLED_NMEA_SENTENCES;
		static const std::array<char, 9> COMMAND_DELIMETERS;
		static uint32_t calculate_checksum(const char* command, uint8_t command_length);
		static bool is_checksum_valid(const char* command, uint8_t command_length);
		static uint8_t get_command_name(char* command, uint8_t length, char* command_name);
		typedef std::function<void(const char*, uint8_t, GPSData&)> parser_function_pointer;
		static const std::unordered_map<std::string, parser_function_pointer> command_parser_map;
		static void parse_gga(const char* command, uint8_t length, GPSData& data);
		static void parse_gsa(const char* command, uint8_t length, GPSData& data);
		static void parse_rmc(const char* command, uint8_t length, GPSData& data);
		static void parse_vtg(const char* command, uint8_t length, GPSData& data);
		static void parse_hchdg(const char* command, uint8_t length, GPSData& data);
		static const size_t READ_BUFFER_LENGTH = 1024;
		char read_buffer[READ_BUFFER_LENGTH] = { 0 };
		std::array<char[UPDATE_MESSAGE_LENGTH], LAST_UPDATE_COUNT> last_update;
		GPIO_TypeDef* const wakeup_port;
		const uint16_t wakeup_pin;
		
		protocols::ISerial& serialHandle;
		protocols::SerialMessage serialMessage;
		GPSData parsedData;
		std::optional<const GPS::GPSData*> parse();
		
	public:
		GPS(protocols::ISerial& _serialHandle, GPIO_TypeDef* _wakeup_port, uint16_t _wakeup_pin)
		: serialHandle(_serialHandle), wakeup_port(_wakeup_port), wakeup_pin(_wakeup_pin) {
			std::optional<protocols::SerialMessage> created_message = serialHandle.create_message(nullptr, 0, 250, protocols::ISerial::DEFAULT_ADDRESS);
			if (!created_message.has_value()) {
				Utils::ERROR_GUARD();
			}
			serialMessage = created_message.value();
		}
		virtual ~GPS() {}
		
		bool init();
		size_t read(char* buffer, size_t buffer_length);
		GPSData read_and_parse();
		std::array<char[UPDATE_MESSAGE_LENGTH], LAST_UPDATE_COUNT> get_last_update_message();
		GPSData get_last_update();
		void get_last_update_as_semtech(uint8_t battery_voltage, char* semtech_buffer, size_t semtech_buffer_length);
		void get_last_update_as_adeunis(uint8_t status, uint8_t temperature, uint16_t battery_voltage, char* adeunis_buffer, size_t adeunis_buffer_length);
		void get_last_update_as_adeunis_alternative(uint8_t status, uint8_t temperature, uint16_t battery_voltage, char* adeunis_buffer, size_t adeunis_buffer_length);
	};
}