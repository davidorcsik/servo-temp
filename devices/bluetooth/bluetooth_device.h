#pragma once
#include <array>
#include <algorithm>
#include <vector>
#include <functional>

#include <bluetooth_service.h>
#include <bluetooth_characteristic.h>
#include <bluetooth_common.h>

namespace devices
{
	namespace Bluetooth
	{
		struct BluetoothServiceCharacteristicRelation {
			BluetoothService* service;
			std::array<BluetoothCharacteristic*, 5> characteristics;
		};
		class BluetoothDevice {
		private:
			static void attach_event_listeners();
			static bool event_listeners_attached;
			static BluetoothDevice* instance;
			enum class MULTIROLE_STATE { DISABLED = 0, ENABLED = 1 };
			enum class GAP_ROLES {
				PERIPHERAL =			0x01,
				BROADCASTER =			0x02,
				CENTRAL =				0x04,  //for IDB05A1 (0x03 for IDB04A1)
				OBSERVER =				0x08,  //for IDB05A1 (0x04 for IDB04A1)
				PERIPHERAL_CENTRAL  =	0x05,
			};
			enum class PERIPHERAL_STATE {
				DISCONNECTED =	0x00,
				CONNECTED =		0x01,
				IDLE =			0x02,
				ADVERTISING =		0x03,
				PAIRED =			0x04,
			};
			enum class CENTRAL_STATE {
				DISCONNECTED =							0x00,
				DISCONNECTING =							0x01,
				SCANNING =								0x02,
				SCANNING_COMPLETED =					0x02,
				CONNECTING =							0x03,
				CONNECTED =								0x04,
				PAIRING =								0x05,
				PAIRED  =								0x06,
				SERVICE_DISCOVERY =						0x07,
				SERIVCE_DISCOVERY_COMPLETED =			0x08,
				CHARACTERISTIC_DISCOVERY =				0x09,
				CHARACTERISTIC_DISCOVERY_COMPLETED =	0x0a,
				IDLE =									0x0b,
			};
			enum class PROCEDURE_STATE {
				NO_PROCEDURE =	0x00,
				PENDING =		0x01,
			};
			enum class AUTHENTICATION_REQUIREMENET { NOT_REQUIRED = 0, REQUIRED = 1 };
			std::array<uint8_t, 7> mac;
			const std::string board_name;
			const std::string manufacturer_data;
			const std::string advertising_name;
			std::array<const char*, 2> advertising_data = { nullptr, nullptr };
			std::array<UUID, 6> advertising_uuids;
			uint16_t* const gap_service_handle;
			uint16_t device_name_characteristic_handle;
			uint16_t appareance_characteristic_handle;
			uint32_t pin;
			PERIPHERAL_STATE peripheral_state = PERIPHERAL_STATE::DISCONNECTED;
			CENTRAL_STATE central_state = CENTRAL_STATE::DISCONNECTED;
			PROCEDURE_STATE procedure_state = PROCEDURE_STATE::NO_PROCEDURE;
			std::vector<BluetoothService> services = std::vector<BluetoothService>(6);
			std::vector<BluetoothCharacteristic> characteristics = std::vector<BluetoothCharacteristic>(6);
			STATUS init_hardware(MULTIROLE_STATE multirole);
			STATUS init_stack(GAP_ROLES mode, AUTHENTICATION_REQUIREMENET authentication);
			std::array<BluetoothServiceCharacteristicRelation, 6> service_characteristic_relations;
			
		public:
			BluetoothDevice(uint8_t _mac[6], const char* _board_name, const char* _manufacturer_data, const char* _advertising_name, uint16_t* const _gap_service_handle)
			: board_name(_board_name), manufacturer_data(_manufacturer_data), advertising_name(_advertising_name), gap_service_handle(_gap_service_handle) {
				std::copy_n(_mac, 6, mac.begin());
				mac[6] = 0;
				if (!event_listeners_attached) {
					attach_event_listeners();	
				}
				instance = this;
				init_hardware(MULTIROLE_STATE::DISABLED);
				init_stack(GAP_ROLES::PERIPHERAL, AUTHENTICATION_REQUIREMENET::REQUIRED);
			}
			virtual ~BluetoothDevice() {}
			const uint8_t* get_mac() const;
			const std::string& get_board_name() const;
			const std::string& get_manufacturer_data() const;
			STATUS set_pin(const uint32_t& pin);
			STATUS set_advertising(bool state);
			STATUS set_advertising_data(bool manufacturer, bool uuid);
			STATUS add_service(BluetoothService service);
			STATUS add_characteristic(Bluetooth::UUID serivce_uuid, BluetoothCharacteristic characteristic);
			
			virtual uint8_t on_connect(uint8_t mac[6], uint16_t handle);
			virtual uint8_t on_disconnect(uint16_t handle, Bluetooth::STATUS);
			virtual uint8_t on_pairing_complete(uint16_t handle, Bluetooth::STATUS);
			virtual uint8_t on_passkey_needed(uint32_t* passkey, uint16_t* connection_handle);
			
			static void connection_callback(uint8_t mac[6], uint16_t handle);
			static void disconnect_callback(uint16_t handle, uint8_t result);
			static void pairing_complete_callback(uint16_t handle, uint8_t status);
			static void passkey_needed_callback(uint32_t* passkey, uint16_t* connection_handle);
		};
	}
}