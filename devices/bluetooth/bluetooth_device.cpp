#include <bluetooth_device.h>

#include <bnrg_common.h>

namespace devices
{
	namespace Bluetooth
	{
		bool BluetoothDevice::event_listeners_attached = false;
		STATUS BluetoothDevice::init_hardware(MULTIROLE_STATE multirole) {
			STATUS status;
			//initialization of SPI and HCI
			hci_init(hci_event_cb, NULL);
			if (multirole == MULTIROLE_STATE::ENABLED) {
				uint8_t stack_mode_3 = 0x03; 	//if the device is peripheral then 2 central can connect to it. if central then it can connect 8 peripheral
												//if dual-role than 1 central can connect to it and it can connect 8 peripheral

				hci_reset();
				status = static_cast<STATUS>(aci_hal_write_config_data(CONFIG_DATA_MODE_OFFSET, CONFIG_DATA_MODE_LEN, &stack_mode_3)); 	//multiple connection (stack mode 3)

				if(status != STATUS::SUCCESS) {
					return status;
				}

				status = static_cast<STATUS>(aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, mac.data()));
			} else {
				//reset device
				hci_reset();
				HAL_Delay(5);
				//set up address
				status = static_cast<STATUS>(aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, mac.data()));
			}
			return status;
		}
		STATUS BluetoothDevice::init_stack(BluetoothDevice::GAP_ROLES mode, BluetoothDevice::AUTHENTICATION_REQUIREMENET authentication) {
			STATUS status;

			//initialization of gatt layer (this function automatically adds attribute profile service and service changed characteristic
			status = static_cast<STATUS>(aci_gatt_init());
			if (status != STATUS::SUCCESS) {
				return status;
			}

			//initializing gap layer (this function automatically adds gap service, device name and appearance characteristic
			status = static_cast<STATUS>(aci_gap_init_IDB05A1(static_cast<uint8_t>(mode), 0, board_name.length(), gap_service_handle, &device_name_characteristic_handle, &appareance_characteristic_handle));
			if (status != STATUS::SUCCESS) {
				return status;
			}

			//set up basic characteristics
			status = static_cast<STATUS>(aci_gatt_update_char_value(*gap_service_handle, device_name_characteristic_handle, 0, board_name.length(), board_name.c_str()));

			if (status != STATUS::SUCCESS) {
				return status;
			}

			//set authentication and io capability for legacy pairing

			switch(mode) {
			case GAP_ROLES::PERIPHERAL:
				status = static_cast<STATUS>(aci_gap_set_io_capability(IO_CAP_DISPLAY_ONLY));
				break;

			case GAP_ROLES::CENTRAL:
				status = static_cast<STATUS>(aci_gap_set_io_capability(IO_CAP_KEYBOARD_ONLY));
				break;

			case GAP_ROLES::PERIPHERAL_CENTRAL:
				status = static_cast<STATUS>(aci_gap_set_io_capability(IO_CAP_DISPLAY_ONLY));
				break;
			default:
				break;
			}
			
			if (status != STATUS::SUCCESS) {
				return status;
			}
			
			status = static_cast<STATUS>(aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED, OOB_AUTH_DATA_ABSENT, NULL, 7, 16, DONOT_USE_FIXED_PIN_FOR_PAIRING, 0, BONDING));
			if (status != STATUS::SUCCESS) {
				return status;
			}
			
			aci_hal_set_tx_power_level(1, 4);

			return status;
		}
		const uint8_t* BluetoothDevice::get_mac() const {
			return mac.data();
		}
		const std::string& BluetoothDevice::get_board_name() const {
			return board_name;
		}
		STATUS BluetoothDevice::set_pin(const uint32_t& newPin) {
			pin = newPin;
			return STATUS::SUCCESS;
		}
		STATUS BluetoothDevice::set_advertising(bool state) {
			if (!state) {
				return static_cast<STATUS>(aci_gap_set_non_discoverable());
			}
			peripheral_state = PERIPHERAL_STATE::ADVERTISING;
			procedure_state = PROCEDURE_STATE::PENDING;
			STATUS status;
			int i;
			uint8_t offset;
			uint8_t uuid_array[MAX_ADV_UUID_NUM * 16 + 2] = { 0 }; 																//worst case is when each service is costume service and AD_TYPE should be used for the first and the last byte
			uint8_t uuid_array_size = 0;
			uint8_t uuid_type_indicator = 0x00;

			char name[MAX_ADV_NAME_SIZE + 1] = { AD_TYPE_COMPLETE_LOCAL_NAME, 0 };
			uint8_t name_size = 0;

			if (advertising_name.length() > MAX_ADV_NAME_SIZE)
				return  STATUS::INVALID_NAME_SIZE;

			name_size = advertising_name.length() + 1;
			memcpy(name + 1, advertising_name.data(), advertising_name.length()); 																		//copy local_name array to name array from first index

			uint8_t manufact_data[MAX_ADV_MANUF_DATA_SIZE + 2];
			std::copy(manufacturer_data.begin(), manufacturer_data.end(), manufact_data);
			manufact_data[MAX_ADV_MANUF_DATA_SIZE] = AD_TYPE_MANUFACTURER_SPECIFIC_DATA;
			manufact_data[MAX_ADV_MANUF_DATA_SIZE + 1] = 0;
			uint8_t manufact_data_size = 0;

			if (manufacturer_data.length() > MAX_ADV_MANUF_DATA_SIZE)
				return STATUS::INVALID_MANUF_DATA_SIZE;

			manufact_data_size = manufacturer_data.length();
			memcpy(manufact_data + 2, manufacturer_data.data(), manufacturer_data.length());

			hci_le_set_scan_resp_data(0, NULL); 																					//scan response disabled (not used)

			for(i = 0 ; i < advertising_uuids.size() ; i++)
			{
				if (advertising_uuids[i].get_type() == UUID_TYPES::UUID_TYPE_128_BIT)
					uuid_type_indicator |= 0x01;
				if (advertising_uuids[i].get_type() == UUID_TYPES::UUID_TYPE_16_BIT)
					uuid_type_indicator |= 0x02;
			}

//			switch (uuid_type_indicator)
//			{
//			case 0x01:
//				uuid_array_size = ble_fill_128bit_uuid(uuid_array, sizeof(uuid_array), advertising_uuids.data(), advertising_uuids.size());
//				break;
//
//			case 0x02:
//				uuid_array_size = ble_fill_16bit_uuid(uuid_array, sizeof(uuid_array), advertising_uuids.data(), advertising_uuids.size());
//				break;
//
//			case 0x03:
//				offset = ble_fill_128bit_uuid(uuid_array, sizeof(uuid_array), advertising_uuids.data(), advertising_uuids.size());
//				uuid_array_size = ble_fill_16bit_uuid(uuid_array + offset, sizeof(uuid_array), advertising_uuids.data(), advertising_uuids.size());
//				break;
//
//			default:
//				uuid_array_size = 0;
//				break;
//			}

			//TODO: returns invalid parameters in case of costume id (stack did not support costume UUID advertising)
			status = static_cast<STATUS>(aci_gap_set_discoverable(ADV_IND, 0x20, 0x100, PUBLIC_ADDR, NO_WHITE_LIST_USE, name_size, name, uuid_array_size, uuid_array, 0x006, 0x008));
			//advertising interval is the period between advertising events (when advertising packet sent in each advertising channel at the same time)
			if(status != STATUS::SUCCESS)
			{
				return status;
			}

			if (manufacturer_data.length() > 1)
			{
				status = static_cast<STATUS>(aci_gap_update_adv_data(manufact_data_size, manufact_data));
			}

			peripheral_state = PERIPHERAL_STATE::ADVERTISING;
			return status;
		}
		STATUS BluetoothDevice::add_service(BluetoothService service) {
			auto relation_it = std::find_if_not(service_characteristic_relations.begin(), service_characteristic_relations.end(), [](BluetoothServiceCharacteristicRelation& relation) { return relation.service->is_service_valid();});
			if (relation_it == service_characteristic_relations.end()) {
				return STATUS::NO_PLACE_FOR_SERVICE;
			}
			services.push_back(service);
			relation_it->service = &services.back();
			return STATUS::SUCCESS;
		}
		STATUS BluetoothDevice::add_characteristic(const Bluetooth::UUID service_uuid, BluetoothCharacteristic characteristic) {
			auto relation_it = std::find_if_not(service_characteristic_relations.begin(), service_characteristic_relations.end(), [&service_uuid](BluetoothServiceCharacteristicRelation& relation) { return relation.service->get_uuid() == service_uuid; });
			if (relation_it == service_characteristic_relations.end()) {
				return STATUS::SERVICE_DOES_NOT_EXISTS;
			}
			auto characteristic_it = std::find_if_not(relation_it->characteristics.begin(), relation_it->characteristics.end(), [](BluetoothCharacteristic* characteristic) { return characteristic->is_valid(); });
			if (characteristic_it == relation_it->characteristics.end()) {
				return STATUS::NO_PLACE_FOR_CHARACTERISTIC;
			}
			characteristics.push_back(characteristic);
			*characteristic_it = &characteristics.back();
		}
		
		void BluetoothDevice::attach_event_listeners() {
//			ble_peripheral_set_connection_complete_cb(&BluetoothDevice::connection_callback);
//			ble_peripheral_set_disconnection_complete_cb(&BluetoothDevice::disconnect_callback);
//			ble_peripheral_set_pairing_complete_cb(&BluetoothDevice::pairing_complete_callback);
//			ble_peripheral_set_attribute_modified_cb(&BluetoothDevice::passkey_needed_callback);
			event_listeners_attached = true;
		}
		
		STATUS BluetoothDevice::set_advertising_data(bool manufacturer, bool uuid) {
			advertising_data[0] = (manufacturer) ? manufacturer_data.c_str() : nullptr;
			// set uuids
		}
		
		uint8_t BluetoothDevice::on_connect(uint8_t mac[6], uint16_t handle) {
		}
		uint8_t BluetoothDevice::on_disconnect(uint16_t handle, Bluetooth::STATUS) {
		}
		uint8_t BluetoothDevice::on_pairing_complete(uint16_t handle, Bluetooth::STATUS) {
		}
		uint8_t BluetoothDevice::on_passkey_needed(uint32_t* passkey, uint16_t* connection_handle) {
		}
		
		void BluetoothDevice::connection_callback(uint8_t mac[6], uint16_t handle) {
			instance->on_connect(mac, handle);
		}
		void BluetoothDevice::disconnect_callback(uint16_t handle, uint8_t status) {
			instance->on_disconnect(handle, static_cast<Bluetooth::STATUS>(status));
		}
		void BluetoothDevice::pairing_complete_callback(uint16_t handle, uint8_t status) {
			instance->on_pairing_complete(handle, static_cast<Bluetooth::STATUS>(status));
		}
		void BluetoothDevice::passkey_needed_callback(uint32_t* passkey, uint16_t* connection_handle) {
			instance->on_passkey_needed(passkey, connection_handle);
		}
	}
}