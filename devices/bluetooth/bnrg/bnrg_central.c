/*
 * ble_central.c
 *
 *  Created on: Feb 15, 2019
 *      Author: mate_pc
 */

#include "bnrg_central.h"

static uint8_t max_scann_num = 6;								//scanning will be terminated after 6 found devices

static ble_central_state_t device_state = c_disconnected;		//status of the device
extern ble_central_procedure_state_t central_procedure_state;	//procedure state (should not start new procedure if another procedure in progress)
static uint8_t found_dev_addresses[48] = {0};		//array to save found devices
static uint8_t found_dev_cntr = 0;								//counter to save how many device was found

static ble_conn_cb_t ble_conn_cb;								//function pointers for callbacks
static ble_disconn_cb_t ble_disconn_cb;
static ble_pairing_cmplt_cb_t ble_pairing_cmplt_cb;
static ble_dev_found_cb_t ble_dev_found_cb;
static ble_serv_disc_cb_t ble_serv_disc_cb;
static ble_char_disc_cb_t ble_char_disc_cb;
static ble_read_req_res_cb_t ble_read_req_res_cb;
static ble_notify_res_cb_t ble_notify_res_cb;
static ble_passkey_needed_cb_t ble_passkey_needed_cb;

uint8_t ble_is_found(uint8_t *addr,uint8_t size)				//check if addr contained MAC was found
{
	int i;
	for(i=0;i<found_dev_cntr;i++)
	{
		if(!memcmp(addr,&found_dev_addresses[i*6],6))
		{
			return 1;
		}
	}

	return 0;
}

void ble_set_max_num_of_scann(uint8_t num)						//function to set max_scann_num (it is not recommended to set it up more than 8)
{
	if(max_scann_num > 8)
	{
		max_scann_num = 8;
	}
	else
	{
		max_scann_num = num;
	}
}

tBleStatus ble_enable_notification(uint16_t connection_handler,uint16_t characteristic_handler)
{
	uint8_t notify[] = {0x01,0x00};
	volatile tBleStatus ret = aci_gatt_write_charac_descriptor(connection_handler,characteristic_handler, 0x02, notify);
	return ret;
}

tBleStatus ble_create_multiple_connection(uint8_t* addr,uint8_t addr_len)
{
	volatile tBleStatus ret;

	ret = aci_gap_create_connection(0x0010,0x0010,PUBLIC_ADDR,addr,PUBLIC_ADDR,0x6c,0x6c,0x00,0xc80,0x000c,0x000c);

	if(ret != BLE_STATUS_SUCCESS)
	{
		return ret;
	}
	device_state = c_connecting;
	central_procedure_state = c_pending_proc;

	return ret;
}

tBleStatus ble_disable_notification(uint16_t connection_handler,uint16_t characteristic_handler)
{
	uint8_t notify[] = {0x00,0x00};
	volatile tBleStatus ret = aci_gatt_write_charac_descriptor(connection_handler,characteristic_handler, 0x02, notify);
	return ret;
}

tBleStatus ble_start_general_connection()
{
	volatile tBleStatus ret = aci_gap_start_general_conn_establish_proc_IDB05A1(PASSIVE_SCAN,0x0004,0x0004, PUBLIC_ADDR,0x01);
	//type of scan can be active or passive (active means scanning device can get additional information by scan request packet and scan response packet)
	//second and third parameter is scan interval and window
	//scan interval: how often the controller should scan
	//scan window: how long the device scan in each interval
	device_state = c_scanning;
	central_procedure_state = c_pending_proc;
	return ret;
}

tBleStatus ble_start_general_discovery()
{
	volatile tBleStatus ret = aci_gap_start_general_discovery_proc(0x10,0x10,PUBLIC_ADDR,0x00);
	//first two parameter is scan interval and scan window
	//scan interval: how often the controller should scan
	//scan window: how long the device scan in each interval
	device_state = c_scanning;
	central_procedure_state = c_pending_proc;
	return ret;
}

tBleStatus ble_create_connection(uint8_t* addr,uint8_t addr_len)
{
	volatile tBleStatus ret;

	ret = aci_gap_create_connection(0x4000, 0x4000, PUBLIC_ADDR,addr, PUBLIC_ADDR, 40, 100, 0, 60, 2000 , 2000);

	if(ret != BLE_STATUS_SUCCESS)
	{
		return ret;
	}
	device_state = c_connecting;
	central_procedure_state = c_pending_proc;
	return ret;
}

tBleStatus ble_start_service_discovery(uint16_t connection_handler)
{
	volatile tBleStatus ret = aci_gatt_disc_all_prim_services(connection_handler);
	device_state = c_service_discovery;
	central_procedure_state = c_pending_proc;
	return ret;
}

tBleStatus ble_start_char_discovery(uint16_t connection_handler,uint16_t service_handler,uint16_t service_end_handler)
{
	volatile tBleStatus ret = aci_gatt_disc_all_charac_of_serv(connection_handler,service_handler,service_end_handler);
	device_state = c_characteristic_discovery;
	central_procedure_state = c_pending_proc;
	return ret;
}

tBleStatus ble_read_remote_char(uint16_t connection_handler,uint16_t char_val_handler)
{
	volatile tBleStatus ret = aci_gatt_read_charac_val(connection_handler,char_val_handler);
	return ret;
}

ble_central_state_t ble_get_central_state()
{
	return device_state;
}

tBleStatus ble_send_pairing_request(uint16_t connection_handler,uint8_t force_rebond)
{
	volatile tBleStatus ret = aci_gap_send_pairing_request(connection_handler,force_rebond);
	device_state = c_pairing;
	central_procedure_state = c_pending_proc;
	return ret;
}

static void ble_evt_hci_handler(hci_event_pckt* event_pckt)
{
	switch(event_pckt->evt)
	{
		 //disconnection
		case EVT_DISCONN_COMPLETE:
		{
			disconnect_cp *ptr = (void*) event_pckt->data;
			if(ble_disconn_cb)
			{
				(*ble_disconn_cb)(ptr->handle,ptr->reason);
				device_state = c_disconnected;
				central_procedure_state = c_no_proc;
			}
		}
		break;
	}
}

ble_central_procedure_state_t ble_get_central_proc_state()
{
	return central_procedure_state;
}

static void ble_evt_le_meta_handler(hci_event_pckt* event_pckt)
{
	evt_le_meta_event *evt = (void *)event_pckt->data;
	switch(evt->subevent)
	{
		//connection
		case EVT_LE_CONN_COMPLETE:
		{
			 evt_le_connection_complete *ptr = (void *)evt->data;
			 if(ble_conn_cb)
			 {
				 (*ble_conn_cb)(ptr->peer_bdaddr, ptr->handle);
			 }

			 device_state = c_connected;
			 central_procedure_state = c_no_proc;
		}
		break;
		//device found
		case EVT_LE_ADVERTISING_REPORT:
		{
			le_advertising_info *ptr = (void *)(evt->data+1);

			if(ble_dev_found_cb)
			{
				memcpy(found_dev_addresses+found_dev_cntr*6,ptr->bdaddr,6);
				found_dev_cntr++;
				if(found_dev_cntr == max_scann_num)
				{
					ble_terminate_procedure(0x10);
				}
			}
		}
		break;
	}
}

static void ble_evt_vendor_handler(hci_event_pckt* event_pckt)
{
	evt_blue_aci *blue_evt = (void*)event_pckt->data;
	switch(blue_evt->ecode)
	{
		//discover all services event (central)
		case EVT_BLUE_ATT_READ_BY_GROUP_TYPE_RESP:
		{
			evt_att_read_by_group_resp *ptr = (void*)blue_evt->data;
			if(ble_serv_disc_cb)
			{
				(*ble_serv_disc_cb)(ptr->conn_handle,ptr->event_data_length,ptr->attribute_data_length,ptr->attribute_data_list);
			}
		}
		break;
		//discover characteristics in a specific service (central)
		case EVT_BLUE_ATT_READ_BY_TYPE_RESP:
		{
			evt_att_read_by_type_resp *ptr = (void*)blue_evt->data;
			if(ble_char_disc_cb)
			{
				(*ble_char_disc_cb)(ptr->conn_handle,ptr->event_data_length,ptr->handle_value_pair_length,ptr->handle_value_pair);
			}
		}
		break;
		//read response to read request (central)
		//will be called after red request command
		case EVT_BLUE_ATT_READ_RESP:
		{
			evt_att_read_resp *ptr = (void*)blue_evt->data;
			if(ble_read_req_res_cb)
			{
				(*ble_read_req_res_cb)(ptr->conn_handle,ptr->event_data_length,ptr->attribute_value);
			}
		}
		break;

		case EVT_BLUE_GATT_NOTIFICATION:
		{
			evt_gatt_attr_notification *ptr = (void*)blue_evt->data;
			if(ble_notify_res_cb)
			{
				(*ble_notify_res_cb)(ptr->conn_handle,ptr->event_data_length,ptr->attr_handle,ptr->attr_value);
			}
		}
		break;

		case EVT_BLUE_GAP_PASS_KEY_REQUEST:
		{
			if(ble_passkey_needed_cb != NULL)
			{
				uint16_t connection_handler;
				uint32_t passkey;
				(*ble_passkey_needed_cb)(&passkey,&connection_handler);
				aci_gap_pass_key_response(connection_handler,passkey);
			}
		}
		break;

		case EVT_BLUE_GAP_PAIRING_CMPLT:
		{
			evt_gap_pairing_cmplt *ptr = (void*)blue_evt->data;
			if(ble_pairing_cmplt_cb)
			{
				(*ble_pairing_cmplt_cb)(ptr->conn_handle,ptr->status);
			}
			device_state = c_paired;
			central_procedure_state = c_no_proc;
		}
		break;

		case EVT_BLUE_GATT_PROCEDURE_COMPLETE:
		{
			switch(device_state)
			{
				case c_service_discovery:
					device_state = c_serivce_discovery_completed;
					central_procedure_state = c_no_proc;
				break;

				case c_characteristic_discovery:
					device_state = c_characteristic_discovery_completed;
					central_procedure_state = c_no_proc;
				break;

				default:
				break;
			}

		}
		break;

		case EVT_BLUE_GAP_PROCEDURE_COMPLETE:
		{
			switch(device_state)
			{
				case c_scanning:						//application started general connection and started to scan for devices, the scanning is done
					if(ble_dev_found_cb)
					{
						(*ble_dev_found_cb)(found_dev_cntr,found_dev_addresses);	//give back all found devices
					}
					device_state = c_scanning_completed;
					central_procedure_state = c_no_proc;

				break;

				default:
					break;
			}

		}
		break;

	}
}

void ble_hci_event_cb_central(void *pData)
{
	hci_uart_pckt *hci_pckt = pData;
	hci_event_pckt *event_pckt = (hci_event_pckt*) hci_pckt->data;

	if (hci_pckt->type != HCI_EVENT_PKT)
	{
		return;
	}

	switch (event_pckt->evt)
	{

		case EVT_LE_META_EVENT:							//hci meta events
			ble_evt_le_meta_handler(event_pckt);
			break;

		case EVT_VENDOR:
			ble_evt_vendor_handler(event_pckt);			//vendor specific events
			break;

		default:
			ble_evt_hci_handler(event_pckt);			//hci events
			break;
	}
}

void ble_terminate_procedure(uint8_t procedure_code)
{
	aci_gap_terminate_gap_procedure(procedure_code);
}

void ble_diconnect(uint16_t conn_handler)
{
	aci_gap_terminate(conn_handler,0x13);
}

void ble_blocking_delay(uint32_t ms)
{
	HAL_Delay(ms);
}

void ble_central_set_connection_complete_cb(ble_conn_cb_t cb)
{
	 ble_conn_cb = cb;
}

void ble_central_set_disconnection_complete_cb(ble_disconn_cb_t cb)
{
	ble_disconn_cb = cb;
}

void ble_central_set_pairing_complete_cb(ble_pairing_cmplt_cb_t cb)
{
	ble_pairing_cmplt_cb = cb;
}

void ble_central_set_device_found_cb(ble_dev_found_cb_t cb)
{
	ble_dev_found_cb = cb;
}

void ble_central_set_service_discovery_result_cb(ble_serv_disc_cb_t cb)
{
	ble_serv_disc_cb = cb;
}

void ble_central_set_characteristic_discovery_result_cb(ble_char_disc_cb_t cb)
{
	ble_char_disc_cb = cb;
}

void ble_central_set_read_request_result_cb(ble_read_req_res_cb_t cb)
{
	 ble_read_req_res_cb = cb;
}

void ble_central_set_notification_result_cb(ble_notify_res_cb_t cb)
{
	 ble_notify_res_cb = cb;
}

void ble_central_set_passkey_needed_cb(ble_passkey_needed_cb_t cb)
{
	ble_passkey_needed_cb = cb;
}

