/*
 * ble_peripheral.h
 *
 *  Created on: Feb 7, 2019
 *      Author: mate_pc
 */

#ifndef BNRG_PERIPHERAL_H_
#define BNRG_PERIPHERAL_H_

#include "bnrg_common.h"

#ifdef __cplusplus
extern "C" {
#endif

tBleStatus ble_start_advertising_extended(const char*local_name, uint8_t local_name_size, uint8_t*manuf_data,
		uint8_t manuf_data_size, uuid_t* uuid, uint8_t uuid_size);

tBleStatus ble_update_local_char_value(uint16_t service_handler,uint16_t characteristic_handler,uint8_t data_len,uint8_t* data);

void ble_set_peripheral_passkey(uint32_t key);

uint8_t ble_has_central_connected();

void ble_set_peripheral_connection_handler(uint16_t conn_handler);

ble_peripheral_state_t ble_get_peripheral_state();

ble_central_procedure_state_t ble_get_peripheral_proc_state();

void ble_peripheral_set_connection_complete_cb(ble_conn_cb_t cb);

void ble_peripheral_set_disconnection_complete_cb(ble_disconn_cb_t cb);

void ble_peripheral_set_pairing_complete_cb(ble_pairing_cmplt_cb_t cb);

void ble_peripheral_set_attribute_modified_cb(ble_attr_mod_cb_t cb);

void ble_peripheral_set_write_request_cb(ble_write_req_cb_t cb);

tBleStatus ble_start_advertising();

tBleStatus ble_stop_advertising();

tBleStatus ble_set_adv_data(uint8_t *data,uint8_t length);

void ble_set_scan_resp_data(uint8_t * data,uint8_t length);

tBleStatus ble_remove_AD_type(uint8_t ad_type);

#ifdef __cplusplus
}
#endif

#endif /* BNRG_PERIPHERAL_H_ */
