/*
 * ble_config.h
 *
 *  Created on: Feb 11, 2019
 *      Author: mate_pc
 */

#ifndef BNRG_CONFIG_H_
#define BNRG_CONFIG_H_

#define MAX_ADV_NAME_SIZE 30			//maximum size of the advertising name
#define MAX_ADV_MANUF_DATA_SIZE 30		//maximum size of manufacturer data in advertising packet
#define MAX_UUID_SIZE 16				//maximum size of uuid array (128bit -> 16byte)
#define MAX_ADV_UUID_NUM 10				//maximum number of uuid-s in advertising data

#define AUTH_NEEDED 1
#define AUTH_NOT_NEEDED 0
#define MULTIROLE_ENABLE 1
#define MULTIROLE_DISABLE 0
#define FORCE_TO_REBOND 0x01
#define NOT_FORCE_TO_REBOND 0x00

#define MAX_ADV_DATA_LENGTH 31

#define BLE_STATUS_INVALID_UUID_TYPE 0x03
#define BLE_STATUS_INVALID_MANUF_DATA_SIZE 0x04
#define BLE_STATUS_INVALID_NAME_SIZE 0x0b
#define BLE_STATUS_PENDING_PROCEDURE 0x0a

//unused error codes



#endif /* BNRG_CONFIG_H_ */
