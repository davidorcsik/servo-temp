#pragma once
#include <bluetooth_common.h>

namespace devices
{
	namespace Bluetooth
	{
		class BluetoothService {
		private:
			const Bluetooth::UUID uuid;
			const bool advertise_service;
			uint16_t* handle;
			uint16_t* end_group_handler;
			uint8_t attribute_max_value;
		public:
			BluetoothService() : uuid({ 0, UUID_TYPES::UUID_TYPE_128_BIT }), advertise_service(false) {}
			BluetoothService(const uint8_t* _uuid, Bluetooth::UUID_TYPES _type, bool _advertise_service)
			: uuid({ _uuid, _type }), advertise_service(_advertise_service) {}
			const Bluetooth::UUID get_uuid() const;
			bool is_advertisable() const;
			bool is_service_valid() const;
		};
	}
}