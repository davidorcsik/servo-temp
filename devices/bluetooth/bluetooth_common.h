#pragma once
#include <inttypes.h>
#include <algorithm>
#include <map>

#include <bluenrg_gatt_server.h>
#include <bluenrg_gap.h>
#include <bluenrg_gap_aci.h>
#include <bluenrg_gatt_aci.h>
#include <hci_const.h>
#include <bluenrg_hal_aci.h>
#include <bluenrg_aci_const.h>
#include <hci.h>
#include <hci_le.h>
#include <sm.h>
#include <bluenrg_hal_aci.h>


namespace devices
{
	namespace Bluetooth
	{
		enum class UUID_TYPES { UUID_TYPE_16_BIT = 0x01, UUID_TYPE_128_BIT = 0x02 };
		struct UUID {
		private:
			const UUID_TYPES type;
			uint8_t value[16];
			uint8_t length;
			bool is_value_equal_to(const uint8_t* other_value) const {
				uint8_t i = 0;
				while (i < length && value[i] == other_value[i]) { //the two values length should be the same the == operator overload is garantees it
					++i;
				}
				return i == length;
			}
		public:
			UUID() : type(UUID_TYPES::UUID_TYPE_128_BIT) {}
			UUID(const uint8_t* _uuid, UUID_TYPES _type) : type(_type) {
				length = (_type == UUID_TYPES::UUID_TYPE_16_BIT) ? 2 : 16 ;
				std::copy_n(_uuid, length, value);
			}
			const uint8_t* get_value() const;
			UUID_TYPES get_type() const;
			uint8_t get_length() const;
			bool operator==(const UUID& other) const {
				return type == other.type && is_value_equal_to(other.value);
			}
		};
		
		enum class STATUS {
			SUCCESS =							0x00,
			UNKNOWN_HCI_COMMAND =				0x01,
			UNKNOWN_CONN_IDENTIFIER =			0x02,
			AUTH_FAILURE =						0x05,
			PIN_OR_KEY_MISSING =				0x06,
			MEM_CAPACITY_EXCEEDED =				0x07,
			CONNECTION_TIMEOUT =				0x08,
			COMMAND_DISALLOWED =				0x0C,
			UNSUPPORTED_FEATURE =				0x11,
			INVALID_HCI_CMD_PARAMS =			0x12,
			RMT_USR_TERM_CONN =					0x13,
			RMT_DEV_TERM_CONN_LOW_RESRCES =		0x14,
			RMT_DEV_TERM_CONN_POWER_OFF =		0x15,
			LOCAL_HOST_TERM_CONN =				0x16,
			UNSUPP_RMT_FEATURE =				0x1A,
			INVALID_LMP_PARAM =					0x1E,
			UNSPECIFIED_ERROR =					0x1F,
			LL_RESP_TIMEOUT =					0x22,
			LMP_PDU_NOT_ALLOWED =				0x24,
			INSTANT_PASSED =					0x28,
			PAIR_UNIT_KEY_NOT_SUPP =			0x29,
			CONTROLLER_BUSY =					0x3A,
			DIRECTED_ADV_TIMEOUT =				0x3C,
			CONN_END_WITH_MIC_FAILURE =			0x3D,
			CONN_FAILED_TO_ESTABLISH =			0x3E,
			INVALID_UUID_TYPE =					0x03,
			INVALID_MANUF_DATA_SIZE =			0x04,
			INVALID_NAME_SIZE =					0x0b,
			PENDING_PROCEDURE =					0x0a,
			NO_PLACE_FOR_SERVICE,
			NO_PLACE_FOR_CHARACTERISTIC,
			SERVICE_DOES_NOT_EXISTS,
			CHARACTERISTIC_DOES_NOT_EXISTS,
		};
	}
}