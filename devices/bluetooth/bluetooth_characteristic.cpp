#include <bluetooth_characteristic.h>
#include <utils.hpp>

namespace devices
{
	namespace Bluetooth
	{
		bool BluetoothCharacteristic::event_listeners_attached = false;
		bool BluetoothCharacteristic::is_valid() const {
			return valid;
		}
//		uint8_t BluetoothCharacteristic::init() {
//			ble_char_init(const_cast<uint8_t*>(uuid.get_value()), uuid.get_type(), value_size, associated_service.ble_primitive, properties, static_cast<uint8_t>(authentication_required), ble_primitive);
//		}
		uint8_t BluetoothCharacteristic::update_characteristic_value() {
			 //TODO: add param
		}
		uint8_t BluetoothCharacteristic::on_write(const uint8_t* const attribute_data, uint8_t attribute_data_length) {
		}
		uint8_t BluetoothCharacteristic::on_write_request(const uint16_t& connection_handle, const uint8_t* const attribute_data, uint8_t attribute_data_length) {
		}
		uint8_t BluetoothCharacteristic::on_read_request(const uint16_t& connection_handle, uint8_t event_data_length, const uint8_t* attribute_value) {
		}
		
		void BluetoothCharacteristic::attach_event_listeners() {
//			ble_peripheral_set_write_request_cb(&BluetoothCharacteristic::write_request_callback);
//			//read request
//			ble_peripheral_set_attribute_modified_cb(&BluetoothCharacteristic::attribute_modification_callback);
//			event_listeners_attached = true;
		}
		void BluetoothCharacteristic::write_request_callback(uint16_t connection_handle, uint16_t attribute_handle, uint8_t* attribute_data, uint8_t attribute_data_length) {
//			auto map_it = std::find_if(
//					handle_to_class_map.begin(),
//					handle_to_class_map.end(),
//					[&attribute_handle](std::pair<characteristic_t, BluetoothCharacteristic&> current_pair) {
//						current_pair.first.value_handle == attribute_handle;
//					});
//			if (map_it == handle_to_class_map.end()) {
//				Utils::ERROR_GUARD();
//			}
//			map_it->second.on_write_request(connection_handle, attribute_data, attribute_data_length);
		}
		void BluetoothCharacteristic::attribute_modification_callback(uint16_t attribute_handle, uint8_t* attribute_data, uint8_t attribute_data_length) {
//			auto map_it = std::find_if(
//					handle_to_class_map.begin(),
//					handle_to_class_map.end(),
//				[&attribute_handle](std::pair<characteristic_t, BluetoothCharacteristic&> current_pair) {
//						current_pair.first.value_handle == attribute_handle ||
//						current_pair.first.value_handle + 1 == attribute_handle;
//					});
//			if (map_it == handle_to_class_map.end()) {
//				Utils::ERROR_GUARD();
//			}
//			map_it->second.on_write(attribute_data, attribute_data_length);
		}
		void BluetoothCharacteristic::read_request_callback(uint16_t connection_handle, uint8_t event_data_length, uint8_t* attribute_value) {
			//TODO: complete
		}
	}
}