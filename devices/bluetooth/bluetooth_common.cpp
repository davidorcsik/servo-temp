#include <bluetooth_common.h>

namespace devices
{
	namespace Bluetooth
	{
		const uint8_t* UUID::get_value() const {
			return value;
		}
		UUID_TYPES UUID::get_type() const {
			return type;
		}
		uint8_t UUID::get_length() const {
			return length;
		}
	}
}