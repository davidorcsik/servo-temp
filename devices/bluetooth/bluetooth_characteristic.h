#pragma once
#include <bluetooth_common.h>

#include <array>
#include <algorithm>
#include <unordered_map>

namespace devices
{
	namespace Bluetooth
	{
		class BluetoothCharacteristic {
		public:
			enum class PROPERTIES
			{
				BROADCAST =				0x01,
				READ =					0x02,
				WRITE_WITHOUT_RESP =	0x04,
				WRITE =					0x08,
				NOTIFY =				0x10,
				INDICATE =				0x20,
				SIGNED_WRITE =			0x40,
				EXT =					0x80
			};
		private:
			const Bluetooth::UUID uuid;
			bool valid = false;
			static void attach_event_listeners();
			static bool event_listeners_attached;
			static std::unordered_map<Bluetooth::UUID, BluetoothCharacteristic*> handle_to_class_map;
		public:
			BluetoothCharacteristic() : uuid({0, Bluetooth::UUID_TYPES::UUID_TYPE_16_BIT}) {}
			BluetoothCharacteristic(const uint8_t* _uuid, Bluetooth::UUID_TYPES _uuid_type)
			: uuid({_uuid, _uuid_type}) {
			//	handle_to_class_map.insert({ uuid, this });
				valid = true;
			}
			virtual ~BluetoothCharacteristic() {}
			bool is_valid() const;
			uint8_t init();
			uint8_t update_characteristic_value(); //TODO: add param
			uint8_t on_write(const uint8_t* const attribute_data, uint8_t attribute_data_length);
			uint8_t on_write_request(const uint16_t& connection_handle, const uint8_t* const attribute_data, uint8_t attribute_data_length);
			uint8_t on_read_request(const uint16_t& connection_handle, uint8_t event_data_length, const uint8_t* attribute_value);
			
			static void write_request_callback(uint16_t connection_handle, uint16_t attribute_handle, uint8_t* attribute_data, uint8_t attribute_data_length);
			static void attribute_modification_callback(uint16_t attribute_handle, uint8_t* attribute_data, uint8_t attribute_data_length);
			static void read_request_callback(uint16_t connection_handle, uint8_t event_data_length, uint8_t* attribute_value);
		};
	}
}