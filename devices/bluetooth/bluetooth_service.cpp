#include <bluetooth_service.h>

namespace devices {
	namespace Bluetooth
	{
		const UUID BluetoothService::get_uuid() const {
			return uuid;
		}
		bool BluetoothService::is_advertisable() const {
			return advertise_service;
		}
		bool BluetoothService::is_service_valid() const {
			return handle != nullptr;
		}
	}
}
